<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="46"/>
        <source>Arrow</source>
        <translation>Flecha</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="49"/>
        <source>Block</source>
        <translation>Bloque</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="52"/>
        <source>Bomb</source>
        <translation>Bomba</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="55"/>
        <source>Boomerang</source>
        <translation>Bumerán</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="58"/>
        <source>Camera</source>
        <translation>Cámara</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="61"/>
        <source>Carried object</source>
        <translation>Objeto llevado</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="64"/>
        <source>Chest</source>
        <translation>Cofre</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="67"/>
        <source>Crystal</source>
        <translation>Cristal</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="70"/>
        <source>Crystal block</source>
        <translation>Bloque de cristal</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="73"/>
        <source>Custom entity</source>
        <translation>Entidad custom</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="76"/>
        <source>Destination</source>
        <translation>Destino</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="79"/>
        <source>Destructible object</source>
        <translation>Destructible</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="82"/>
        <source>Door</source>
        <translation>Puerta</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="85"/>
        <source>Dynamic tile</source>
        <translation>Tile dinámica</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="88"/>
        <source>Enemy</source>
        <translation>Enemigo</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="91"/>
        <source>Explosion</source>
        <translation>Explosión</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="94"/>
        <source>Fire</source>
        <translation>Fuego</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="97"/>
        <source>Hero</source>
        <translation>Héroe</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="100"/>
        <source>Hookshot</source>
        <translation>Gancho</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="103"/>
        <source>Jumper</source>
        <translation>Saltador</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="106"/>
        <source>NPC</source>
        <translation>PNJ</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="109"/>
        <source>Pickable treasure</source>
        <translation>Tesoro recogible</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="112"/>
        <source>Sensor</source>
        <translation>Sensor</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="115"/>
        <source>Separator</source>
        <translation>Separador</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="118"/>
        <source>Shop treasure</source>
        <translation>Artículo de tienda</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="121"/>
        <source>Stairs</source>
        <translation>Escaleras</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="124"/>
        <source>Stream</source>
        <translation>Flujo</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="127"/>
        <source>Switch</source>
        <translation>Botón</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="130"/>
        <source>Teletransporter</source>
        <translation>Teletransportador</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="133"/>
        <source>Tile</source>
        <translation>Tile</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="136"/>
        <location filename="../src/ground_traits.cpp" line="52"/>
        <source>Wall</source>
        <translation>Pared</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="194"/>
        <source>Ctrl+E,Ctrl+B</source>
        <translation>Ctrl+E,Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="197"/>
        <source>Ctrl+E,Ctrl+C</source>
        <translation>Ctrl+E,Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="200"/>
        <source>Ctrl+E,Ctrl+L</source>
        <translation>Ctrl+E,Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="203"/>
        <source>Ctrl+E,Ctrl+K</source>
        <translation>Ctrl+E,Ctrl+K</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="206"/>
        <source>Ctrl+E,Ctrl+Y</source>
        <translation>Ctrl+E,Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="209"/>
        <source>Ctrl+E,Ctrl+I</source>
        <translation>Ctrl+E,Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="212"/>
        <source>Ctrl+E,Ctrl+D</source>
        <translation>Ctrl+E,Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="215"/>
        <source>Ctrl+E,Ctrl+O</source>
        <translation>Ctrl+E,Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="218"/>
        <source>Ctrl+E,Ctrl+2</source>
        <translation>Ctrl+E,Ctrl+2</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="221"/>
        <source>Ctrl+E,Ctrl+E</source>
        <translation>Ctrl+E,Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="224"/>
        <source>Ctrl+E,Ctrl+J</source>
        <translation>Ctrl+E,Ctrl+J</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="227"/>
        <source>Ctrl+E,Ctrl+N</source>
        <translation>Ctrl+E,Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="230"/>
        <source>Ctrl+E,Ctrl+P</source>
        <translation>Ctrl+E,Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="233"/>
        <source>Ctrl+E,Ctrl+S</source>
        <translation>Ctrl+E,Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="236"/>
        <source>Ctrl+E,Ctrl+A</source>
        <translation>Ctrl+E,Ctrl+A</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="239"/>
        <source>Ctrl+E,Ctrl+U</source>
        <translation>Ctrl+E,Ctrl+U</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="242"/>
        <source>Ctrl+E,Ctrl+R</source>
        <translation>Ctrl+E,Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="245"/>
        <source>Ctrl+E,Ctrl+M</source>
        <translation>Ctrl+E,Ctrl+M</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="248"/>
        <source>Ctrl+E,Ctrl+H</source>
        <translation>Ctrl+E,Ctrl+H</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="251"/>
        <source>Ctrl+E,Ctrl+T</source>
        <translation>Ctrl+E,Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="254"/>
        <source>Ctrl+E,Ctrl+1</source>
        <translation>Ctrl+E,Ctrl+1</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="257"/>
        <source>Ctrl+E,Ctrl+W</source>
        <translation>Ctrl+E,Ctrl+W</translation>
    </message>
    <message>
        <source>No such file or directory: &apos;%1&apos;</source>
        <translation type="vanished">No se encuentra el archivo o directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="121"/>
        <location filename="../src/widgets/import_dialog.cpp" line="375"/>
        <source>Source file cannot be read: &apos;%1&apos;</source>
        <translation>No puede leerse el archivo de origen: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="125"/>
        <source>Destination already exists: &apos;%1&apos;</source>
        <translation>El destino ya existe: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>No such directory: &apos;%1&apos;</source>
        <translation type="vanished">No se encuentra el directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="110"/>
        <source>Source and destination are the same: &apos;%1&apos;</source>
        <translation>El origen y el destino son idénticos: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="117"/>
        <source>No such file or folder: &apos;%1&apos;</source>
        <translation>No se encuentra el archivo o directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="134"/>
        <source>No such folder: &apos;%1&apos;</source>
        <translation>No se encuentra el directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="141"/>
        <source>Cannot copy folder &apos;%1&apos; to one of its own subfolders: &apos;%2&apos;</source>
        <translation>No se puede copiar el directorio &apos;%1&apos; a uno de sus propios subdirectorios &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="145"/>
        <location filename="../src/file_tools.cpp" line="221"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>No se puede crear la carpeta &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="159"/>
        <source>Cannot copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>No se puede copiar el archivo &apos;%1&apos; a &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="190"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>No se pudo eliminar el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="204"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>No se pudo eliminar la carpeta &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="244"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="267"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>No se puede abrir el archivo &apos;%1&apos; para escritura</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="45"/>
        <source>Plain</source>
        <translation>Liso</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="48"/>
        <source>Dashed</source>
        <translation>Punteado</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="51"/>
        <source>Intersections (cross)</source>
        <translation>Intersecciones (cruz)</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="54"/>
        <source>Intersections (point)</source>
        <translation>Intersecciones (punto)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="46"/>
        <source>Empty</source>
        <translation>Vacío</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="49"/>
        <source>Traversable</source>
        <translation>Atravesable</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="55"/>
        <source>Low wall</source>
        <translation>Pared baja</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="58"/>
        <source>Top right</source>
        <translation>Arriba derecha</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="61"/>
        <source>Top left</source>
        <translation>Arriba izquierda</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="64"/>
        <source>Bottom left</source>
        <translation>Abajo izquierda</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="67"/>
        <source>Bottom right</source>
        <translation>Abajo derecha</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="70"/>
        <source>Top right (water)</source>
        <translation>Arriba derecha (agua)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="73"/>
        <source>Top left (water)</source>
        <translation>Arriba izquierda (agua)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="76"/>
        <source>Bottom left (water)</source>
        <translation>Abajo izquierda (agua)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="79"/>
        <source>Bottom right (water)</source>
        <translation>Abajo derecha (agua)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="82"/>
        <source>Deep water</source>
        <translation>Agua profunda</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="85"/>
        <source>Shallow water</source>
        <translation>Agua poco profunda</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="88"/>
        <source>Grass</source>
        <translation>Hierba</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="91"/>
        <source>Hole</source>
        <translation>Agujero</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="94"/>
        <source>Ice</source>
        <translation>Hielo</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="97"/>
        <source>Ladder</source>
        <translation>Escalera</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="100"/>
        <source>Prickles</source>
        <translation>Pinchos</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="103"/>
        <source>Lava</source>
        <translation>Lava</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="45"/>
        <source>Could not create data directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="56"/>
        <source>Untitled Quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="59"/>
        <source>Could not create quest properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="101"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>No se pudo encontrar el directorio &quot;assets&quot;.
Asegúrate de que Solarus Quest Editor está bien instalado.</translation>
    </message>
    <message>
        <location filename="../src/obsolete_editor_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is not supported by this version of the quest editor (%2).
Please download the latest version of the editor on www.solarus-games.org.</source>
        <translation>El formato de este proyecto (%1) no es compatible con esta versión del Quest Editor (%2).
Por favor, descarga la última versión del editor en www.solarus-games.org.</translation>
    </message>
    <message>
        <location filename="../src/obsolete_quest_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is obsolete.
Please upgrade your quest  data files to Solarus %2.</source>
        <translation>El formato de este proyecto (%1) está obsoleto.
Por favor, actualiza los archivos de datos del proyecto para Solarus %2.</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Tile pattern animation</comment>
        <translation type="vanished">Ninguno</translation>
    </message>
    <message>
        <source>Frames 1-2-3-1</source>
        <translation type="vanished">Frames 1-2-3-1</translation>
    </message>
    <message>
        <source>Frames 1-2-3-2-1</source>
        <translation type="vanished">Frames 1-2-3-2-1</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="46"/>
        <source>None</source>
        <comment>Tile pattern scrolling</comment>
        <translatorcomment>Desplazamiento de patrón de tile</translatorcomment>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="49"/>
        <source>Scrolling on itself</source>
        <translation>Scrolling en sí mismo</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="52"/>
        <source>Parallax scrolling</source>
        <translation>Scrolling parallax</translation>
    </message>
    <message>
        <source>Frames 1-2-3-1, parallax</source>
        <translation type="vanished">Frames 1-2-3-1, parallax</translation>
    </message>
    <message>
        <source>Frames 1-2-3-2-1, parallax</source>
        <translation type="vanished">Frames 1-2-3-2-1, parallax</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="50"/>
        <source>In both directions</source>
        <translation>En ambas direcciones</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="53"/>
        <source>Horizontally</source>
        <translation>Horizontalmente</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="56"/>
        <source>Vertically</source>
        <translation>Verticalmente</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="59"/>
        <source>Non repeatable</source>
        <translation>No repetible</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="43"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="46"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="45"/>
        <source>When the world changes</source>
        <translation>Cuando cambie el mundo</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="48"/>
        <source>Always</source>
        <translation>Siempre</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="51"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="44"/>
        <source>Immediate</source>
        <translation>Inmediato</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="47"/>
        <source>Fade in/out</source>
        <translation>Fundido aparecer/desaparecer</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="50"/>
        <source>Scrolling</source>
        <translation>Scrolling</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="61"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="64"/>
        <source>Right side</source>
        <translation>Lado derecho</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="67"/>
        <source>Top side</source>
        <translation>Lado superior</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="70"/>
        <source>Left side</source>
        <translation>Lado izquierdo</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="73"/>
        <source>Bottom side</source>
        <translation>Lado inferior</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="76"/>
        <source>Top-right corner (convex)</source>
        <translation>Esquina superior derecha (convexo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="79"/>
        <source>Top-left corner (convex)</source>
        <translation>Esquina superior izquierda (convexo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="82"/>
        <source>Bottom-left corner (convex)</source>
        <translation>Esquina inferior-izquierda (convexo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="85"/>
        <source>Bottom-right corner (convex)</source>
        <translation>Esquina inferior-derecha (convexo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="88"/>
        <source>Top-right corner (concave)</source>
        <translation>Esquina superior derecha (cóncavo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="91"/>
        <source>Top-left corner (concave)</source>
        <translation>Esquina superior izquierda (cóncavo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="94"/>
        <source>Bottom-left corner (concave)</source>
        <translation>Esquina inferior-izquierda (cóncavo)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="97"/>
        <source>Bottom-right corner (concave)</source>
        <translation>Esquina inferior-derecha (cóncavo)</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="46"/>
        <source>Side by side</source>
        <translation>Uno junto al otro</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="49"/>
        <source>Input texture</source>
        <translation>Textura de entrada</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="52"/>
        <source>Output texture</source>
        <translation>Textura de salida</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="55"/>
        <source>Swipe</source>
        <translation>Herramienta deslizante</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="363"/>
        <source>Source file does not exist: &apos;%1&apos;</source>
        <translation>El archivo de origen no existe: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="367"/>
        <source>Source path is a folder: &apos;%1&apos;</source>
        <translation>La ruta de origen es un directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="371"/>
        <source>Source file is a symbolic link: &apos;%1&apos;</source>
        <translation>El archivo de origen es un enlace simbólico: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="456"/>
        <source>Source folder does not exist: &apos;%1&apos;</source>
        <translation>El directorio de origen no existe: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="460"/>
        <source>Source path is not a folder: &apos;%1&apos;</source>
        <translation>La ruta de origen no es un directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="464"/>
        <source>Source folder cannot be read: &apos;%1&apos;</source>
        <translation>El directorio de origen no se puede leer: &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="35"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="48"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="61"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::AboutDialog</name>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="176"/>
        <source>Integrated development environment for Solarus, a free and open-source ARPG 2D game engine.</source>
        <translation>Entorno de desarrollo integrado para Solarus, un motor libre y de código abierto para juegos ARPG 2D.</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Website&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Página Web&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="240"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;This program licensed is under the &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Este programa utiliza la licencia: &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.cpp" line="37"/>
        <source>About %0</source>
        <translation>Acerca de %0</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeBorderSetIdDialog</name>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="14"/>
        <source>Border set id</source>
        <translation>Id de conjunto de bordes</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="20"/>
        <source>New contour id:</source>
        <translation>Nuevo id de contorno:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.cpp" line="35"/>
        <source>New id for contour &apos;%1&apos;:</source>
        <translation>Nuevo id para contorno &apos;%1&apos;:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeDialogIdDialog</name>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="14"/>
        <source>Change dialog id</source>
        <translation>Cambiar id del diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="20"/>
        <source>New dialog id:</source>
        <translation>Nuevo id de diálogo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="30"/>
        <source>Change the id of all dialogs with this id as prefix</source>
        <translation>Cambiar el id de todos los diálogos que tienen este id como prefijo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="41"/>
        <source>New id for dialogs prefixed by &apos;%1&apos;:</source>
        <translation>Nuevo id para diálogos con el prefijo &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="44"/>
        <source>New id for dialog &apos;%1&apos;:</source>
        <translation>Nuevo id para diálogo &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="101"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de diálogo inválido: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="115"/>
        <source>The dialog &apos;%1&apos; already exists</source>
        <translation>El diálogo &apos;%1&apos; ya existe</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeFileInfoDialog</name>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="14"/>
        <source>File information</source>
        <translation>Información de archivo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="20"/>
        <source>Set file information</source>
        <translation>Añadir información de archivo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="29"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="42"/>
        <source>License:</source>
        <translation>Licencia:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangePatternIdDialog</name>
    <message>
        <source>Rename tile pattern</source>
        <translation type="vanished">Renombrar patrón de tile</translation>
    </message>
    <message>
        <source>New pattern id:</source>
        <translation type="vanished">Nuevo id de patrón:</translation>
    </message>
    <message>
        <source>Update references in existing maps</source>
        <translation type="vanished">Actualizar referencias en mapas existentes</translation>
    </message>
    <message>
        <source>New id for pattern &apos;%1&apos;:</source>
        <translation type="vanished">Nuevo id para el patrón &apos;%1&apos;:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeResourceIdDialog</name>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="14"/>
        <source>Rename resource</source>
        <translation>Renombrar recurso</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="20"/>
        <source>New id for resource element</source>
        <translation>Nuevo id para el elemento</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="30"/>
        <source>Update references</source>
        <translation>Actualizar referencias</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="46"/>
        <source>New id for %1 &apos;%2&apos;:</source>
        <translation>Nuevo id para %1 &apos;%2&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="52"/>
        <source>Update existing teletransporters leading to this map</source>
        <translation>Actualizarteletransportadores que llevan a este mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="56"/>
        <source>Update existing maps using this tileset</source>
        <translation>Actualizar mapas que usan este tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="60"/>
        <source>Update existing maps using this music</source>
        <translation>Actualizar mapas existentes que utilicen esta música</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="64"/>
        <source>Update existing enemies having this breed</source>
        <translation>Actualizar enemigos existentes que tengan este modelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="68"/>
        <source>Update existing custom entities having this model</source>
        <translation>Actualizar entidades custom existentes que tengan este modelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="126"/>
        <source>Empty resource element id</source>
        <translation>Falta id de recurso</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="131"/>
        <source>Invalid resource element id</source>
        <translation>Id de recurso inválida</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeSourceImageDialog</name>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="14"/>
        <source>Change source image</source>
        <translation>Cambiar la imagen fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="28"/>
        <source>Image</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="35"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.cpp" line="125"/>
        <source>No image selected.</source>
        <translation>No hay imagen seleccionada.</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeStringKeyDialog</name>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="14"/>
        <source>Change string key</source>
        <translation>Cambiar la clave del texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="20"/>
        <source>New string key:</source>
        <translation>Nueva clave de texto:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="30"/>
        <source>Change the key of all strings with this key as prefix</source>
        <translation>Cambiar la clave de todos los textos con esta clave como prefijo</translation>
    </message>
    <message>
        <source>Change the key of all string with this key as prefix</source>
        <translation type="vanished">Cambiar la clave de todos los textos con esta clave como prefijo</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="41"/>
        <source>New key for strings prefixed by &apos;%1&apos;:</source>
        <translation>Nueva clave para textos con prefijo &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="44"/>
        <source>New key for string &apos;%1&apos;:</source>
        <translation>Nueva clave para el texto &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="101"/>
        <source>Invalid string key: %1</source>
        <translation>Clave de texto inválida: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="115"/>
        <source>The string &apos;%1&apos; already exists</source>
        <translation>El texto &apos;%1&apos; ya existe</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ColorChooser</name>
    <message>
        <location filename="../src/widgets/color_chooser.cpp" line="89"/>
        <source>Select color</source>
        <translation>Selecciona color</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ColorPicker</name>
    <message>
        <source>Select color</source>
        <translation type="vanished">Selecciona color</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Console</name>
    <message>
        <location filename="../src/widgets/console.ui" line="42"/>
        <source>Run Lua code to the quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="259"/>
        <source>The quest process failed to start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="262"/>
        <source>The quest process crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="265"/>
        <source>The quest process timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="273"/>
        <source>An unknown error occurred to the quest process.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogPropertiesTable</name>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="35"/>
        <source>New property...</source>
        <translation>Nueva propiedad...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="41"/>
        <source>Change key...</source>
        <translation>Cambiar clave...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="42"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="49"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="57"/>
        <source>Set from translation...</source>
        <translation>Definir desde la traducción...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsEditor</name>
    <message>
        <source>Dialogss editor</source>
        <translation type="vanished">Editor de diálogos</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="14"/>
        <source>Dialogs editor</source>
        <translation>Editor de diálogos</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="35"/>
        <source>Language properties</source>
        <translation>Propiedades del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="44"/>
        <source>Language id</source>
        <translation>Id del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="51"/>
        <source>Folder name of the language</source>
        <translation>Nombre de la carpeta del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="64"/>
        <source>Language description</source>
        <translation>Descripción del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="71"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="99"/>
        <source>Compare to language</source>
        <translation>Comparar con el idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="112"/>
        <source>Refresh language</source>
        <translation>Recargar idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="115"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="164"/>
        <source>Add dialog</source>
        <translation>Añadir diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="202"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="134"/>
        <source>Change dialog id</source>
        <translation>Cambiar id de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="240"/>
        <source>Duplicate dialog(s)</source>
        <translation>Duplicar diálogo(s)</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="278"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="205"/>
        <source>Delete dialog</source>
        <translation>Eliminar diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="316"/>
        <source>Dialog properties</source>
        <translation>Propiedades del diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="324"/>
        <source>Dialog id:</source>
        <translation>Id del diálogo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="371"/>
        <source>Text:</source>
        <translation>Texto:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="391"/>
        <source>1,1</source>
        <translation>1,1</translation>
    </message>
    <message>
        <source>Display right margin at column:</source>
        <translation type="vanished">Mostrar margen derecho en la columna:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="416"/>
        <source>Translation:</source>
        <translation>Traducción:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="445"/>
        <source>Properties :</source>
        <translation>Propiedades:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="475"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="480"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="496"/>
        <source>Add property</source>
        <translation>Añadir propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="534"/>
        <source>Change property key</source>
        <translation>Cambiar clave de la propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="572"/>
        <source>Delete property</source>
        <translation>Eliminar propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="70"/>
        <source>Create dialog</source>
        <translation>Crear diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="103"/>
        <source>Duplicate dialogs</source>
        <translation>Duplicar diálogos</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="167"/>
        <source>Change dialog id prefix</source>
        <translation>Cambiar prefijo de id de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="237"/>
        <source>Delete dialogs</source>
        <translation>Eliminar diálogos</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="271"/>
        <source>Change dialog text</source>
        <translation>Cambiar texto de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="306"/>
        <source>Create dialog property</source>
        <translation>Crear propiedad de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="340"/>
        <source>Delete dialog property</source>
        <translation>Eliminar propiedad de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="375"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="973"/>
        <source>Change dialog property key</source>
        <translation>Cambiar clave de propiedad de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="414"/>
        <source>Change dialog property</source>
        <translation>Cambiar propiedad de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="464"/>
        <source>Dialogs %1</source>
        <translation>Diálogos %1</translation>
    </message>
    <message>
        <source>Dialogs &apos;%1&apos; has been modified. Save changes?</source>
        <translation type="vanished">Los diálogos &apos;%1&apos; han sido modificados. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="467"/>
        <source>Dialogs &apos;%1&apos; have been modified. Save changes?</source>
        <translation>Los diálogos &apos;%1&apos; han sido modificados. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="476"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;Ningún idioma&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="628"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog</source>
        <translation>Nuevo diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog id:</source>
        <translation>Nueva id de diálogo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="681"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de diálogo inválida: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="686"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="703"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>El diálogo &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="699"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="777"/>
        <source>Delete confirmation</source>
        <translation>Confirmar la eliminación</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="778"/>
        <source>Do you really want to delete all dialogs prefixed by &apos;%1&apos;?</source>
        <translation>¿De verdad quieres eliminar todos los diálogos con prefijo &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="910"/>
        <source>New dialog property</source>
        <translation>Nueva propiedad de diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="911"/>
        <source>New property key:</source>
        <translation>Nueva clave de propiedad:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="930"/>
        <source>The property &apos;%1&apos; already exists in the dialog &apos;%2&apos;</source>
        <translation>La propiedad &apos;%1&apos; ya existe en el diálogo &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="974"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>Cambiar clave de la propiedad &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="1099"/>
        <source>Invalid property key: it should be a valid Lua identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="919"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="982"/>
        <source>The property key cannot be empty</source>
        <translation>La clave de la propiedad no puede estar vacía</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsModel</name>
    <message>
        <location filename="../src/dialogs_model.cpp" line="46"/>
        <location filename="../src/dialogs_model.cpp" line="878"/>
        <source>Cannot open dialogs data file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo de diálogos &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="84"/>
        <source>Cannot save dialogs data file &apos;%1&apos;</source>
        <translation>No se puede guardar el archivo de diálogos &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="394"/>
        <location filename="../src/dialogs_model.cpp" line="708"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de diálogo inválida: %1</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="398"/>
        <location filename="../src/dialogs_model.cpp" line="496"/>
        <location filename="../src/dialogs_model.cpp" line="589"/>
        <location filename="../src/dialogs_model.cpp" line="671"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>El diálogo &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="585"/>
        <source>Dialog &apos;%1&apos; does not exist</source>
        <translation>El diálogo &apos;%1&apos; no existe</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="593"/>
        <source>Invalid dialog id: &apos;%1&apos;</source>
        <translation>Id de diálogo inválida: %1</translation>
    </message>
    <message>
        <source>Dialog &apos;%1&apos; no exists</source>
        <translation type="vanished">El diálogo &apos;%1&apos; no existe</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsTreeView</name>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="37"/>
        <source>New dialog...</source>
        <translation>Nuevo diálogo...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="43"/>
        <source>Duplicate dialog(s)...</source>
        <translation>Duplicar diálogo(s)...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="49"/>
        <source>Change id...</source>
        <translation>Cambiar id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DrawingRectangleState</name>
    <message>
        <source>New pattern (more options)</source>
        <translation type="obsolete">Nuevo patrón (más opciones)</translation>
    </message>
    <message>
        <source>New pattern (%1)</source>
        <translation type="obsolete">Nuevo patrón (%1)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::EditEntityDialog</name>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="20"/>
        <source>Edit an entity</source>
        <translation>Editar una entidad</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="28"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="48"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="60"/>
        <source>Update existing teletransporters</source>
        <translation>Actualizar teletransportadores existentes</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="72"/>
        <source>Layer</source>
        <translation>Capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="126"/>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="136"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="146"/>
        <source>Origin</source>
        <translation>Origen</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="156"/>
        <source>Direction</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="212"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="219"/>
        <source>Sprite</source>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="234"/>
        <source>Repeat sprite with tiling</source>
        <translation>Repetir sprite con embaldosado</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="243"/>
        <source>Subtype</source>
        <translation>Subtipo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="253"/>
        <source>Breed</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="263"/>
        <source>Custom entity script</source>
        <translation>Script de entidad custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="273"/>
        <source>Save the state</source>
        <translation>Guardar estado</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="295"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="382"/>
        <source>in variable</source>
        <translation>en la variable</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="308"/>
        <source>Treasure</source>
        <translation>Tesoro</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="335"/>
        <source>Variant of this item</source>
        <translation>Variante de este ítem</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="373"/>
        <source>Save the treasure state</source>
        <translation>Guardar estado del tesoro</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="397"/>
        <source>Price font</source>
        <translation>Fuente de escritura del precio</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="407"/>
        <source>Play a sound</source>
        <translation>Reproducir un sonido</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="417"/>
        <source>Transition</source>
        <translation>Transición</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="427"/>
        <source>Destination map</source>
        <translation>Mapa de destino</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="437"/>
        <source>Destination</source>
        <translation>Destino</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="447"/>
        <source>Can be lifted</source>
        <translation>Se puede levantar</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="469"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <source>Damage on enemies</source>
        <translation type="vanished">Daño sobre enemigos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="569"/>
        <source>Set a special ground</source>
        <translation>Modificar el suelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1060"/>
        <source>Maximum moves</source>
        <translation>Máximo de movimientos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="579"/>
        <source>Opening mode</source>
        <translation>Modo de apertura</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="508"/>
        <source>Can hurt enemies</source>
        <translation>Puede herir enemigos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="530"/>
        <source>Damage</source>
        <translation>Daño</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="601"/>
        <source>By script</source>
        <translation>Por script</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="608"/>
        <source>By hero</source>
        <translation>Por héroe</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="617"/>
        <source>By hero, savegame variable required</source>
        <translation>Por héroe,variable de guardado requerida</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="647"/>
        <source>Reset/decrement when opening</source>
        <translation>Reajustar/disminuir la variable al abrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="658"/>
        <source>By hero, item required</source>
        <translation>Por héroe, ítem requerido</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="695"/>
        <source>Remove/decrement when opening</source>
        <translation>Retirar/disminuir el ítem al abrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="704"/>
        <source>By explosion</source>
        <translation>Por explosión</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="714"/>
        <source>Action</source>
        <translation>Acción</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="738"/>
        <source>Show a dialog</source>
        <translation>Mostrar un diálogo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="750"/>
        <source>Call the map script</source>
        <translation>Llamar al script del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="759"/>
        <source>Call an item script</source>
        <translation>Llamar al script de un ítem</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="781"/>
        <source>Update starting location</source>
        <translation>Actualizar lugar de comienzo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="788"/>
        <source>Only possible if the destination has a name</source>
        <translation>Solamente posible si el destino tiene un nombre</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="795"/>
        <source>Pattern</source>
        <translation>Patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="802"/>
        <source>Click to choose another pattern</source>
        <translation>Clica para elegir otro patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="809"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="831"/>
        <source>Tileset of the map</source>
        <translation>Tileset del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="840"/>
        <source>Other:</source>
        <translation>Otro:</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="862"/>
        <source>User properties</source>
        <translation>Propiedades de usuario</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="884"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="889"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="905"/>
        <source>Add property</source>
        <translation>Añadir propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="934"/>
        <source>Change property key</source>
        <translation>Cambiar clave de la propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="963"/>
        <source>Delete property</source>
        <translation>Eliminar propiedad</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="992"/>
        <source>Move up</source>
        <translation>Mover arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1021"/>
        <source>Move down</source>
        <translation>Mover abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1103"/>
        <source>Initial state</source>
        <translation>Estado inicial</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1096"/>
        <source>Enabled at start</source>
        <translation>Activado al principio</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="198"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="238"/>
        <source>The property &apos;%1&apos; already exists</source>
        <translation>La propiedad &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="223"/>
        <source>Change user property key</source>
        <translation>Cambiar clave de propiedad de usuario</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="224"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>Cambiar clave de la propiedad &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="232"/>
        <source>The property key cannot be empty</source>
        <translation>La clave de la propiedad no puede estar vacía</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="244"/>
        <source>The key &apos;%1&apos; is invalid</source>
        <translation>La clave &apos;%1&apos; es inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Set as the default destination</source>
        <translation>Poner como destino por defecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Cutting the object</source>
        <translation>Cortar el objeto</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Can be cut</source>
        <translation>Se puede cortar</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Exploding</source>
        <translation>Explotar</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Can explode</source>
        <translation>Puede explotar</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Regeneration</source>
        <translation>Regeneración</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Can regenerate</source>
        <translation>Puede regenerarse</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Interactions</source>
        <translation>Interacciones</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Can be pushed</source>
        <translation>Se puede empujar</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="527"/>
        <source>Can be pulled</source>
        <translation>Se puede tirar de él</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Activation</source>
        <translation>Activación</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Requires a block to be activated</source>
        <translation>Requiere un bloque para activarse</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Leaving the switch</source>
        <translation>Alejarse del interruptor</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Deactivate when leaving</source>
        <translation>Desactivar al apartarse</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Hero</source>
        <translation>Héroe</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Obstacle for the hero</source>
        <translation>Obstáculo para el héroe</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Enemies</source>
        <translation>Enemigos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Obstacle for enemies</source>
        <translation>Obstáculo para enemigos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>NPCs</source>
        <translation>PNJs</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>Obstacle for NPCs</source>
        <translation>Obstáculo para PNJs</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Blocks</source>
        <translation>Bloques</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Obstacle for blocks</source>
        <translation>Obstáculo para bloques</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Projectiles</source>
        <translation>Proyectiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Obstacle for projectiles</source>
        <translation>Obstáculo para proyectiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Movements</source>
        <translation>Movimientos</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Allow to move</source>
        <translation>Permitir movimiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Sword</source>
        <translation>Espada</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Allow to use the sword</source>
        <translation>Permitir la espada</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Items</source>
        <translation>Ítems</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Allow to use equipment items</source>
        <translation>Permitir los ítems de equipamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="582"/>
        <source>Price</source>
        <translation>Precio</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="583"/>
        <source>Jump length</source>
        <translation>Longitud del salto</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="584"/>
        <source>Speed</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="633"/>
        <source>Show a dialog if fails to open</source>
        <translation>Mostrar un diálogo si falla al abrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="634"/>
        <source>Description dialog id</source>
        <translation>Diálogo de descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="852"/>
        <source>(Default destination)</source>
        <translation>(Destino por defecto)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="853"/>
        <source>(Same point)</source>
        <translation>(Mismo punto)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="854"/>
        <source>(Side of the map)</source>
        <translation>(Lado del mapa)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="918"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="926"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="919"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="928"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="920"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="930"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="921"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="932"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="927"/>
        <source>Right-up</source>
        <translation>Derecha-arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="929"/>
        <source>Left-up</source>
        <translation>Izquierda-arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="931"/>
        <source>Left-down</source>
        <translation>Izquierda-abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="933"/>
        <source>Right-down</source>
        <translation>Derecha-abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1002"/>
        <source>(Default)</source>
        <translation>(Predeterminado)</translation>
    </message>
    <message>
        <source>Cannot move</source>
        <translation type="vanished">No puede moverse</translation>
    </message>
    <message>
        <source>1 move only</source>
        <translation type="vanished">1 único movimiento</translation>
    </message>
    <message>
        <source>Unlimited</source>
        <translation type="vanished">Ilimitado</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1418"/>
        <source>Save the enemy state</source>
        <translation>Guardar estado del enemigo</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1419"/>
        <source>Save the door state</source>
        <translation>Guardar estado de la puerta</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1510"/>
        <source>Play a sound when destroyed</source>
        <translation>Reproducir un sonido al destruirse</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1787"/>
        <source>(None)</source>
        <translation>(Ninguno)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Editor</name>
    <message>
        <location filename="../src/widgets/editor.cpp" line="172"/>
        <source>File &apos;%1&apos; has been modified. Save changes?</source>
        <translation>El archivo &apos;%1&apos; ha sido modificado. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/editor.cpp" line="466"/>
        <source>Save changes</source>
        <translation>Guardar cambios</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ExternalScriptDialog</name>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="14"/>
        <source>Running script</source>
        <translation>Ejecutando script</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="24"/>
        <source>Running script...</source>
        <translation>Ejecutando script...</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="40"/>
        <location filename="../src/widgets/external_script_dialog.cpp" line="169"/>
        <source>In progress</source>
        <translation>En progreso</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="62"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="56"/>
        <source>Cannot determine the directory of script &apos;%1&apos;</source>
        <translation>No se puede determinar el directorio del script &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="201"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="244"/>
        <source>Successful!</source>
        <translation>¡Éxito!</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="248"/>
        <source>Failure</source>
        <translation>Error</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::FindTextDialog</name>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="14"/>
        <source>Find text</source>
        <translation>Buscar texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="25"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="32"/>
        <source>Find</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="42"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="37"/>
        <source>Replace</source>
        <translation>Reemplazar</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::GetAnimationNameDialog</name>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="34"/>
        <source>New animation</source>
        <translation>Nueva animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="50"/>
        <source>Change animation name</source>
        <translation>Cambiar nombre de animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="84"/>
        <source>Empty animation name</source>
        <translation>Vaciar nombre de animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="91"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>La animación &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="105"/>
        <source>Animation name:</source>
        <translation>Nombre de animación:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImageView</name>
    <message>
        <location filename="../src/widgets/image_view.cpp" line="41"/>
        <source>Failed to load image &apos;%1&apos;</source>
        <translation>No se pudo cargar la imagen &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImportDialog</name>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="14"/>
        <source>Import files from another quest</source>
        <translation>Importar archivos de otro proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="36"/>
        <source>Source quest</source>
        <translation>Proyecto fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="52"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="68"/>
        <source>Destination quest</source>
        <translation>Proyecto de destino</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="91"/>
        <source>Identify missing</source>
        <translation>Identificar lo que falte</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="102"/>
        <source>No missing files found</source>
        <translation>No se encontraron archivos que falten</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="57"/>
        <source>Import files</source>
        <translation>Importar archivos</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="120"/>
        <source>Select a quest where to import from</source>
        <translation>Seleccionar un proyecto del que importar</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="136"/>
        <source>Source and destination quest are the same</source>
        <translation>Los proyectos fuente y de destino son el mismo</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="143"/>
        <source>No source quest was not found in folder &apos;%1&apos;</source>
        <translation>No se ha encontrado el proyecto fuente en el directorio &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="209"/>
        <source>No candidates found</source>
        <translation>No se encontraron candidatos</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="210"/>
        <source>%1 candidates found</source>
        <translation>%1 candidato encontrado</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="272"/>
        <source>Import 1 item</source>
        <translation>Importar 1 ítem</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="275"/>
        <source>Import %1 items</source>
        <translation>Importar %1 ítems</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="309"/>
        <source>Import confirmation</source>
        <translation>Confirmación de importado</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="310"/>
        <source>%1 items will be imported to your quest.</source>
        <translation>%1 ítems serán importados a tu proyecto.</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="344"/>
        <source>Cannot import symbolic link &apos;%1&apos;</source>
        <translation>No se puede importar el enlace simbólico &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="382"/>
        <source>Destination path already exists and is a folder: &apos;%1&apos;</source>
        <translation>La ruta de destino ya existe y es un directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="391"/>
        <location filename="../src/widgets/import_dialog.cpp" line="482"/>
        <source>Destination already exists</source>
        <translation>El destino ya existe</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="392"/>
        <source>The destination file &apos;%1&apos; already exists.
Do you want to overwrite it?</source>
        <translation>El archivo de destino &apos;%1&apos; ya existe.
¿Quieres sobreescribirlo?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="393"/>
        <source>Apply this choice for remaining files</source>
        <translation>Aplicar esta elección al resto de archivos</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="420"/>
        <source>Failed to remove existing file &apos;%1&apos;</source>
        <translation>No se pudo eliminar el archivo existente &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="436"/>
        <source>Failed to copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>No se pudo copiar el archivo &apos;%1&apos; a &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="471"/>
        <source>Destination path already exists and is not a directory: &apos;%1&apos;</source>
        <translation>La ruta de destino ya existe y no es un directorio: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="483"/>
        <source>The destination directory &apos;%1&apos; already exists.
Do you want to overwrite its content?</source>
        <translation>El directorio de destino &apos;%1&apos; ya existe.
¿Quieres sobreescribir su contenido?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="484"/>
        <source>Apply this choice for remaining directories</source>
        <translation>Aplicar esta elección al resto de directorios</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="601"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="604"/>
        <source>Overwrite</source>
        <translation>Sobreescribir</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="606"/>
        <source>Skip</source>
        <translation>Saltar</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::InputDialogWithCheckBox</name>
    <message>
        <location filename="../src/widgets/input_dialog_with_check_box.ui" line="21"/>
        <source>New value:</source>
        <translation>Nuevo valor:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MainWindow</name>
    <message>
        <location filename="../src/widgets/main_window.ui" line="20"/>
        <source>Solarus Quest Editor</source>
        <translation>Solarus Quest Editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="77"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="96"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="110"/>
        <source>Run</source>
        <translation>Ejecutar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="117"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="130"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="139"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="146"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="188"/>
        <source>New quest...</source>
        <translation>Nuevo proyecto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="193"/>
        <source>Load quest...</source>
        <translation>Cargar proyecto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="196"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="201"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="210"/>
        <location filename="../src/widgets/main_window.cpp" line="1736"/>
        <source>Run quest</source>
        <translation>Ejecutar proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="213"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="222"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="231"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="240"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="245"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="254"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="266"/>
        <source>Show grid</source>
        <translation>Mostrar grid</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="269"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="284"/>
        <location filename="../src/widgets/main_window.ui" line="287"/>
        <source>Show layer 0</source>
        <translation>Mostrar capa 0</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="290"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="305"/>
        <location filename="../src/widgets/main_window.ui" line="308"/>
        <source>Show layer 1</source>
        <translation>Mostrar capa 1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="311"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="326"/>
        <location filename="../src/widgets/main_window.ui" line="329"/>
        <source>Show layer 2</source>
        <translation>Mostrar capa 2</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="332"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="341"/>
        <source>Documentation</source>
        <translation>Documentación</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="344"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="353"/>
        <source>Website</source>
        <translation>Sitio web</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="358"/>
        <source>Find / Replace</source>
        <translation>Encontrar / Reemplazar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="467"/>
        <source>Import from a quest...</source>
        <translation>Importar desde un proyecto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="470"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="475"/>
        <source>Build quest package...</source>
        <translation>Crear paquete de proyecto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="484"/>
        <source>Export to image...</source>
        <translation>Exportar a imagen...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="487"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="492"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="501"/>
        <location filename="../src/widgets/main_window.ui" line="504"/>
        <source>Run current map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="507"/>
        <source>Shift+F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="512"/>
        <source>Re-open closed tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="515"/>
        <source>Ctrl+Shift+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Buscar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="363"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="372"/>
        <source>Select all</source>
        <translation>Seleccionar todo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="377"/>
        <source>Save all</source>
        <translation>Guardar todo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="380"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="385"/>
        <source>Close all</source>
        <translation>Cerrar todo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="388"/>
        <source>Ctrl+Shift+W</source>
        <translation>Ctrl+Shift+W</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="396"/>
        <source>Show console</source>
        <translation>Mostrar consola</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="399"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="407"/>
        <source>Unselect all</source>
        <translation>Deseleccionar todo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="412"/>
        <source>Close quest</source>
        <translation>Cerrar proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="421"/>
        <location filename="../src/widgets/main_window.cpp" line="1785"/>
        <location filename="../src/widgets/main_window.cpp" line="1792"/>
        <source>Pause music</source>
        <translation>Pausar música</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="430"/>
        <location filename="../src/widgets/main_window.cpp" line="1781"/>
        <source>Stop music</source>
        <translation>Detener música</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="442"/>
        <source>Show traversable entities</source>
        <translation>Mostrar entidades atravesables</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="454"/>
        <source>Show obstacle entities</source>
        <translation>Mostrar entidades obstáculo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="459"/>
        <source>Quest properties</source>
        <translation>Propiedades del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="462"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="104"/>
        <source>Recent quests</source>
        <translation>Proyectos recientes</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="130"/>
        <location filename="../src/widgets/main_window.cpp" line="331"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="150"/>
        <source>Show/hide more layers</source>
        <translation>Mostrar/ocultar más capas</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="154"/>
        <source>Lock/unlock layers</source>
        <translation>Bloquear/desbloquear capas</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="162"/>
        <location filename="../src/widgets/main_window.cpp" line="481"/>
        <source>Show/hide entity types</source>
        <translation>Mostrar/ocultar tipos de entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="265"/>
        <source>Could not locate the assets directory.
Some features like creating a new quest will not be available.
Please make sure that Solarus Quest Editor is correctly installed.</source>
        <translation>No se pudo localizar el directorio &quot;assets&quot;.
Algunas características como crear un nuevo proyecto no estarán disponibles.
Por favor, asegúrate de que Solarus Quest Editor está bien instalado.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="333"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="334"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="335"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="336"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="337"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="373"/>
        <source>Show all layers</source>
        <translation>Mostrar todas capas</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="383"/>
        <source>Hide all layers</source>
        <translation>Ocultar todas capas</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="402"/>
        <source>Show layer %1</source>
        <translation>Mostrar capa %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="453"/>
        <source>Lock layer %1</source>
        <translation>Bloquear capa %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="458"/>
        <source>Ctrl+%1</source>
        <translation>Ctrl+%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="510"/>
        <source>Show all entities</source>
        <translation>Mostrar todas entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="520"/>
        <source>Hide all entities</source>
        <translation>Ocultar todas entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="615"/>
        <source>No quest was found in directory
&apos;%1&apos;</source>
        <translation>No se encontraron proyectos en el directorio
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="688"/>
        <source>Obsolete quest</source>
        <translation>Proyecto obsoleto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="689"/>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.
Would you like to update this quest?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="696"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="737"/>
        <source>An error occurred while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.</source>
        <translation type="vanished">El formato de este proyecto (%1) está obsoleto.
Tus archivos de datos serán actualizados automáticamente a Solarus %2.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="720"/>
        <source>Upgrading quest data files</source>
        <translation>Actualizar archivos del proyecto</translation>
    </message>
    <message>
        <source>An error occured while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation type="vanished">Ha ocurrido un error al actualizar el proyecto.
Tu proyecto se ha guardado sin cambios en formato %1.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="785"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>No se pudo encontrar el directorio &quot;assets&quot;.
Asegúrate de que Solarus Quest Editor está bien instalado.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="845"/>
        <source>Select quest directory</source>
        <translation>Seleccionar directorio del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1082"/>
        <source>Files are modified</source>
        <translation>Modificaciones sin guardar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1083"/>
        <source>Do you want to save modifications before running the quest?</source>
        <translation>¿Quieres guardar las modificaciones antes de ejecutar el proyecto?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1313"/>
        <source>Local Documentation Not Found</source>
        <translation>Documentación Local No Encontrada</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1314"/>
        <source>The local copy of Solarus Documentation could not be found. Would you like to try going on line to find the documentaion?</source>
        <translation>No se pudo encontrar la copia local de la Documentación Solarus. ¿Te gustaría intentar buscar la documentación en línea?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1716"/>
        <source>Missing show entity type action</source>
        <translation>No se encuentra la acción de mostrar el tipo de entidad</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1733"/>
        <source>Stop quest</source>
        <translation>Detener proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1789"/>
        <source>Play selected music</source>
        <translation>Reproducir música seleccionada</translation>
    </message>
    <message>
        <source>Solarus Quest Editor %1</source>
        <translation type="vanished">Solarus Quest Editor %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1900"/>
        <source>File modified</source>
        <translation>Archivo modificado</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1901"/>
        <source>This file is open and has unsaved changes.
Please save it or close it before renaming.</source>
        <translation>Este archivo está abierto y tiene cambios sin guardar.
Por favor, guárdalo o ciérralo antes de renombrarlo.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1965"/>
        <location filename="../src/widgets/main_window.cpp" line="1991"/>
        <source>Rename file</source>
        <translation>Renombrar archivo</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1966"/>
        <location filename="../src/widgets/main_window.cpp" line="1992"/>
        <source>New name for file &apos;%1&apos;:</source>
        <translation>Nuevo nombre para el archivo &apos;%1&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1967"/>
        <source>Update existing sprites using this image</source>
        <translation>Actualizar sprites existentes usando esta imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2034"/>
        <source>Unsaved changes</source>
        <translation>Cambios sin guardar</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2035"/>
        <source>All files must be saved before this operation.
Do you want to save them now?</source>
        <translation>Todos los archivos deben ser guardados antes de esta operación.
¿Quieres guardarlos ahora?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapEditor</name>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="14"/>
        <source>Map editor</source>
        <translation>Editor de mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="103"/>
        <source>Map id</source>
        <translation>Id del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="118"/>
        <source>Filename of the map (without extension)</source>
        <translation>Nombre de archivo del mapa (sin extensión)</translation>
    </message>
    <message>
        <source>Open map script</source>
        <translation type="vanished">Abrir script del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="131"/>
        <source>Open map script (F4)</source>
        <translation>Abrir script de mapa (F4)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="134"/>
        <location filename="../src/widgets/map_editor.ui" line="357"/>
        <location filename="../src/widgets/map_editor.ui" line="428"/>
        <location filename="../src/widgets/map_editor.ui" line="498"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="156"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="169"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="176"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="186"/>
        <source>Layers</source>
        <translation>Capas</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="217"/>
        <source>to</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="258"/>
        <source>Set a world</source>
        <translation>Mundo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="265"/>
        <source>A name to group maps together</source>
        <translation>Nombre que permite agrupar los mapas</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="272"/>
        <source>Set a floor</source>
        <translation>Piso</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="293"/>
        <source>Floor number of the map</source>
        <translation>Número de piso del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="321"/>
        <source>Location in its world</source>
        <translation>Posición en su mundo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="341"/>
        <location filename="../src/widgets/map_editor.ui" line="479"/>
        <location filename="../src/widgets/map_editor.cpp" line="241"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="350"/>
        <location filename="../src/widgets/map_editor.ui" line="418"/>
        <location filename="../src/widgets/map_editor.ui" line="488"/>
        <source>Tileset of the map</source>
        <translation>Tileset del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="386"/>
        <source>Patterns</source>
        <translation>Patrones</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="425"/>
        <location filename="../src/widgets/map_editor.ui" line="495"/>
        <source>Edit tileset (Ctrl-T)</source>
        <translation>Editar tileset (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="441"/>
        <location filename="../src/widgets/map_editor.ui" line="511"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="465"/>
        <source>Contour generator</source>
        <translation>Generador de contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="471"/>
        <source>Generate contour tiles around the selection</source>
        <translation>Generar tiles de contorno alrededor de la selección</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="520"/>
        <source>Contour</source>
        <translation>Contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="570"/>
        <source>Generate contour tiles around the selection (Ctrl+B)</source>
        <translation>Generar tiles de contorno alrededor de la selección (Ctrl+B)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="573"/>
        <source>Generate tiles</source>
        <translation>Generar tiles</translation>
    </message>
    <message>
        <source>Refresh tileset</source>
        <translation type="vanished">Recargar tileset</translation>
    </message>
    <message>
        <source>Edit tileset</source>
        <translation type="vanished">Editar tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="331"/>
        <location filename="../src/widgets/map_editor.cpp" line="259"/>
        <source>Music</source>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="113"/>
        <source>Map size</source>
        <translation>Tamaño del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="131"/>
        <source>Lowest layer</source>
        <translation>Capa más baja</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="159"/>
        <source>Highest layer</source>
        <translation>Capa más alta</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="187"/>
        <source>Map world</source>
        <translation>Mundo del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="205"/>
        <source>Map floor</source>
        <translation>Piso del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="223"/>
        <source>Map location</source>
        <translation>Posición del mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="277"/>
        <source>Edit entity</source>
        <translation>Editar entidad</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="362"/>
        <source>Move entities</source>
        <translation>Mover entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="414"/>
        <source>Resize entities</source>
        <translation>Redimensionar entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="496"/>
        <location filename="../src/widgets/map_editor.cpp" line="553"/>
        <source>Convert tiles</source>
        <translation>Convertir tiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="615"/>
        <source>Change pattern</source>
        <translation>Cambiar patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="669"/>
        <source>Set direction</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="731"/>
        <source>Set layer</source>
        <translation>Capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="775"/>
        <source>Increment layer</source>
        <translation>Incrementar capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="820"/>
        <source>Decrement layer</source>
        <translation>Disminuir capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="865"/>
        <source>Bring to front</source>
        <translation>Traer al frente</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="923"/>
        <source>Bring to back</source>
        <translation>Poner al fondo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="983"/>
        <source>Add entities</source>
        <translation>Añadir entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1030"/>
        <source>Delete entities</source>
        <translation>Eliminar entidades</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1085"/>
        <source>File &apos;%1&apos; is not a map</source>
        <translation>El archivo &apos;%1&apos; no es un mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1090"/>
        <source>Map %1</source>
        <translation>Mapa %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1093"/>
        <source>Map &apos;%1&apos; has been modified. Save changes?</source>
        <translation>El mapa &apos;%1&apos; ha sido modificado. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1105"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1121"/>
        <source>&lt;No music&gt;</source>
        <translation>&lt;Sin música&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1122"/>
        <source>&lt;Same as before&gt;</source>
        <translation>&lt;Igual que antes&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1127"/>
        <location filename="../src/widgets/map_editor.cpp" line="1131"/>
        <source>(Tileset of the map)</source>
        <translation>(Tileset del mapa)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1140"/>
        <source>Width of the map in pixels</source>
        <translation>Anchura del mapa en píxeles</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1141"/>
        <source>Height of the map in pixels</source>
        <translation>Altura del mapa en píxeles</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1145"/>
        <location filename="../src/widgets/map_editor.cpp" line="1146"/>
        <source>Coordinates of the map in its world (useful to make adjacent scrolling maps)</source>
        <translation>Coordenadas del mapa en su mundo (útil para hacer scrolling entre mapas adyacentes)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1294"/>
        <source>Add tile</source>
        <translation>Añadir tile</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1295"/>
        <source>Add destination</source>
        <translation>Añadir destino</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1296"/>
        <source>Add teletransporter</source>
        <translation>Añadir teletransportador</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1297"/>
        <source>Add pickable</source>
        <translation>Añadir recogible</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1298"/>
        <source>Add destructible</source>
        <translation>Añadir destructible</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1299"/>
        <source>Add chest</source>
        <translation>Añadir cofre</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1300"/>
        <source>Add jumper</source>
        <translation>Añadir saltador</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1301"/>
        <source>Add enemy</source>
        <translation>Añadir enemigo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1302"/>
        <source>Add non-playing character</source>
        <translation>Añadir personaje no-jugador</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1303"/>
        <source>Add block</source>
        <translation>Añadir bloque</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1304"/>
        <source>Add switch</source>
        <translation>Añadir botón</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1305"/>
        <source>Add wall</source>
        <translation>Añadir pared</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1306"/>
        <source>Add sensor</source>
        <translation>Añadir sensor</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1307"/>
        <source>Add crystal</source>
        <translation>Añadir cristal</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1308"/>
        <source>Add crystal block</source>
        <translation>Añadir bloque de cristal</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1309"/>
        <source>Add shop treasure</source>
        <translation>Añadir artículo de tienda</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1310"/>
        <source>Add stream</source>
        <translation>Añadir flujo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1311"/>
        <source>Add door</source>
        <translation>Añadir puerta</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1312"/>
        <source>Add stairs</source>
        <translation>Añadir escaleras</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1313"/>
        <source>Add separator</source>
        <translation>Añadir separador</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1314"/>
        <source>Add custom entity</source>
        <translation>Añadir entidad custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1367"/>
        <source>Save map as PNG file</source>
        <translation>Guardar mapa como archivo PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1369"/>
        <source>PNG image (*.png)</source>
        <translation>Imagen PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1535"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1606"/>
        <location filename="../src/widgets/map_editor.cpp" line="1677"/>
        <source>Layer not empty</source>
        <translation>Capa no vacía</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1607"/>
        <location filename="../src/widgets/map_editor.cpp" line="1678"/>
        <source>This layer is not empty: %1 entities will be destroyed.</source>
        <translation>Esta capa no está vacía: %1 entidades serán destruídas.</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2151"/>
        <source>%1,%2,%3 </source>
        <translation>%1,%2,%3 </translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2459"/>
        <source>Cannot generate tiles: some patterns of the contour &apos;%1&apos; are missing.
Please fix it in the tileset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1,%2 </source>
        <translation type="vanished">%1,%2 </translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2138"/>
        <source> - %1</source>
        <translation> - %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2140"/>
        <source>: %1</source>
        <translation>: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapModel</name>
    <message>
        <location filename="../src/entities/block.cpp" line="34"/>
        <location filename="../src/entities/npc.cpp" line="34"/>
        <source>Any</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="34"/>
        <source>Initially lowered</source>
        <translation>Inicialmente bajado</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="35"/>
        <source>Initially raised</source>
        <translation>Inicialmente subido</translation>
    </message>
    <message>
        <location filename="../src/entities/destination.cpp" line="35"/>
        <source>Keep the same direction</source>
        <translation>Mantener la misma dirección</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="79"/>
        <source>No direction</source>
        <translation>Sin dirección</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="297"/>
        <source>Unexpected entity type (not allowed in map files): %1</source>
        <translation>Tipo de entidad inesperado (no permitido en archivos de  mapas): %1</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="37"/>
        <source>Generalized NPC (something)</source>
        <translation>PNJ generalizado (algo)</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="38"/>
        <source>Usual NPC (somebody)</source>
        <translation>PNJ normal (alguien)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="33"/>
        <source>Spiral staircase (going upstairs)</source>
        <translation>Escalera espiral (subida)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="34"/>
        <source>Spiral staircase (going downstairs)</source>
        <translation>Escalera espiral (bajada)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="35"/>
        <source>Straight staircase (going upstairs)</source>
        <translation>Escalera recta (subida)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="36"/>
        <source>Straight staircase (going downstairs)</source>
        <translation>Escalera recta (bajada)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="37"/>
        <source>Platform stairs (same map)</source>
        <translation>Escalera de plataforma (mismo mapa)</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="31"/>
        <source>Walkable</source>
        <translation>Botón de suelo</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="32"/>
        <source>Arrow target</source>
        <translation>Blanco para flechas</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="33"/>
        <source>Solid</source>
        <translation>Sólido</translation>
    </message>
    <message>
        <location filename="../src/map_model.cpp" line="51"/>
        <source>Cannot open map data file &apos;%1&apos;</source>
        <translation>No se puede abrir archivo de mapa &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/map_model.cpp" line="102"/>
        <source>Cannot save map data file &apos;%1&apos;</source>
        <translation>No se puede guardar archivo de mapa &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapView</name>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="542"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="550"/>
        <source>Resize</source>
        <translation>Redimensionar</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="551"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="558"/>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tile</source>
        <translation>Convertir en tile dinámica</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="564"/>
        <source>Change pattern...</source>
        <translation>Cambiar patrón...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="571"/>
        <source>Change pattern of similar tiles...</source>
        <translation>Cambiar patrón de tiles similares...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="577"/>
        <source>Generate borders around selection</source>
        <translation>Generar bordes alrededor de la selección</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="578"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="587"/>
        <source>One layer up</source>
        <translation>Subir una capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="588"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="597"/>
        <source>One layer down</source>
        <translation>Bajar una capa</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="598"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="607"/>
        <source>Bring to front</source>
        <translation>Traer al frente</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="608"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="617"/>
        <source>Bring to back</source>
        <translation>Llevar al fondo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="618"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="627"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="634"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="663"/>
        <source>Layer %1</source>
        <translation>Capa %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tiles</source>
        <translation>Convertir en tiles dinámicas</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tile</source>
        <translation>Convertir en tile estática</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tiles</source>
        <translation>Convertir en tiles estáticas</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="814"/>
        <source>Direction</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="833"/>
        <location filename="../src/widgets/map_view.cpp" line="841"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="834"/>
        <location filename="../src/widgets/map_view.cpp" line="843"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="835"/>
        <location filename="../src/widgets/map_view.cpp" line="845"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="836"/>
        <location filename="../src/widgets/map_view.cpp" line="847"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="842"/>
        <source>Right-up</source>
        <translation>Derecha-arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="844"/>
        <source>Left-up</source>
        <translation>Izquierda-arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="846"/>
        <source>Left-down</source>
        <translation>Izquierda-abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="848"/>
        <source>Right-down</source>
        <translation>Derecha-abajo</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MovingPatternsState</name>
    <message>
        <source>Move here</source>
        <translation type="obsolete">Mover aquí</translation>
    </message>
    <message>
        <source>Duplicate here</source>
        <translation type="obsolete">Duplicar aquí</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MusicChooser</name>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="161"/>
        <source>Play music</source>
        <translation>Reproducir música</translation>
    </message>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="182"/>
        <source>Stop music</source>
        <translation>Detener música</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewElementDialog</name>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="14"/>
        <source>New element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="22"/>
        <source>License:</source>
        <translation type="unfinished">Licencia:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="29"/>
        <location filename="../src/widgets/new_element_dialog.cpp" line="57"/>
        <source>Script name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="36"/>
        <source>Author:</source>
        <translation type="unfinished">Autor:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="53"/>
        <source>New GLSL file</source>
        <translation type="unfinished">Nuevo archivo GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="54"/>
        <source>GLSL file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="56"/>
        <source>New Lua script</source>
        <translation type="unfinished">Nuevo script Lua</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="59"/>
        <source>New folder</source>
        <translation type="unfinished">Nueva carpeta</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="60"/>
        <source>Folder name:</source>
        <translation type="unfinished">Nombre de la carpeta:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewEntityUserPropertyDialog</name>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="14"/>
        <source>New user property</source>
        <translation>Nueva propiedad de usuario</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="24"/>
        <source>Key:</source>
        <translation>Clave:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="34"/>
        <source>Value:</source>
        <translation>Valor:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialog</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog.ui" line="14"/>
        <source>Create New Quest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogContentsPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="17"/>
        <source>Initial Quest Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="20"/>
        <source>Choose the initial set of files for your quest.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="42"/>
        <source>Community Resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="60"/>
        <source>Empty Quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start with default free resources and scripts.&lt;br/&gt;Uses GPL and Creative Commons licenses.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start from no quest data.&lt;br/&gt;You will have to create data files from scratch.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogDirectoryPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="17"/>
        <source>Quest Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="20"/>
        <source>Choose a location on disk for the new quest.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="31"/>
        <source>Browse</source>
        <translation type="unfinished">Navegar</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="40"/>
        <source>Error Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="122"/>
        <source>Directory not empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="123"/>
        <source>The quest directory is not empty, are you sure you wish to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="148"/>
        <source>Select quest directory</source>
        <translation type="unfinished">Seleccionar directorio del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="171"/>
        <source>A quest already exists in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="177"/>
        <source>Parent directory does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogTitlePage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="17"/>
        <source>Quest Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="20"/>
        <source>Enter the title of the new quest.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewResourceElementDialog</name>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="20"/>
        <source>Create resource</source>
        <translation>Crear recurso</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="31"/>
        <source>Resource id (filename)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="41"/>
        <source>[required]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="48"/>
        <source>Description</source>
        <translation type="unfinished">Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="55"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="90"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="104"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="118"/>
        <source>[optional]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="62"/>
        <source>Create with default code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="74"/>
        <source>Ownership</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="83"/>
        <source>Author</source>
        <translation type="unfinished">Autor</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="97"/>
        <source>License (data file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="111"/>
        <source>License (script file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resource id (filename):</source>
        <translation type="vanished">Id de recurso (nombre de archivo):</translation>
    </message>
    <message>
        <source>Licence:</source>
        <translation type="vanished">Licencia:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation type="vanished">Autor:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">Descripción:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="48"/>
        <source>New map</source>
        <translation>Nuevo mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="49"/>
        <source>Map id (filename):</source>
        <translation>Id de mapa (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="56"/>
        <source>New tileset</source>
        <translation>Nuevo tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="57"/>
        <source>Tileset id (filename):</source>
        <translation>Id de tileset (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="62"/>
        <source>New sprite</source>
        <translation>Nuevo sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="63"/>
        <source>Sprite id (filename):</source>
        <translation>Id de sprite (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="68"/>
        <source>New music</source>
        <translation>Nueva música</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="69"/>
        <source>Music id (filename):</source>
        <translation>Id de música (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="74"/>
        <source>New sound</source>
        <translation>Nuevo sonido</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="75"/>
        <source>Sound id (filename):</source>
        <translation>Id de sonido (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="80"/>
        <source>New item</source>
        <translation>Nuevo ítem</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="81"/>
        <source>Item id (filename):</source>
        <translation>Id de ítem (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="87"/>
        <source>New enemy</source>
        <translation>Nuevo enemigo</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="88"/>
        <source>Enemy id (filename):</source>
        <translation>Id de enemigo (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="94"/>
        <source>New custom entity</source>
        <translation>Nueva entidad custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="95"/>
        <source>Custom entity id (filename):</source>
        <translation>Id de entidad custom (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="101"/>
        <source>New language</source>
        <translation>Nuevo idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="102"/>
        <source>Language id (filename):</source>
        <translation>Id de idioma (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="107"/>
        <source>New font</source>
        <translation>Nueva fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="108"/>
        <source>Font id (filename):</source>
        <translation>Id de fuente (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="113"/>
        <source>New shader</source>
        <translation>Nuevo shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="114"/>
        <source>Shader id (filename):</source>
        <translation>Id de shader (nombre de archivo):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="129"/>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="140"/>
        <source>License</source>
        <translation type="unfinished">Licencia</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewStringDialog</name>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="14"/>
        <source>New string</source>
        <translation>Nuevo texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="24"/>
        <source>New string key:</source>
        <translation>Nueva clave de texto:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="34"/>
        <source>New string value:</source>
        <translation>Nuevo valor de texto:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PackageDialog</name>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="14"/>
        <source>Build Solarus Package</source>
        <translation>Crear paquete Solarus</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="27"/>
        <source>Save quest package to:</source>
        <translation>Guardar paquete de proyecto en:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="43"/>
        <source>Browse</source>
        <translation>Navegar</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="69"/>
        <source>Building quest package...</source>
        <translation>Creando paquete de proyecto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="90"/>
        <source>Build successful!</source>
        <translation>¡Creación finalizada!</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="116"/>
        <source>Build failed</source>
        <translation>Creación fallida</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="138"/>
        <source>Exit Code:</source>
        <translation>Código de salida:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="145"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="59"/>
        <source>Build</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="90"/>
        <source>Starting...
</source>
        <translation>Comenzando...
</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="103"/>
        <source>Crashed</source>
        <translation>Detenido</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="115"/>
        <source>Quest package location:</source>
        <translation>Ruta del paquete de proyecto:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="117"/>
        <source>Solarus Packages (*.solarus)</source>
        <translation>Paquetes Solarus (*.solarus)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PatternPickerDialog</name>
    <message>
        <location filename="../src/widgets/pattern_picker_dialog.ui" line="14"/>
        <source>Pick a pattern</source>
        <translation>Elige un patrón</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Quest</name>
    <message>
        <location filename="../src/quest.cpp" line="140"/>
        <source>No quest</source>
        <translation>No hay proyecto</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="145"/>
        <source>Missing Solarus version in quest.dat</source>
        <translation>No se encuentra la versión de Solarus en quest.dat</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="289"/>
        <source>Unknown resource type</source>
        <translation>Tipo de recurso desconocido</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1196"/>
        <source>Empty file name</source>
        <translation>Nombre de archivo vacío</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1199"/>
        <source>Invalid file name: &apos;%1&apos;</source>
        <translation>Nombre de archivo inválido: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1234"/>
        <source>File &apos;%1&apos; is not in this quest</source>
        <translation>El archivo &apos;%1&apos; no está en este proyecto</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1257"/>
        <source>File &apos;%1&apos; does not exist</source>
        <translation>El archivo &apos;%1&apos; no existe</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1270"/>
        <source>File &apos;%1&apos; already exists</source>
        <translation>El archivo &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1293"/>
        <source>File &apos;%1&apos; is not a folder</source>
        <translation>El archivo &apos;%1&apos; no es una carpeta</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1306"/>
        <source>File &apos;%1&apos; is a folder</source>
        <translation>El archivo &apos;%1&apos; es una carpeta</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1331"/>
        <source>Wrong script name: &apos;%1&apos; (should end with &apos;.lua&apos;)</source>
        <translation>Nombre de script incorrecto: &apos;%1&apos; (debería acabar en &apos;.lua&apos;)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1357"/>
        <source>Wrong GLSL shader file name: &apos;%1&apos; (should end with &apos;.glsl&apos;)</source>
        <translation>Nombre de shader GLSL incorrecto: &apos;%1&apos; (debería acabar en &apos;.glsl&apos;)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1392"/>
        <source>Wrong image file name: &apos;%1&apos; (should end with &apos;.png&apos;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1407"/>
        <source>Cannot create file &apos;%1&apos;</source>
        <translation>No se puede crear el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1456"/>
        <source>Cannot read file &apos;%1&apos;</source>
        <translation>No se puede leer el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1779"/>
        <source>Cannot load image file: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1784"/>
        <source>This sprite already exists: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2268"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>No se pudo eliminar el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2274"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>No se pudo eliminar el directorio &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1427"/>
        <location filename="../src/quest.cpp" line="1464"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>No se puede escribir en el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1806"/>
        <source>Cannot create folder &apos;%1&apos;: parent folder does not exist</source>
        <translation>No se puede crear la carpeta &apos;%1&apos;: el directorio padre no existe</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1812"/>
        <location filename="../src/quest.cpp" line="1849"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>No se puede crear la carpeta &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1982"/>
        <source>Resource &apos;%1&apos; already exists</source>
        <translation>El recurso &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1999"/>
        <location filename="../src/quest.cpp" line="2053"/>
        <source>Cannot rename file &apos;%1&apos;</source>
        <translation>No se puede renombrar el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2121"/>
        <source>Same source and destination id</source>
        <translation>Id de fuente y destino idénticos</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2128"/>
        <source>A resource with id &apos;%1&apos; already exists</source>
        <translation>Ya existe un recurso con el id &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2133"/>
        <location filename="../src/quest.cpp" line="2177"/>
        <source>No such resource: &apos;%1&apos;</source>
        <translation>No existe el recurso: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2191"/>
        <source>Cannot delete file &apos;%1&apos;</source>
        <translation>No se puede eliminar el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2231"/>
        <source>Cannot delete folder &apos;%1&apos;</source>
        <translation>No se puede eliminar la carpeta &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestDatabase</name>
    <message>
        <location filename="../src/quest_database.cpp" line="36"/>
        <source>Map</source>
        <comment>resource_type</comment>
        <extracomment>To describe the type of resource itself like: this is a Map.</extracomment>
        <translatorcomment>resource_type
Para describir el tipo del propio recurso como: esto es un Mapa.</translatorcomment>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="37"/>
        <source>Tileset</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="38"/>
        <source>Sprite</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="39"/>
        <source>Music</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="40"/>
        <source>Sound</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Sonido</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="41"/>
        <source>Item</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Ítem</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="42"/>
        <source>Enemy</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Enemigo</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="43"/>
        <source>Custom entity</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Entidad custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="44"/>
        <source>Language</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="45"/>
        <source>Font</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="46"/>
        <source>Shader</source>
        <comment>resource_type</comment>
        <translatorcomment>resource_type</translatorcomment>
        <translation>Shader</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="51"/>
        <source>Map</source>
        <comment>resource_element</comment>
        <extracomment>To be used with a specific element id like: Rename Map X.</extracomment>
        <translatorcomment>resource_element
Para ser usado con un id de elemento específico como: Renombrar Mapa X.</translatorcomment>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="52"/>
        <source>Tileset</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="53"/>
        <source>Sprite</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="54"/>
        <source>Music</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="55"/>
        <source>Sound</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Sonido</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="56"/>
        <source>Item</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Ítem</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="57"/>
        <source>Enemy</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Enemigo</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="58"/>
        <source>Custom entity</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Entidad custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="59"/>
        <source>Language</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="60"/>
        <source>Font</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="61"/>
        <source>Shader</source>
        <comment>resource_element</comment>
        <translatorcomment>resource_element</translatorcomment>
        <translation>Shader</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="65"/>
        <source>Maps folder</source>
        <translation>Directorio de mapas</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="66"/>
        <source>Tilesets folder</source>
        <translation>Directorio de tilesets</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="67"/>
        <source>Sprites folder</source>
        <translation>Directorio de sprites</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="68"/>
        <source>Musics folder</source>
        <translation>Directorio de músicas</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="69"/>
        <source>Sounds folder</source>
        <translation>Directorio de sonidos</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="70"/>
        <source>Items folder</source>
        <translation>Directorio de ítems</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="71"/>
        <source>Enemies folder</source>
        <translation>Directorio de enemigos</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="72"/>
        <source>Custom entities folder</source>
        <translation>Directorio de entidades custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="73"/>
        <source>Languages folder</source>
        <translation>Directorio de idiomas</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="74"/>
        <source>Fonts folder</source>
        <translation>Directorio de fuentes</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="75"/>
        <source>Shaders folder</source>
        <translation>Directorio de shaders</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="79"/>
        <source>New map...</source>
        <translation>Nuevo mapa...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="80"/>
        <source>New tileset...</source>
        <translation>Nuevo tileset...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="81"/>
        <source>New sprite...</source>
        <translation>Nuevo sprite...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="82"/>
        <source>New music...</source>
        <translation>Nueva música...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="83"/>
        <source>New sound...</source>
        <translation>Nuevo sonido...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="84"/>
        <source>New item...</source>
        <translation>Nuevo ítem...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="85"/>
        <source>New enemy breed...</source>
        <translation>Nuevo modelo de enemigo...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="86"/>
        <source>New custom entity model...</source>
        <translation>Nuevo modelo de entidad custom...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="87"/>
        <source>New language...</source>
        <translation>Nuevo idioma...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="88"/>
        <source>New font...</source>
        <translation>Nueva fuente...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="89"/>
        <source>New shader...</source>
        <translation>Nuevo shader...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="125"/>
        <source>No quest</source>
        <translation>No hay proyecto</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="130"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>No se puede escribir en el archivo &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestFilesModel</name>
    <message>
        <source>Resource</source>
        <translation type="vanished">Recurso</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="359"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="362"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="365"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="368"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="371"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="591"/>
        <source>Quest</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="596"/>
        <source>Main Lua script</source>
        <translation>Script Lua principal</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="610"/>
        <source>Dialogs file</source>
        <translation>Archivo de diálogos</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="614"/>
        <source>Strings file</source>
        <translation>Archivo de textos</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="622"/>
        <source>Map script</source>
        <translation>Script de mapa</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="625"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="629"/>
        <source>GLSL shader code</source>
        <translation>Código shader GLSL</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="637"/>
        <source>Tileset tiles image</source>
        <translation>Imagen de tiles de tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="641"/>
        <source>Tileset sprites image</source>
        <translation>Imagen de sprites de tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="644"/>
        <source>Image</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="649"/>
        <source>Data file</source>
        <translation>Archivo de datos</translation>
    </message>
    <message>
        <source>Lua script</source>
        <translation type="vanished">script Lua</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="778"/>
        <source>%1 (file not found)</source>
        <translation>%1 (archivo no encontrado)</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="788"/>
        <source>%1 (not in the quest)</source>
        <translation>%1 (no está en el proyecto)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestProperties</name>
    <message>
        <location filename="../src/quest_properties.cpp" line="47"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="58"/>
        <source>No quest</source>
        <translation>No hay proyecto</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="63"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>No se puede escribir en el archivo &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestPropertiesEditor</name>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="62"/>
        <source>Quest information</source>
        <translation>Información del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="81"/>
        <source>Solarus version</source>
        <translation>Versión de Solarus</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="88"/>
        <source>Version of the engine your data files are compatible with</source>
        <translation>Versión del motor compatible con tus archivos</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="103"/>
        <source>Write directory</source>
        <translation>Directorio de guardado</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="115"/>
        <source>Folder where to write savegames, relative to &quot;$HOME/.solarus/&quot;.
Must identify your quest to avoid confusion with other quests.</source>
        <translation>Carpeta donde se guardan las partidas, relativo a &quot;$HOME/.solarus/&quot;.
Debe identificar tu proyecto para evitar confusión con otros proyectos.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="133"/>
        <source>Quest title</source>
        <translation>Título del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="145"/>
        <source>The name of your quest.</source>
        <translation>El nombre de tu proyecto.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="159"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="171"/>
        <source>One line describing your quest.</source>
        <translation>Describe tu proyecto en una línea.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="185"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="224"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="236"/>
        <source>People who develop this quest.</source>
        <translation>Gente que desarrolla este proyecto.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="250"/>
        <source>Quest version</source>
        <translation>Versión del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="262"/>
        <source>Current release of your quest.</source>
        <translation>Versión actual de tu proyecto.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="276"/>
        <source>Release date</source>
        <translation>Fecha de lanzamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="287"/>
        <source>In progress</source>
        <translation>En desarrollo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="294"/>
        <source>Released</source>
        <translation>Lanzado el</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="329"/>
        <source>Status and date of the current release.</source>
        <translation>Estado y fecha de la versión actual.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="343"/>
        <source>Website</source>
        <translation>Sitio web</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="355"/>
        <source>Official website of the quest.</source>
        <translation>Sitio web oficial del proyecto.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="378"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="389"/>
        <source>Video options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="398"/>
        <source>Smooth camera scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="405"/>
        <source>Enables subpixel smoothing of camera scrolling movement.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="415"/>
        <source>Synchronize update rate with video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="422"/>
        <source>Keeps the main loop update rate the same as the video refresh rate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="210"/>
        <source>A more detailed description of your quest.</source>
        <translation>Una descripción más detallada de tu proyecto.</translation>
    </message>
    <message>
        <source>Quest size</source>
        <translation type="vanished">Tamaño del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="436"/>
        <source>Normal quest size</source>
        <translation>Tamaño normal del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="466"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="555"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="640"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="507"/>
        <source>Size of the logical game area (before any scaling).
This will be the visible space of the current map.</source>
        <translation>Tamaño del área de proyecto (antes de cualquier aumento).
Esto será el espacio visible del mapa actual.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="525"/>
        <source>Minimum quest size</source>
        <translation>Tamaño mínimo del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="596"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="681"/>
        <source>Only useful to support a range of logical sizes.</source>
        <translation>Útil únicamente para permitir varios tamaños.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="610"/>
        <source>Maximum quest size</source>
        <translation>Tamaño máximo del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="63"/>
        <source>Change write directory</source>
        <translation>Cambiar directorio de guardado</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="93"/>
        <source>Change title</source>
        <translation>Cambiar título</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="122"/>
        <source>Change summary</source>
        <translation>Cambiar resumen</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="148"/>
        <source>Change detailed description</source>
        <translation>Cambiar descripción detallada</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="174"/>
        <source>Change author</source>
        <translation>Cambiar autor</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="200"/>
        <source>Change quest version</source>
        <translation>Cambiar versión del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="226"/>
        <source>Change release date</source>
        <translation>Cambiar fecha de lanzamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="252"/>
        <source>Change website</source>
        <translation>Cambiar sitio web</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="279"/>
        <source>Change normal size</source>
        <translation>Cambiar tamaño normal</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="309"/>
        <source>Change minimum size</source>
        <translation>Cambiar tamaño mínimo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="353"/>
        <source>Change maximum size</source>
        <translation>Cambiar tamaño máximo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="397"/>
        <source>Set subpixel camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="424"/>
        <source>Set dynamic timestep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="463"/>
        <source>Quest properties</source>
        <translation>Propiedades del proyecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="466"/>
        <source>Quest properties have been modified. Save changes?</source>
        <translation>Las propiedades del proyecto han sido modificadas. ¿Guardar cambios?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestResources</name>
    <message>
        <source>Map</source>
        <comment>resource_type</comment>
        <extracomment>To describe the type of resource itself like: New Map.</extracomment>
        <translation type="vanished">Mapa</translation>
    </message>
    <message>
        <source>Tileset</source>
        <comment>resource_type</comment>
        <translation type="vanished">Tileset</translation>
    </message>
    <message>
        <source>Sprite</source>
        <comment>resource_type</comment>
        <translation type="vanished">Sprite</translation>
    </message>
    <message>
        <source>Music</source>
        <comment>resource_type</comment>
        <translation type="vanished">Música</translation>
    </message>
    <message>
        <source>Sound</source>
        <comment>resource_type</comment>
        <translation type="vanished">Sonido</translation>
    </message>
    <message>
        <source>Item</source>
        <comment>resource_type</comment>
        <translation type="vanished">Ítem</translation>
    </message>
    <message>
        <source>Enemy</source>
        <comment>resource_type</comment>
        <translation type="vanished">Enemigo</translation>
    </message>
    <message>
        <source>Custom entity</source>
        <comment>resource_type</comment>
        <translation type="vanished">Entidad custom</translation>
    </message>
    <message>
        <source>Language</source>
        <comment>resource_type</comment>
        <translation type="vanished">Idioma</translation>
    </message>
    <message>
        <source>Font</source>
        <comment>resource_type</comment>
        <translation type="vanished">Fuente</translation>
    </message>
    <message>
        <source>Map</source>
        <comment>resource_element</comment>
        <extracomment>To be used with a specific element id like: Rename Map X.</extracomment>
        <translation type="vanished">el Mapa</translation>
    </message>
    <message>
        <source>Tileset</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Tileset</translation>
    </message>
    <message>
        <source>Sprite</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Sprite</translation>
    </message>
    <message>
        <source>Music</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Música</translation>
    </message>
    <message>
        <source>Sound</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Sonido</translation>
    </message>
    <message>
        <source>Item</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Ítem</translation>
    </message>
    <message>
        <source>Enemy</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Enemigo</translation>
    </message>
    <message>
        <source>Custom entity</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Entidad custom</translation>
    </message>
    <message>
        <source>Language</source>
        <comment>resource_element</comment>
        <translation type="vanished">el Idioma</translation>
    </message>
    <message>
        <source>Font</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Fuente</translation>
    </message>
    <message>
        <source>Map folder</source>
        <translation type="vanished">Carpeta de mapas</translation>
    </message>
    <message>
        <source>Tileset folder</source>
        <translation type="vanished">Carpeta de tileset</translation>
    </message>
    <message>
        <source>Sprite folder</source>
        <translation type="vanished">Carpeta de sprites</translation>
    </message>
    <message>
        <source>Music folder</source>
        <translation type="vanished">Carpeta de música</translation>
    </message>
    <message>
        <source>Sound folder</source>
        <translation type="vanished">Carpeta de sonidos</translation>
    </message>
    <message>
        <source>Item folder</source>
        <translation type="vanished">Carpeta de ítems</translation>
    </message>
    <message>
        <source>Enemy folder</source>
        <translation type="vanished">Carpeta de enemigos</translation>
    </message>
    <message>
        <source>Custom entity folder</source>
        <translation type="vanished">Carpeta de entidades custom</translation>
    </message>
    <message>
        <source>Language folder</source>
        <translation type="vanished">Carpeta de idiomas</translation>
    </message>
    <message>
        <source>Font folder</source>
        <translation type="vanished">Carpeta de fuentes</translation>
    </message>
    <message>
        <source>New map...</source>
        <translation type="vanished">Nuevo mapa...</translation>
    </message>
    <message>
        <source>New tileset...</source>
        <translation type="vanished">Nuevo tileset...</translation>
    </message>
    <message>
        <source>New sprite...</source>
        <translation type="vanished">Nuevo sprite...</translation>
    </message>
    <message>
        <source>New music...</source>
        <translation type="vanished">Nueva música...</translation>
    </message>
    <message>
        <source>New sound...</source>
        <translation type="vanished">Nuevo sonido...</translation>
    </message>
    <message>
        <source>New item...</source>
        <translation type="vanished">Nuevo ítem...</translation>
    </message>
    <message>
        <source>New enemy breed...</source>
        <translation type="vanished">Nuevo modelo de enemigo...</translation>
    </message>
    <message>
        <source>New custom entity model...</source>
        <translation type="vanished">Nuevo modelo de entidad custom...</translation>
    </message>
    <message>
        <source>New language...</source>
        <translation type="vanished">Nuevo idioma...</translation>
    </message>
    <message>
        <source>New font...</source>
        <translation type="vanished">Nueva fuente...</translation>
    </message>
    <message>
        <source>No quest</source>
        <translation type="vanished">No hay proyecto</translation>
    </message>
    <message>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation type="vanished">No se puede escribir en el archivo &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestTreeView</name>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="59"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="503"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="511"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="65"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="544"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="72"/>
        <source>Rename...</source>
        <translation>Renombrar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="73"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="80"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="87"/>
        <source>Author and license...</source>
        <translation>Autor y licencia...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="88"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="405"/>
        <source>Add to quest as %1...</source>
        <translation>Añadir al proyecto como %1...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="429"/>
        <source>New sprite from image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="443"/>
        <source>New GLSL file...</source>
        <translation>Nuevo archivo GLSL...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="458"/>
        <source>New folder...</source>
        <translation>Nueva carpeta...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="466"/>
        <source>New script...</source>
        <translation>Nuevo script...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="499"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="565"/>
        <source>Open Script</source>
        <translation>Abrir Script</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="576"/>
        <source>Open Dialogs</source>
        <translation>Abrir Diálogos</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="582"/>
        <source>Open Strings</source>
        <translation>Abrir los Textos</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="625"/>
        <source>Open Properties</source>
        <translation>Abrir Propiedades</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="631"/>
        <source>Explore folder</source>
        <translation>Explorar directorio</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="680"/>
        <source>Change description...</source>
        <translation>Cambiar descripción...</translation>
    </message>
    <message>
        <source>New folder</source>
        <translation type="vanished">Nueva carpeta</translation>
    </message>
    <message>
        <source>Folder name:</source>
        <translation type="vanished">Nombre de la carpeta:</translation>
    </message>
    <message>
        <source>New Lua script</source>
        <translation type="vanished">Nuevo script Lua</translation>
    </message>
    <message>
        <source>File name:</source>
        <translation type="vanished">Nombre de archivo:</translation>
    </message>
    <message>
        <source>New GLSL file</source>
        <translation type="vanished">Nuevo archivo GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1101"/>
        <source>Change description</source>
        <translation>Cambiar descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1102"/>
        <source>New description for %1 &apos;%2&apos;:</source>
        <translation>Nueva descripción para %1 &apos;%2&apos;:</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1139"/>
        <source>File information for &apos;%1&apos;</source>
        <translation>Información de archivo para &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1141"/>
        <source>File information for %1 selected items</source>
        <translation>Información de archivo para &apos;%1&apos; ítems seleccionados</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1222"/>
        <source>Do you really want to delete &apos;%1&apos;?</source>
        <translation>¿De verdad quieres eliminar &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1223"/>
        <source>Do you really want to delete these %1 items?</source>
        <translation>¿De verdad quieres eliminar estos &apos;%1&apos; ítems?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1225"/>
        <source>Delete confirmation</source>
        <translation>Confirmar eliminación</translation>
    </message>
    <message>
        <source>Do you really want to delete %1 &apos;%2&apos;?</source>
        <translation type="vanished">¿De verdad quieres eliminar %1 &apos;%2&apos;?</translation>
    </message>
    <message>
        <source>Folder is not empty</source>
        <translation type="vanished">La carpeta no está vacía</translation>
    </message>
    <message>
        <source>Do you really want to delete folder &apos;%1&apos;?</source>
        <translation type="vanished">¿De verdad quieres eliminar la carpeta &apos;%1&apos;?</translation>
    </message>
    <message>
        <source>Do you really want to delete file &apos;%1&apos;?</source>
        <translation type="vanished">¿De verdad quieres eliminar el archivo &apos;%1&apos;?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SettingsDialog</name>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="14"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="24"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="30"/>
        <source>Appaerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="36"/>
        <source>Theme:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="50"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="55"/>
        <source>Light mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="60"/>
        <source>Dark mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="84"/>
        <source>Files</source>
        <translation>Archivos</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="92"/>
        <source>Working directory:</source>
        <translation>Directorio de trabajo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="102"/>
        <source>Browse...</source>
        <translation>Explorar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="111"/>
        <source>Restore open tabs at startup</source>
        <translation>Restaurar pestañas abiertas al arrancar</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="121"/>
        <source>Running</source>
        <translation>Al ejecutar</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="129"/>
        <source>Save modified files before running:</source>
        <translation>Guardar archivos modificados antes de ejecutar:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="137"/>
        <source>Ask</source>
        <translation>Preguntar</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="142"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="147"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="170"/>
        <source>No audio</source>
        <translation>Sin audio</translation>
    </message>
    <message>
        <source>Video acceleration</source>
        <translation type="vanished">Aceleración de vídeo</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="179"/>
        <source>Quest size:</source>
        <translation>Tamaño de proyecto:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="191"/>
        <source>Force Software Rendering</source>
        <translation>Forzar renderizado por software</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="198"/>
        <source>Suspend when unfocused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="222"/>
        <source>Text editor</source>
        <translation>Editor de textos</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="228"/>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="234"/>
        <source>Family:</source>
        <translation>Familia:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="268"/>
        <source>Size:</source>
        <translation>Tamaño:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="292"/>
        <source>Tabulation</source>
        <translation>Tabulación</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="298"/>
        <source>Length:</source>
        <translation>Longitud:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="331"/>
        <source>Replace by space</source>
        <translation>Sustituir por espacios</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="354"/>
        <source>External Editor</source>
        <translation>Editor externo</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="366"/>
        <source>Command:</source>
        <translation>Comando:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="376"/>
        <source>executable %f %p</source>
        <translation>ejecutable %f %p</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="395"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="419"/>
        <source>Map editor</source>
        <translation>Editor de mapas</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="425"/>
        <location filename="../src/widgets/settings_dialog.ui" line="617"/>
        <source>Main graphics view</source>
        <translation>Vista gráfica principal</translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="vanished">Color de fondo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="433"/>
        <location filename="../src/widgets/settings_dialog.ui" line="571"/>
        <location filename="../src/widgets/settings_dialog.ui" line="625"/>
        <location filename="../src/widgets/settings_dialog.ui" line="772"/>
        <location filename="../src/widgets/settings_dialog.ui" line="912"/>
        <source>Default zoom:</source>
        <translation>Zoom predeterminado:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="458"/>
        <location filename="../src/widgets/settings_dialog.ui" line="650"/>
        <location filename="../src/widgets/settings_dialog.ui" line="937"/>
        <source>Grid:</source>
        <translation>Grid:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="491"/>
        <location filename="../src/widgets/settings_dialog.ui" line="693"/>
        <location filename="../src/widgets/settings_dialog.ui" line="824"/>
        <location filename="../src/widgets/settings_dialog.ui" line="970"/>
        <source>Show at opening</source>
        <translation>Mostrar al abrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="508"/>
        <location filename="../src/widgets/settings_dialog.ui" line="716"/>
        <location filename="../src/widgets/settings_dialog.ui" line="987"/>
        <source>Default size:</source>
        <translation>Tamaño predeterminado:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="515"/>
        <location filename="../src/widgets/settings_dialog.ui" line="723"/>
        <location filename="../src/widgets/settings_dialog.ui" line="994"/>
        <source>Style:</source>
        <translation>Estilo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="522"/>
        <location filename="../src/widgets/settings_dialog.ui" line="730"/>
        <location filename="../src/widgets/settings_dialog.ui" line="831"/>
        <location filename="../src/widgets/settings_dialog.ui" line="1001"/>
        <source>Color:</source>
        <translation>Color:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="565"/>
        <source>Tileset graphics view</source>
        <translation>Vista gráfica del tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="611"/>
        <source>Sprite editor</source>
        <translation>Editor de sprites</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="737"/>
        <source>Auto detect size</source>
        <translation>Detectar tamaño automáticamente</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="764"/>
        <source>Previewer graphics view</source>
        <translation>Previsualización</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="797"/>
        <source>Origin:</source>
        <translation>Origen:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="898"/>
        <source>Tileset editor</source>
        <translation>Editor de tilesets</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="904"/>
        <source>Graphics view</source>
        <translation>Vista gráfica</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="108"/>
        <source>Select external editor</source>
        <translation>Seleccionar editor externo</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="186"/>
        <source>Restore default settings</source>
        <translation>Restablecer configuración predeterminada</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="187"/>
        <source>Do you really want to restore default settings?</source>
        <translation>¿De verdad quieres restaurar la configuración predeterminada?</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="355"/>
        <source>Working directory</source>
        <translation>Directorio de trabajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="994"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="995"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="996"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="997"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="998"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderEditor</name>
    <message>
        <source>Shader properties</source>
        <translation type="vanished">Propiedades de shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="81"/>
        <source>Shader id</source>
        <translation>Id de shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="88"/>
        <source>Filename of the shader program (without extension)</source>
        <translation>Nombre de archivo del programa shader (sin extensión)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="104"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="117"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="124"/>
        <location filename="../src/widgets/shader_editor.cpp" line="68"/>
        <source>Scaling factor</source>
        <translation>Factor de escala</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="153"/>
        <source>Preview settings</source>
        <translation>Configuración de vista previa</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="167"/>
        <source>Preview mode</source>
        <translation>Modo de vista previa</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="188"/>
        <source>Picture</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="202"/>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="213"/>
        <source>Sprite</source>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="275"/>
        <source>Open a PNG file</source>
        <translation>Abrir archivo PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="376"/>
        <source>Animation</source>
        <translation>Animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="393"/>
        <source>Direction</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="463"/>
        <source>Vertex shader</source>
        <translation>Shader de vértices</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="481"/>
        <source>Use a vertex shader</source>
        <translation>Usar un shader de vértices</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="539"/>
        <source>New vertex shader file</source>
        <translation>Nuevo archivo de shader de vértices</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="550"/>
        <source>Save vertex shader file</source>
        <translation>Guardar archivo de shader de vértices</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="566"/>
        <source>Open vertex shader file</source>
        <translation>Abrir archivo de shader de vértices</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="624"/>
        <source>Fragment shader</source>
        <translation>Shader de fragmento</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="642"/>
        <source>Use a fragment shader</source>
        <translation>Usar un shader de fragmento</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="700"/>
        <source>New fragment shader file</source>
        <translation>Nuevo archivo de shader de fragmento</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="711"/>
        <source>Save fragment shader file</source>
        <translation>Guardar archivo de shader de fragmento</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="727"/>
        <source>Open fragment shader file</source>
        <translation>Abrir archivo de shader de fragmento</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="92"/>
        <source>Shader file</source>
        <translation>Archivo shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="158"/>
        <source>File &apos;%1&apos; is not a shader</source>
        <translation>El archivo &apos;%1&apos; no es un shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="163"/>
        <source>Shader %1</source>
        <translation>Shader &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="166"/>
        <source>Shader &apos;%1&apos; has been modified. Save changes?</source>
        <translation>El shader &apos;%1&apos; ha sido modificado. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="426"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="647"/>
        <source>New GLSL file</source>
        <translation>Nuevo archivo GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="648"/>
        <source>File name:</source>
        <translation>Nombre de archivo:</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="688"/>
        <source>Open a GLSL file</source>
        <translation>Abrir archivo GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="690"/>
        <source>GLSL shader file (*.glsl)</source>
        <translation>Archivo de shader GLSL (*.glsl)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="702"/>
        <source>Shader GLSL files must be in the shaders directory</source>
        <translation>Los archivos shader GLSL deben estar en el directorio de shaders</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="836"/>
        <source>Open a PNG picture</source>
        <translation>Abrir imagen PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="838"/>
        <source>PNG file (*.png)</source>
        <translation>Archivo PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderModel</name>
    <message>
        <location filename="../src/shader_model.cpp" line="44"/>
        <source>Cannot open shader data file &apos;%1&apos;</source>
        <translation>No se puede abrir el fichero de shader &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/shader_model.cpp" line="166"/>
        <source>Cannot save shader &apos;%1&apos;</source>
        <translation>No se puede guardar el shader &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SoundChooser</name>
    <message>
        <location filename="../src/widgets/sound_chooser.cpp" line="37"/>
        <source>Play sound</source>
        <translation>Reproducir sonido</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteEditor</name>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="20"/>
        <source>Sprite editor</source>
        <translation>Editor de sprites</translation>
    </message>
    <message>
        <source>Sprite properties</source>
        <translation type="vanished">Propiedades de sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="114"/>
        <source>Sprite id</source>
        <translation>Id de sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="121"/>
        <source>Filename of the sprite (without extension)</source>
        <translation>Nombre de archivo del sprite (sin extensión)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="134"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="141"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="164"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="202"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="240"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="278"/>
        <source>Move up</source>
        <translation>Mover arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="316"/>
        <source>Move down</source>
        <translation>Mover abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="354"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="397"/>
        <source>Animation properties</source>
        <translation>Propiedades de animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="409"/>
        <source>Source image</source>
        <translation>Imagen fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="445"/>
        <source>Source image of the animation</source>
        <translation>Imagen fuente de la animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="461"/>
        <source>Tileset of the animation</source>
        <translation>Tileset de la animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="468"/>
        <source>Refresh image</source>
        <translation>Recargar imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="471"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="488"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="513"/>
        <source>Delay in milliseconds between two frames of the animation</source>
        <translation>Demora en milisegundos entre dos frames de la animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="516"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="532"/>
        <source>Frame delay</source>
        <translation>Demora entre frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="539"/>
        <source>Loop on frame</source>
        <translation>Bucle en el frame</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="549"/>
        <source>Index of the frame where you want the animation to come back when the last frame finishes</source>
        <translation>Índice del frame donde quieres que la animación vuelva cuando el último frame acaba</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="565"/>
        <source>Default animation</source>
        <translation>Animación por defecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="572"/>
        <source>Default animation of the sprite</source>
        <translation>Animación por defecto del sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="575"/>
        <source>Set as default</source>
        <translation>Definir por defecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="585"/>
        <source>Direction properties</source>
        <translation>Propiedades de dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="594"/>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="601"/>
        <source>Origin</source>
        <translation>Origen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="608"/>
        <source>Number of frames</source>
        <translation>Número de frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="615"/>
        <source>Number of columns</source>
        <translation>Número de columnas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="622"/>
        <source>Number of frames of this direction</source>
        <translation>Número de frames en esta dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="635"/>
        <source>Number of columns of the grid containing the frames of this direction in the image</source>
        <translation>Número de columnas del grid que contienen los frames de esta dirección en la imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="648"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="690"/>
        <source>Direction preview</source>
        <translation>Previsualización de la dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="70"/>
        <location filename="../src/widgets/sprite_editor.cpp" line="801"/>
        <source>Create animation</source>
        <translation>Crear animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="107"/>
        <source>Duplicate animation</source>
        <translation>Duplicar animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="119"/>
        <source> (copy)</source>
        <translation> (copia)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="141"/>
        <source>Change animation name</source>
        <translation>Cambiar nombre de la animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="176"/>
        <source>Delete animation</source>
        <translation>Eliminar animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="213"/>
        <source>Change default animation</source>
        <translation>Cambiar animación por defecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="246"/>
        <source>Change source image</source>
        <translation>Cambiar imagen fuente</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="281"/>
        <source>Change frame delay</source>
        <translation>Cambiar demora entre frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="315"/>
        <source>Change loop on frame</source>
        <translation>Cambiar bucle en frame</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="350"/>
        <source>Add direction</source>
        <translation>Añadir dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="387"/>
        <source>Duplicate direction</source>
        <translation>Duplicar dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="422"/>
        <source>Move direction up</source>
        <translation>Mover dirección arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="455"/>
        <source>Move direction down</source>
        <translation>Mover dirección abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="488"/>
        <source>Delete direction</source>
        <translation>Eliminar dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="519"/>
        <source>Change direction size</source>
        <translation>Cambiar tamaño de la dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="554"/>
        <source>Change direction position</source>
        <translation>Cambiar posición de la dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="589"/>
        <source>Change direction origin</source>
        <translation>Cambiar origen de la dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="623"/>
        <source>Change number of frames of direction</source>
        <translation>Cambiar número de frames de dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="659"/>
        <source>Change number of columns of direction</source>
        <translation>Cambiar número de columnas de dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="695"/>
        <source>Change number of frames/columns of direction</source>
        <translation>Cambiar número de frames/columnas de dirección</translation>
    </message>
    <message>
        <source>Change direction num frames</source>
        <translation type="vanished">Cambiar número de frames de la dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="749"/>
        <source>File &apos;%1&apos; is not a sprite</source>
        <translation>El archivo &apos;%1&apos; no es un sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="754"/>
        <source>Sprite %1</source>
        <translation>Sprite %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="757"/>
        <source>Sprite &apos;%1&apos; has been modified. Save changes?</source>
        <translation>El sprite &apos;%1&apos; ha sido modificado. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="780"/>
        <source>Width of each frame in the image</source>
        <translation>Anchura de cada frame en la imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="781"/>
        <source>Height of each frame in the image</source>
        <translation>Altura de cada frame en la imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="785"/>
        <source>X coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>Coordenada X de la esquina superior-izquierda del área que contiene los frames de la imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="786"/>
        <source>Y coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>Coordenada Y de la esquina superior-izquierda del área que contiene los frames de la imagen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="790"/>
        <source>X coordinate of the origin point of the sprite</source>
        <translation>Coordenada X del punto de origen del sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="791"/>
        <source>Y coordinate of the origin point of the sprite</source>
        <translation>Coordenada Y del punto de origen del sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="802"/>
        <source>Create direction</source>
        <translation>Crear dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="992"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteModel</name>
    <message>
        <location filename="../src/sprite_model.cpp" line="51"/>
        <source>Cannot open sprite &apos;%1&apos;</source>
        <translation>No se puede abrir el sprite &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="205"/>
        <source>Cannot save sprite &apos;%1&apos;</source>
        <translation>No se puede guardar el sprite &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="326"/>
        <source> (default)</source>
        <translation> (por defecto)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="349"/>
        <source>(right)</source>
        <translation>(derecha)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="350"/>
        <source>(up)</source>
        <translation>(arriba)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="351"/>
        <source>(left)</source>
        <translation>(izquierda)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="352"/>
        <source>(down)</source>
        <translation>(abajo)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="355"/>
        <source>Direction %1 %2</source>
        <translation>Dirección %1 %2</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="428"/>
        <location filename="../src/sprite_model.cpp" line="487"/>
        <location filename="../src/sprite_model.cpp" line="611"/>
        <source>Animation name cannot be empty</source>
        <translation>El nombre de la animación no puede estar vacío</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="433"/>
        <location filename="../src/sprite_model.cpp" line="492"/>
        <location filename="../src/sprite_model.cpp" line="615"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>La animación &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="549"/>
        <location filename="../src/sprite_model.cpp" line="607"/>
        <location filename="../src/sprite_model.cpp" line="876"/>
        <location filename="../src/sprite_model.cpp" line="931"/>
        <source>Animation &apos;%1&apos; does not exist</source>
        <translation>La animación &apos;%1&apos; no existe</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="984"/>
        <location filename="../src/sprite_model.cpp" line="1047"/>
        <source>Direction %1 does not exist in animation &apos;%2&apos;</source>
        <translation>La dirección %1 no existe en la animación &apos;%2&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpritePreviewer</name>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="33"/>
        <source>Show origin</source>
        <translation>Mostrar origen</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="40"/>
        <location filename="../src/widgets/sprite_previewer.cpp" line="377"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="86"/>
        <source>Index of the current frame</source>
        <translation>Índice del frame actual</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="114"/>
        <source>Last</source>
        <translation>Último</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="146"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="178"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="239"/>
        <source>Previous</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="271"/>
        <source>First</source>
        <translation>Primero</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="187"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="190"/>
        <source>Start</source>
        <translation>Comenzar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="379"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="380"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="381"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="382"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="383"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteScene</name>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="201"/>
        <source>This tileset has no sprite image.
Please select another tileset.</source>
        <translation>Este tileset no tiene imagen de sprite.
Por favor, selecciona otro tileset.</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="207"/>
        <source>Missing source image &apos;%1&apos;</source>
        <translation>No se encuentra la imagen fuente &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="371"/>
        <source>No such direction index: %1</source>
        <translation>No existe la dirección: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteTreeView</name>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="38"/>
        <source>Create animation</source>
        <translation>Crear animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="44"/>
        <source>Create direction</source>
        <translation>Crear dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="50"/>
        <source>Rename animation</source>
        <translation>Renombrar animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="51"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="58"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="65"/>
        <source>Move up</source>
        <translation>Mover arriba</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="71"/>
        <source>Move down</source>
        <translation>Mover abajo</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="77"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <source>Duplicate...</source>
        <translation type="vanished">Duplicar...</translation>
    </message>
    <message>
        <source>Delete...</source>
        <translation type="vanished">Eliminar...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteView</name>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="50"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="57"/>
        <source>Duplicate...</source>
        <translation>Duplicar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="64"/>
        <source>Change the number of frames/columns</source>
        <translation>Cambiar número de frames/columnas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="65"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="73"/>
        <source>Change the number of frames</source>
        <translation>Cambiar número de frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="80"/>
        <source>Change the number of columns</source>
        <translation>Cambiar número de columnas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="613"/>
        <source>New multiframe direction</source>
        <translation>Nueva dirección multiframe</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="622"/>
        <location filename="../src/widgets/sprite_view.cpp" line="682"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="608"/>
        <source>New direction</source>
        <translation>Nueva dirección</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="670"/>
        <source>Move here</source>
        <translation>Mover aquí</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="676"/>
        <source>Duplicate here</source>
        <translation>Duplicar aquí</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsEditor</name>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="14"/>
        <source>Strings editor</source>
        <translation>Editor de textos</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="32"/>
        <source>Language properties</source>
        <translation>Propiedades de idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="41"/>
        <source>Language id</source>
        <translation>Id de idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="48"/>
        <source>Folder name of the language</source>
        <translation>Nombre de la carpeta del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="61"/>
        <source>Language description</source>
        <translation>Descripción del idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="68"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="80"/>
        <source>Compare to language</source>
        <translation>Comparar idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="93"/>
        <source>Refresh language</source>
        <translation>Recargar idioma</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="96"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="145"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="183"/>
        <source>Change key</source>
        <translation>Cambiar clave</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="221"/>
        <source>Duplicate string(s)</source>
        <translation>Duplicar texto(s)</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="259"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="67"/>
        <source>Create string</source>
        <translation>Crear texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="98"/>
        <source>Duplicate strings</source>
        <translation>Duplicar textos</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="129"/>
        <source>Change string key</source>
        <translation>Cambiar clave del texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="162"/>
        <source>Change string key prefix</source>
        <translation>Cambiar prefijo de claves de texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="200"/>
        <source>Delete string</source>
        <translation>Eliminar texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="230"/>
        <source>Delete strings</source>
        <translation>Eliminar textos</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="264"/>
        <source>Change string value</source>
        <translation>Cambiar valor de texto</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="312"/>
        <source>Strings %1</source>
        <translation>Textos %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="315"/>
        <source>Strings &apos;%1&apos; have been modified. Save changes?</source>
        <translation>Los textos &apos;%1&apos; han sido modificados. ¿Guardar cambios?</translation>
    </message>
    <message>
        <source>Strings &apos;%1&apos; has been modified. Save changes?</source>
        <translation type="vanished">Los textos &apos;%1&apos; han sido modificados. ¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="325"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;Ningún idioma&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="440"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="499"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="502"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>El texto &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="576"/>
        <source>Delete confirmation</source>
        <translation>Confirmar eliminación</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="577"/>
        <source>Do you really want to delete all strings prefixed by &apos;%1&apos;?</source>
        <translation>¿De verdad quieres eliminar todos los textos con el prefijo &apos;%1&apos;?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsModel</name>
    <message>
        <location filename="../src/strings_model.cpp" line="45"/>
        <location filename="../src/strings_model.cpp" line="791"/>
        <source>Cannot open strings data file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo de textos &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="83"/>
        <source>Cannot save strings data file &apos;%1&apos;</source>
        <translation>No se puede guardar el archivo de textos &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="236"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="237"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="240"/>
        <source>Translation (%1)</source>
        <translation>Traducción (%1)</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="242"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="392"/>
        <location filename="../src/strings_model.cpp" line="529"/>
        <location filename="../src/strings_model.cpp" line="642"/>
        <source>Invalid string id: &apos;%1&apos;</source>
        <translation>Id de texto inválido: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="521"/>
        <source>String &apos;%1&apos; does not exist</source>
        <translation>El texto &apos;%1&apos; no existe</translation>
    </message>
    <message>
        <source>Invalid string Key: %1</source>
        <translation type="vanished">Clave de texto inválida: %1</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="396"/>
        <location filename="../src/strings_model.cpp" line="459"/>
        <location filename="../src/strings_model.cpp" line="525"/>
        <location filename="../src/strings_model.cpp" line="605"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>El texto &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <source>String &apos;%1&apos; no exists</source>
        <translation type="vanished">El texto &apos;%1&apos; no existe</translation>
    </message>
    <message>
        <source>Invalid string key: %1</source>
        <translation type="vanished">Clave de texto inválida: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsTreeView</name>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="37"/>
        <source>New string...</source>
        <translation>Nuevo texto...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="43"/>
        <source>Duplicate string(s)...</source>
        <translation>Duplicar texto(s)...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="49"/>
        <source>Change key...</source>
        <translation>Cambiar clave...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditor</name>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="59"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="101"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="128"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>No se puede abrir el archivo &apos;%1&apos; para escritura</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditorWidget</name>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="360"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="371"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="382"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="395"/>
        <source>Select all</source>
        <translation>Seleccionar todo</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilePatternsListView</name>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="39"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="46"/>
        <source>Change id...</source>
        <translation>Cambiar id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="47"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetEditor</name>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="14"/>
        <source>Tileset editor</source>
        <translation>Editor de tilesets</translation>
    </message>
    <message>
        <source>Tileset properties</source>
        <translation type="vanished">Propiedades del tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="102"/>
        <source>Tileset id</source>
        <translation>Id del tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="109"/>
        <source>Filename of the tileset (without extension)</source>
        <translation>Nombre de archivo del tileset (sin extensión)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="125"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="138"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Descripción intuitiva para mostrar en el editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="148"/>
        <source>Background</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="161"/>
        <source>Background color applied to maps using this tileset</source>
        <translation>Color de fondo aplicado a mapas que usan este tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="181"/>
        <source>Patterns</source>
        <translation>Patrones</translation>
    </message>
    <message>
        <source>Number of existing tile patterns in the tileset</source>
        <translation type="vanished">Número de patrones de tile existentes en el tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="196"/>
        <source>Selection properties</source>
        <translation>Propiedades de la selección</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="208"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="717"/>
        <source>Pattern id</source>
        <translation>Id del patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="232"/>
        <source>Name identifying the pattern</source>
        <translation>Nombre identificador del patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="242"/>
        <location filename="../src/widgets/tileset_editor.ui" line="488"/>
        <location filename="../src/widgets/tileset_editor.ui" line="585"/>
        <source>Rename (F2)</source>
        <translation>Renombrar (F2)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="277"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="210"/>
        <source>Ground</source>
        <translation>Suelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="290"/>
        <source>Terrain of the pattern</source>
        <translation>Tipo de terreno del patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="300"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="252"/>
        <source>Default layer</source>
        <translation>Capa por defecto</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="320"/>
        <source>Repeatable</source>
        <translation>Repetible</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="337"/>
        <source>Scrolling</source>
        <translation>Desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="391"/>
        <source>Delay</source>
        <translation>Demora</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="398"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="414"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="500"/>
        <source>Mirror loop</source>
        <translation>Bucle espejo</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="425"/>
        <source>Contours</source>
        <translation>Contornos</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="456"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="887"/>
        <source>Create contour</source>
        <translation>Crear contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="563"/>
        <source>Contour properties</source>
        <translation>Propiedades de contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="569"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="588"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="595"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="617"/>
        <source>Positioning</source>
        <translation>Posicionamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="627"/>
        <source>Outside the selection</source>
        <translation>Fuera de la selección</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="636"/>
        <source>Inside the selection</source>
        <translation>Dentro de la selección</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="337"/>
        <source>Animation</source>
        <translation>Animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="350"/>
        <source>Kind of animation of the pattern</source>
        <translation>Tipo de animación del patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="360"/>
        <source>Frames</source>
        <translation>Frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="382"/>
        <source>Horizontal or vertical separation (only for multi-frame patterns)</source>
        <translation>Separación horizontal o vertical (solamente para patrones multi-frame)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="76"/>
        <source>Background color</source>
        <translation>Color de fondo</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="104"/>
        <source>Move pattern</source>
        <translation>Mover patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="138"/>
        <source>Resize pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="172"/>
        <source>Move patterns</source>
        <translation>Mover patrones</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="294"/>
        <source>Repeat mode</source>
        <translation>Modo de repetición</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="381"/>
        <source>Animation separation</source>
        <translation>Separación de animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="420"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="460"/>
        <source>Frame delay</source>
        <translation>Demora entre frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="543"/>
        <source>Create pattern</source>
        <translation>Crear patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="526"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="643"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="579"/>
        <source>Duplicate</source>
        <translation>Duplicar</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="750"/>
        <source>Border set id</source>
        <translation>Id de conjunto de bordes</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="777"/>
        <source>Border set inner</source>
        <translation>Conjunto de bordes interior</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="809"/>
        <source>Border set patterns</source>
        <translation>Patrones de conjunto de bordes</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="838"/>
        <source>Delete contour</source>
        <translation>Borrar contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="915"/>
        <source>Delete contour pattern</source>
        <translation>Borrar patrón de contorno</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="966"/>
        <source>File &apos;%1&apos; is not a tileset</source>
        <translation>El archivo &apos;%1&apos; no es un tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="971"/>
        <source>Tileset %1</source>
        <translation>Tileset %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="974"/>
        <source>Tileset &apos;%1&apos; has been modified. Save changes?</source>
        <translation>El tileset &apos;%1&apos; ha sido modificado.¿Guardar cambios?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1256"/>
        <source>Invalid description</source>
        <translation>Descripción inválida</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1352"/>
        <source>Rename tile pattern</source>
        <translation>Renombrar patrón de tile</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1353"/>
        <source>New pattern id:</source>
        <translation>Nuevo id de patrón:</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1354"/>
        <source>Update existing maps using this pattern</source>
        <translation>Actualizar mapas existentes usando este patrón</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1437"/>
        <source>Cannot open map file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo del mapa &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1445"/>
        <source>Invalid map file: &apos;%1&apos;</source>
        <translation>Archivo de mapa inválido: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1470"/>
        <source>Failed to export map after changing pattern id: &apos;%1&apos;</source>
        <translation>No se pudo exportar mapa tras cambiar el id de patrón: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1478"/>
        <source>Cannot open map file &apos;%1&apos; for writing</source>
        <translation>No se puede abrir el archivo del mapa &apos;%1&apos; para escritura</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1891"/>
        <source>Do you really want to delete pattern &apos;%1&apos;?</source>
        <translation>¿De verdad quieres eliminar el patrón &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1895"/>
        <source>Do you really want to delete these %1 patterns?</source>
        <translation>¿De verdad quieres eliminar estos %1 patrones?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1901"/>
        <source>Delete confirmation</source>
        <translation>Confirmar eliminación</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1938"/>
        <source>Border set name</source>
        <translation>Nombre de conjunto de bordes</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1939"/>
        <source>Border set name:</source>
        <translation>Nombre de conjunto de bordes:</translation>
    </message>
    <message>
        <source>Image was modified externally</source>
        <translation type="vanished">La imagen ha sido modificada externamente</translation>
    </message>
    <message>
        <source>The tileset image was modified.
Do you want to refresh the tileset?</source>
        <translation type="vanished">La imagen del tileset ha sido modificada.
¿Quieres recargar el tileset?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetModel</name>
    <message>
        <location filename="../src/tileset_model.cpp" line="118"/>
        <source>Cannot open tileset data file &apos;%1&apos;</source>
        <translation>No se puede abrir el archivo del tileset &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="165"/>
        <source>Cannot save tileset data file &apos;%1&apos;</source>
        <translation>No se puede guardar el archivo del tileset &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="184"/>
        <source>Failed to refresh tileset: %1</source>
        <translation>No se pudo recargar el tileset: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="432"/>
        <location filename="../src/tileset_model.cpp" line="638"/>
        <source>Invalid tile pattern id: &apos;%1&apos;</source>
        <translation>Id de patrón de tile inválido: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="436"/>
        <location filename="../src/tileset_model.cpp" line="642"/>
        <source>Tile pattern &apos;%1&apos; already exists</source>
        <translation>El patrón del tile &apos;%1&apos; ya existe</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="502"/>
        <location filename="../src/tileset_model.cpp" line="567"/>
        <location filename="../src/tileset_model.cpp" line="634"/>
        <source>Invalid tile pattern index: %1</source>
        <translation>Índice de patrón de tile inválido: %1</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1796"/>
        <source>Contour already exists: &apos;%1&apos;</source>
        <translation>El contorno ya existe: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1800"/>
        <location filename="../src/tileset_model.cpp" line="1851"/>
        <source>Invalid contour id: &apos;%1&apos;</source>
        <translation>Id de contorno inválido: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1806"/>
        <source>Failed to create contour &apos;%1&apos;</source>
        <translation>No se pudo crear el contorno: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1823"/>
        <location filename="../src/tileset_model.cpp" line="1847"/>
        <location filename="../src/tileset_model.cpp" line="1878"/>
        <location filename="../src/tileset_model.cpp" line="1899"/>
        <location filename="../src/tileset_model.cpp" line="1923"/>
        <location filename="../src/tileset_model.cpp" line="1938"/>
        <location filename="../src/tileset_model.cpp" line="1966"/>
        <location filename="../src/tileset_model.cpp" line="1987"/>
        <location filename="../src/tileset_model.cpp" line="2005"/>
        <source>No such contour: &apos;%1&apos;</source>
        <translation>No existe el contorno: &apos;%1</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1829"/>
        <source>Failed to delete contour &apos;%1&apos;</source>
        <translation>No se pudo borrar el contorno: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1855"/>
        <source>Contour id already in use: &apos;%1&apos;</source>
        <translation>La id de contorno ya está en uso: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1861"/>
        <source>Failed to rename contour &apos;%1&apos;</source>
        <translation>No se pudo renombrar el contorno: &apos;%1&apos;</translation>
    </message>
    <message>
        <source>No such tile pattern: %1</source>
        <translation type="vanished">No se encuentra el patrón de tile: %1</translation>
    </message>
    <message>
        <source>Cannot divide the pattern in 3 frames : the size of each frame must be a multiple of 8 pixels</source>
        <translation type="vanished">No se puede dividir el patrón en 3 frames: el tamaño de cada frame debe ser un múltiplo de 8 píxeles</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetScene</name>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="177"/>
        <source>Missing tileset image &apos;%1&apos;</source>
        <translation>No se encuentra la imagen del tileset &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="454"/>
        <source>No such pattern index: %1</source>
        <translation>No existe el índice de patrón: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetView</name>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="182"/>
        <source>Change id...</source>
        <translation>Cambiar id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="183"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="190"/>
        <source>Delete...</source>
        <translation>Eliminar...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="160"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="157"/>
        <source>Resize</source>
        <translation type="unfinished">Redimensionar</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="168"/>
        <source>Create contour...</source>
        <translation>Crear contorno...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="171"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="204"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="205"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="206"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="207"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="667"/>
        <source>Ground</source>
        <translation type="unfinished">Suelo</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="672"/>
        <source>Default layer</source>
        <translation>Capa predeterminada</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="677"/>
        <source>Repeatable</source>
        <translation>Repetible</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="682"/>
        <source>Scrolling</source>
        <translation>Desplazamiento</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1494"/>
        <source>Duplicate here</source>
        <translation>Duplicar aquí</translation>
    </message>
    <message>
        <source>Animation</source>
        <translation type="vanished">Animación</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="756"/>
        <source>Layer %1</source>
        <translation>Capa %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1351"/>
        <source>New pattern (more options)</source>
        <translation>Nuevo patrón (más opciones)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1357"/>
        <source>New pattern (%1)</source>
        <translation>Nuevo patrón (%1)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1367"/>
        <location filename="../src/widgets/tileset_view.cpp" line="1502"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1488"/>
        <source>Move here</source>
        <translation>Mover aquí</translation>
    </message>
</context>
</TS>

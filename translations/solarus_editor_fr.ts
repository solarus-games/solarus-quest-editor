<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/ground_traits.cpp" line="46"/>
        <source>Empty</source>
        <translation>Vide</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="49"/>
        <source>Traversable</source>
        <translation>Traversable</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="46"/>
        <source>Arrow</source>
        <translation>Flèche</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="49"/>
        <source>Block</source>
        <translation>Bloc</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="52"/>
        <source>Bomb</source>
        <translation>Bombe</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="55"/>
        <source>Boomerang</source>
        <translation>Boomerang</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="58"/>
        <source>Camera</source>
        <translation>Caméra</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="61"/>
        <source>Carried object</source>
        <translation>Objet porté</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="64"/>
        <source>Chest</source>
        <translation>Coffre</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="67"/>
        <source>Crystal</source>
        <translation>Cristal</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="70"/>
        <source>Crystal block</source>
        <translation>Plot de cristal</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="73"/>
        <source>Custom entity</source>
        <translation>Entité custom</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="76"/>
        <source>Destination</source>
        <translation>Destination</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="79"/>
        <source>Destructible object</source>
        <translation>Destructible</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="82"/>
        <source>Door</source>
        <translation>Porte</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="85"/>
        <source>Dynamic tile</source>
        <translation>Tile dynamique</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="88"/>
        <source>Enemy</source>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="91"/>
        <source>Explosion</source>
        <translation>Explosion</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="94"/>
        <source>Fire</source>
        <translation>Feu</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="97"/>
        <source>Hero</source>
        <translation>Héros</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="100"/>
        <source>Hookshot</source>
        <translation>Grappin</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="103"/>
        <source>Jumper</source>
        <translation>Sauteur</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="106"/>
        <source>NPC</source>
        <translation>PNJ</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="109"/>
        <source>Pickable treasure</source>
        <translation>Trésor ramassable</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="112"/>
        <source>Sensor</source>
        <translation>Capteur</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="115"/>
        <source>Separator</source>
        <translation>Séparateur</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="118"/>
        <source>Shop treasure</source>
        <translation>Article de magasin</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="121"/>
        <source>Stairs</source>
        <translation>Escaliers</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="124"/>
        <source>Stream</source>
        <translation>Flux</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="127"/>
        <source>Switch</source>
        <translation>Bouton</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="130"/>
        <source>Teletransporter</source>
        <translation>Téléporteur</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="133"/>
        <source>Tile</source>
        <translation>Tile</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="136"/>
        <location filename="../src/ground_traits.cpp" line="52"/>
        <source>Wall</source>
        <translation>Mur</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="194"/>
        <source>Ctrl+E,Ctrl+B</source>
        <translation>Ctrl+E,Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="197"/>
        <source>Ctrl+E,Ctrl+C</source>
        <translation>Ctrl+E,Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="206"/>
        <source>Ctrl+E,Ctrl+Y</source>
        <translation>Ctrl+E,Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="203"/>
        <source>Ctrl+E,Ctrl+K</source>
        <translation>Ctrl+E,Ctrl+K</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="239"/>
        <source>Ctrl+E,Ctrl+U</source>
        <translation>Ctrl+E,Ctrl+U</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="209"/>
        <source>Ctrl+E,Ctrl+I</source>
        <translation>Ctrl+E,Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="212"/>
        <source>Ctrl+E,Ctrl+D</source>
        <translation>Ctrl+E,Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="215"/>
        <source>Ctrl+E,Ctrl+O</source>
        <translation>Ctrl+E,Ctrl+O</translation>
    </message>
    <message>
        <source>Ctrl+E,Ctrl+Shift+T</source>
        <translation type="vanished">Ctrl+E,Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="221"/>
        <source>Ctrl+E,Ctrl+E</source>
        <translation>Ctrl+E,Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="224"/>
        <source>Ctrl+E,Ctrl+J</source>
        <translation>Ctrl+E,Ctrl+J</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="227"/>
        <source>Ctrl+E,Ctrl+N</source>
        <translation>Ctrl+E,Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="230"/>
        <source>Ctrl+E,Ctrl+P</source>
        <translation>Ctrl+E,Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="233"/>
        <source>Ctrl+E,Ctrl+S</source>
        <translation>Ctrl+E,Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="236"/>
        <source>Ctrl+E,Ctrl+A</source>
        <translation>Ctrl+E,Ctrl+A</translation>
    </message>
    <message>
        <source>Ctrl+E,Ctrl+Shift+H</source>
        <translation type="vanished">Ctrl+E,Ctrl+Shift+H</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="242"/>
        <source>Ctrl+E,Ctrl+R</source>
        <translation>Ctrl+E,Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="245"/>
        <source>Ctrl+E,Ctrl+M</source>
        <translation>Ctrl+E,Ctrl+M</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="248"/>
        <source>Ctrl+E,Ctrl+H</source>
        <translation>Ctrl+E,Ctrl+H</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="200"/>
        <source>Ctrl+E,Ctrl+L</source>
        <translation>Ctrl+E,Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="218"/>
        <source>Ctrl+E,Ctrl+2</source>
        <translation>Ctrl+E,Ctrl+2</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="251"/>
        <source>Ctrl+E,Ctrl+T</source>
        <translation>Ctrl+E,Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="254"/>
        <source>Ctrl+E,Ctrl+1</source>
        <translation>Ctrl+E,Ctrl+1</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="257"/>
        <source>Ctrl+E,Ctrl+W</source>
        <translation>Ctrl+E,Ctrl+W</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="55"/>
        <source>Low wall</source>
        <translation>Muret</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="58"/>
        <source>Top right</source>
        <translation>Haut droite</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="61"/>
        <source>Top left</source>
        <translation>Haut gauche</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="64"/>
        <source>Bottom left</source>
        <translation>Bas gauche</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="67"/>
        <source>Bottom right</source>
        <translation>Bas droite</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="70"/>
        <source>Top right (water)</source>
        <translation>Haut droite (eau)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="73"/>
        <source>Top left (water)</source>
        <translation>Haut gauche (eau)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="76"/>
        <source>Bottom left (water)</source>
        <translation>Bas gauche (eau)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="79"/>
        <source>Bottom right (water)</source>
        <translation>Bas droite (eau)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="82"/>
        <source>Deep water</source>
        <translation>Eau profonde</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="85"/>
        <source>Shallow water</source>
        <translation>Eau peu profonde</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="88"/>
        <source>Grass</source>
        <translation>Herbe</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="91"/>
        <source>Hole</source>
        <translation>Trou</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="94"/>
        <source>Ice</source>
        <translation>Glace</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="97"/>
        <source>Ladder</source>
        <translation>Échelle</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="100"/>
        <source>Prickles</source>
        <translation>Piquants</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="103"/>
        <source>Lava</source>
        <translation>Lave</translation>
    </message>
    <message>
        <source>Low</source>
        <comment>Layer</comment>
        <translation type="vanished">Basse</translation>
    </message>
    <message>
        <source>Intermediate</source>
        <comment>Layer</comment>
        <translation type="vanished">Intermédiaire</translation>
    </message>
    <message>
        <source>High</source>
        <comment>Layer</comment>
        <translation type="vanished">Haute</translation>
    </message>
    <message>
        <source>None</source>
        <comment>Tile pattern animation</comment>
        <translation type="vanished">Aucune</translation>
    </message>
    <message>
        <source>Frames 1-2-3-1</source>
        <translation type="vanished">Images 1-2-3-1</translation>
    </message>
    <message>
        <source>Frames 1-2-3-2-1</source>
        <translation type="vanished">Images 1-2-3-2-1</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="46"/>
        <source>None</source>
        <comment>Tile pattern scrolling</comment>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="49"/>
        <source>Scrolling on itself</source>
        <translation>Scrolling sur lui-même</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="52"/>
        <source>Parallax scrolling</source>
        <translation>Scrolling parallax</translation>
    </message>
    <message>
        <source>Frames 1-2-3-1, parallax</source>
        <translation type="vanished">Images 1-2-3-1, parallax</translation>
    </message>
    <message>
        <source>Frames 1-2-3-2-1, parallax</source>
        <translation type="vanished">Images 1-2-3-2-1, parallax</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="43"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="46"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="110"/>
        <source>Source and destination are the same: &apos;%1&apos;</source>
        <translation>Source et destination identiques : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>No such file or directory: &apos;%1&apos;</source>
        <translation type="vanished">Fichier ou dossier introuvable : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="121"/>
        <location filename="../src/widgets/import_dialog.cpp" line="375"/>
        <source>Source file cannot be read: &apos;%1&apos;</source>
        <translation>Fichier source non accessible en lecture : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="125"/>
        <source>Destination already exists: &apos;%1&apos;</source>
        <translation>La destination existe déjà : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>No such directory: &apos;%1&apos;</source>
        <translation type="vanished">Dossier introuvable : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Cannot copy directory &apos;%1&apos; to one of its own subdirectories: &apos;%2&apos;</source>
        <translation type="vanished">Impossible de copier le dossier &apos;%1&apos; vers l&apos;un de ses sous-dossiers : &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="117"/>
        <source>No such file or folder: &apos;%1&apos;</source>
        <translation>Fichier ou dossier introuvable : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="134"/>
        <source>No such folder: &apos;%1&apos;</source>
        <translation>Dossier introuvable : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="141"/>
        <source>Cannot copy folder &apos;%1&apos; to one of its own subfolders: &apos;%2&apos;</source>
        <translation>Impossible de copier le dossier &apos;%1&apos; vers l&apos;un de ses sous-dossiers : &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="145"/>
        <location filename="../src/file_tools.cpp" line="221"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>Impossible de créer le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="190"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>Impossible de supprimer le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="204"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>Impossible de supprimer le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="244"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="267"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos; en écriture</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="159"/>
        <source>Cannot copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Impossible de copier le fichier &apos;%1&apos; vers &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/obsolete_editor_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is not supported by this version of the quest editor (%2).
Please download the latest version of the editor on www.solarus-games.org.</source>
        <translation>Le format de cette quête (%1) n&apos;est pas supporté par cette version de l&apos;éditeur de quêtes (%2).
Veuillez télécharger la dernière version de l&apos;éditeur sur www.solarus-games.org.</translation>
    </message>
    <message>
        <location filename="../src/obsolete_quest_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is obsolete.
Please upgrade your quest  data files to Solarus %2.</source>
        <translation>Le format de cette quête (%1) est obsolète.
Veuillez mettre à jour vos fichiers de données vers Solarus %2.</translation>
    </message>
    <message>
        <source>Low layer</source>
        <translation type="vanished">Couche basse</translation>
    </message>
    <message>
        <source>Intermediate layer</source>
        <translation type="vanished">Couche intermédiaire</translation>
    </message>
    <message>
        <source>High layer</source>
        <translation type="vanished">Couche haute</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="44"/>
        <source>Immediate</source>
        <translation>Immédiat</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="47"/>
        <source>Fade in/out</source>
        <translation>Fondu entrée/sortie</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="50"/>
        <source>Scrolling</source>
        <translation>Scrolling</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="45"/>
        <source>Plain</source>
        <translation>Plein</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="48"/>
        <source>Dashed</source>
        <translation>Pointillé</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="51"/>
        <source>Intersections (cross)</source>
        <translation>Intersections (croix)</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="54"/>
        <source>Intersections (point)</source>
        <translation>Intersections (point)</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="50"/>
        <source>In both directions</source>
        <translation>Dans les deux directions</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="53"/>
        <source>Horizontally</source>
        <translation>Horizontalement</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="56"/>
        <source>Vertically</source>
        <translation>Verticalement</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="59"/>
        <source>Non repeatable</source>
        <translation>Non répétable</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="45"/>
        <source>When the world changes</source>
        <translation>Si changement de monde</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="48"/>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="51"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="45"/>
        <source>Could not create data directory.</source>
        <translation>Impossible de créer le dossier de la quête.</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="56"/>
        <source>Untitled Quest</source>
        <translation>Quête sans titre</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="59"/>
        <source>Could not create quest properties.</source>
        <translation>Impossible de créer les propriétés de la quête.</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="101"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>Impossible de trouver le dossier &quot;assets&quot;.\nVérifiez que Solarus Quest Editor est correctement installé.</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="61"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="64"/>
        <source>Right side</source>
        <translation>Côté est</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="67"/>
        <source>Top side</source>
        <translation>Côté nord</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="70"/>
        <source>Left side</source>
        <translation>Côté ouest</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="73"/>
        <source>Bottom side</source>
        <translation>Côté sud</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="76"/>
        <source>Top-right corner (convex)</source>
        <translation>Sommet nord-est (convexe)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="79"/>
        <source>Top-left corner (convex)</source>
        <translation>Sommet nord-ouest (convexe)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="82"/>
        <source>Bottom-left corner (convex)</source>
        <translation>Sommet sud-ouest (convexe)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="85"/>
        <source>Bottom-right corner (convex)</source>
        <translation>Sommet sud-est (convexe)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="88"/>
        <source>Top-right corner (concave)</source>
        <translation>Sommet nord-est (concave)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="91"/>
        <source>Top-left corner (concave)</source>
        <translation>Sommet nord-ouest (concave)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="94"/>
        <source>Bottom-left corner (concave)</source>
        <translation>Sommet sud-ouest (concave)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="97"/>
        <source>Bottom-right corner (concave)</source>
        <translation>Sommet sud-est (concave)</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="363"/>
        <source>Source file does not exist: &apos;%1&apos;</source>
        <translation>Le fichier source n&apos;existe pas: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="367"/>
        <source>Source path is a folder: &apos;%1&apos;</source>
        <translation>Le chemin source est un dossier : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="456"/>
        <source>Source folder does not exist: &apos;%1&apos;</source>
        <translation>Le dossier source n&apos;existe pas : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="460"/>
        <source>Source path is not a folder: &apos;%1&apos;</source>
        <translation>Le chemin source n&apos;est pas un dossier : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="464"/>
        <source>Source folder cannot be read: &apos;%1&apos;</source>
        <translation>Dossier source non accessible en lecture : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="371"/>
        <source>Source file is a symbolic link: &apos;%1&apos;</source>
        <translation>Le fichier source est un lien symbolique : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="46"/>
        <source>Side by side</source>
        <translation>Côte-à-côte</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="49"/>
        <source>Input texture</source>
        <translation>Texture d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="52"/>
        <source>Output texture</source>
        <translation>Texture de sortie</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="55"/>
        <source>Swipe</source>
        <translation>Balayage</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="35"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="48"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="61"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::AboutDialog</name>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="176"/>
        <source>Integrated development environment for Solarus, a free and open-source ARPG 2D game engine.</source>
        <translation>Environnement de développement intégré pour Solarus, un moteur de jeu Action-RPG 2D libre et open-source.</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Website&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Site web&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="240"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;This program licensed is under the &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Ce programme est sous licence  &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.cpp" line="37"/>
        <source>About %0</source>
        <translation>À propos de %0</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeBorderSetIdDialog</name>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="14"/>
        <source>Border set id</source>
        <translation>Id du border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="20"/>
        <source>New contour id:</source>
        <translation>Nouvel id du contour :</translation>
    </message>
    <message>
        <source>New border set id:</source>
        <translation type="vanished">Id du nouveau border set :</translation>
    </message>
    <message>
        <source>New id for border set &apos;%1&apos;:</source>
        <translation type="vanished">Nouvel id pour le border set &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.cpp" line="35"/>
        <source>New id for contour &apos;%1&apos;:</source>
        <translation>Nouveau id du contour &apos;%1&apos; :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeDialogIdDialog</name>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="14"/>
        <source>Change dialog id</source>
        <translation>Changer l&apos;id d&apos;un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="20"/>
        <source>New dialog id:</source>
        <translation>Nouvel id de dialogue :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="30"/>
        <source>Change the id of all dialogs with this id as prefix</source>
        <translation>Changer l&apos;id de tous les dialogues avec cet id comme préfixe</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="41"/>
        <source>New id for dialogs prefixed by &apos;%1&apos;:</source>
        <translation>Nouvel id pour les dialogues préfixés par &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="44"/>
        <source>New id for dialog &apos;%1&apos;:</source>
        <translation>Nouvel id du dialogue &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="101"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de dialogue invalide : %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="115"/>
        <source>The dialog &apos;%1&apos; already exists</source>
        <translation>Le dialogue &apos;%1&apos; existe déjà</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeFileInfoDialog</name>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="14"/>
        <source>File information</source>
        <translation>Informations de fichier</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="20"/>
        <source>Set file information</source>
        <translation>Changer les informations du fichier</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="29"/>
        <source>Author:</source>
        <translation>Auteur :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="42"/>
        <source>License:</source>
        <translation>Licence :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangePatternIdDialog</name>
    <message>
        <source>New pattern id:</source>
        <translation type="vanished">Nouvel id du motif :</translation>
    </message>
    <message>
        <source>Update references in existing maps</source>
        <translation type="vanished">Mettre à jour les références dans les maps existantes</translation>
    </message>
    <message>
        <source>Rename tile pattern</source>
        <translation type="vanished">Renommer un motif</translation>
    </message>
    <message>
        <source>New id for pattern &apos;%1&apos;:</source>
        <translation type="vanished">Nouvel id du motif &apos;%1&apos; :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeResourceIdDialog</name>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="14"/>
        <source>Rename resource</source>
        <translation>Renommer la ressource</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="20"/>
        <source>New id for resource element</source>
        <translation>Nouvel id pour l&apos;élément</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="30"/>
        <source>Update references</source>
        <translation>Mettre à jour les références</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="46"/>
        <source>New id for %1 &apos;%2&apos;:</source>
        <translation>Nouvel id pour %1 &apos;%2&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="52"/>
        <source>Update existing teletransporters leading to this map</source>
        <translation>Mettre à jour les téléporteurs menant à cette map</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="56"/>
        <source>Update existing maps using this tileset</source>
        <translation>Mettre à jour les maps utilisant ce tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="60"/>
        <source>Update existing maps using this music</source>
        <translation>Mettre à jour les maps utilisant cette musique</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="64"/>
        <source>Update existing enemies having this breed</source>
        <translation>Mettre à jour les ennemis utilisant ce modèle</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="68"/>
        <source>Update existing custom entities having this model</source>
        <translation>Mettre à jour les entités custom utilisant ce modèle</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="126"/>
        <source>Empty resource element id</source>
        <translation>Id de ressource manquant</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="131"/>
        <source>Invalid resource element id</source>
        <translation>Id de ressource invalide</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeSourceImageDialog</name>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="14"/>
        <source>Change source image</source>
        <translation>Changer l&apos;image source</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="28"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="35"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.cpp" line="125"/>
        <source>No image selected.</source>
        <translation>Aucune image n&apos;est sélectionnée.</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeStringKeyDialog</name>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="14"/>
        <source>Change string key</source>
        <translation>Changer la clé d&apos;un texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="20"/>
        <source>New string key:</source>
        <translation>Nouvelle clé de texte :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="30"/>
        <source>Change the key of all strings with this key as prefix</source>
        <translation>Changer la clé de tous les textes ayant cette clé comme préfixe</translation>
    </message>
    <message>
        <source>Change the key of all string with this key as prefix</source>
        <translation type="vanished">Changer la clé de tous les textes avec cette clé comme préfixe</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="41"/>
        <source>New key for strings prefixed by &apos;%1&apos;:</source>
        <translation>Nouvelle clé pour les textes préfixés par &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="44"/>
        <source>New key for string &apos;%1&apos;:</source>
        <translation>Nouvelle clé pour le texte &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="101"/>
        <source>Invalid string key: %1</source>
        <translation>Clé de texte invalide: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="115"/>
        <source>The string &apos;%1&apos; already exists</source>
        <translation>Le texte &apos;%1&apos; existe déjà</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ColorChooser</name>
    <message>
        <location filename="../src/widgets/color_chooser.cpp" line="89"/>
        <source>Select color</source>
        <translation>Choix d&apos;une couleur</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ColorPicker</name>
    <message>
        <source>Select color</source>
        <translation type="vanished">Choix d&apos;une couleur</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Console</name>
    <message>
        <location filename="../src/widgets/console.ui" line="42"/>
        <source>Run Lua code to the quest</source>
        <translation>Exécuter du code Lua dans la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="259"/>
        <source>The quest process failed to start.</source>
        <translation>Le processus de quête n&apos;a pas pu démarrer.</translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="262"/>
        <source>The quest process crashed.</source>
        <translation>Le processus de quête a crashé.</translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="265"/>
        <source>The quest process timed out.</source>
        <translation>Le processus de quête a cessé de &apos;repondre.</translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="273"/>
        <source>An unknown error occurred to the quest process.</source>
        <translation>Une erreur inconnue s&apos;est produite avec le processus de quête.</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogPropertiesTable</name>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="35"/>
        <source>New property...</source>
        <translation>Nouvelle propriété...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="41"/>
        <source>Change key...</source>
        <translation>Changer la clé...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="42"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="49"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="57"/>
        <source>Set from translation...</source>
        <translation>Définir depuis la traduction...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsEditor</name>
    <message>
        <source>Dialogss editor</source>
        <translation type="vanished">Éditeur de dialogues</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="14"/>
        <source>Dialogs editor</source>
        <translation>Éditeur de dialogues</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="35"/>
        <source>Language properties</source>
        <translation>Propriétés de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="44"/>
        <source>Language id</source>
        <translation>Id de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="51"/>
        <source>Folder name of the language</source>
        <translation>Nom du dossier de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="64"/>
        <source>Language description</source>
        <translation>Description de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="71"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="99"/>
        <source>Compare to language</source>
        <translation>Comparer à la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="112"/>
        <source>Refresh language</source>
        <translation>Recharger la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="115"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="164"/>
        <source>Add dialog</source>
        <translation>Ajouter un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="202"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="134"/>
        <source>Change dialog id</source>
        <translation>Changer l&apos;id du dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="240"/>
        <source>Duplicate dialog(s)</source>
        <translation>Dupliquer le dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="278"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="205"/>
        <source>Delete dialog</source>
        <translation>Supprimer le dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="316"/>
        <source>Dialog properties</source>
        <translation>Propriétés du dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="324"/>
        <source>Dialog id:</source>
        <translation>Id du dialogue :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="371"/>
        <source>Text:</source>
        <translation>Texte :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="391"/>
        <source>1,1</source>
        <translation>1,1</translation>
    </message>
    <message>
        <source>Display right margin at column:</source>
        <translation type="vanished">Afficher une marge à la colonne :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="416"/>
        <source>Translation:</source>
        <translation>Traduction :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="445"/>
        <source>Properties :</source>
        <translation>Propriétés :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="475"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="480"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="496"/>
        <source>Add property</source>
        <translation>Nouvelle propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="534"/>
        <source>Change property key</source>
        <translation>Changer la clé de la propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="572"/>
        <source>Delete property</source>
        <translation>Supprimer la propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="70"/>
        <source>Create dialog</source>
        <translation>Créer dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="103"/>
        <source>Duplicate dialogs</source>
        <translation>Dupliquer le dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="167"/>
        <source>Change dialog id prefix</source>
        <translation>Changer préfixe des ids de dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="237"/>
        <source>Delete dialogs</source>
        <translation>Supprimer dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="271"/>
        <source>Change dialog text</source>
        <translation>Changer le texte d&apos;un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="306"/>
        <source>Create dialog property</source>
        <translation>Créer une propriété d&apos;un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="340"/>
        <source>Delete dialog property</source>
        <translation>Supprimer une propriété d&apos;un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="375"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="973"/>
        <source>Change dialog property key</source>
        <translation>Changer la clé d&apos;une propriété de dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="414"/>
        <source>Change dialog property</source>
        <translation>Changer la propriété d&apos;un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="464"/>
        <source>Dialogs %1</source>
        <translation>Dialogues %1</translation>
    </message>
    <message>
        <source>Dialogs &apos;%1&apos; has been modified. Save changes?</source>
        <translation type="vanished">Les Dialogues &apos;%1&apos; ont été modifié. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="467"/>
        <source>Dialogs &apos;%1&apos; have been modified. Save changes?</source>
        <translation>Les dialogues &apos;%1&apos; ont été modifiés. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="476"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;Aucune langue&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="628"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog</source>
        <translation>Nouveau dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog id:</source>
        <translation>Nouvel id de dialogue :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="681"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de dialogue invalide : %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="686"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="703"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>Le dialogue &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="699"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="777"/>
        <source>Delete confirmation</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="778"/>
        <source>Do you really want to delete all dialogs prefixed by &apos;%1&apos;?</source>
        <translation>Voulez-vous vraiment supprimer tous les dialogues préfixés par &apos;%1&apos; ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="910"/>
        <source>New dialog property</source>
        <translation>Nouvelle propriété de dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="911"/>
        <source>New property key:</source>
        <translation>Nouvelle clé de propriété :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="930"/>
        <source>The property &apos;%1&apos; already exists in the dialog &apos;%2&apos;</source>
        <translation>La propriété &apos;%1&apos; existe déjà dans le dialogue &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="974"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>Changer la clé de la propriété &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="1099"/>
        <source>Invalid property key: it should be a valid Lua identifier</source>
        <translation>La clé de propriété n&apos;est pas un identificateur Lua valide</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="919"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="982"/>
        <source>The property key cannot be empty</source>
        <translation>La clé de propriété ne peut pas être vide</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsModel</name>
    <message>
        <location filename="../src/dialogs_model.cpp" line="46"/>
        <location filename="../src/dialogs_model.cpp" line="878"/>
        <source>Cannot open dialogs data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de dialogues &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="84"/>
        <source>Cannot save dialogs data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le fichier de dialogues &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="394"/>
        <location filename="../src/dialogs_model.cpp" line="708"/>
        <source>Invalid dialog id: %1</source>
        <translation>Id de dialogue invalide : %1</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="398"/>
        <location filename="../src/dialogs_model.cpp" line="496"/>
        <location filename="../src/dialogs_model.cpp" line="589"/>
        <location filename="../src/dialogs_model.cpp" line="671"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>Le dialogue &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="585"/>
        <source>Dialog &apos;%1&apos; does not exist</source>
        <translation>Le dialogue &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="593"/>
        <source>Invalid dialog id: &apos;%1&apos;</source>
        <translation>Id de dialogue invalide : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Dialog &apos;%1&apos; no exists</source>
        <translation type="vanished">Le dialogue &apos;%1&apos; n&apos;existe pas</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsTreeView</name>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="37"/>
        <source>New dialog...</source>
        <translation>Nouveau dialogue...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="43"/>
        <source>Duplicate dialog(s)...</source>
        <translation>Dupliquer le dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="49"/>
        <source>Change id...</source>
        <translation>Changer l&apos;id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DocWidget</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">MainWindow</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="vanished">toolBar</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DrawingRectangleState</name>
    <message>
        <source>New pattern (more options)</source>
        <translation type="obsolete">Nouveau motif (plus d&apos;options)</translation>
    </message>
    <message>
        <source>New pattern (%1)</source>
        <translation type="obsolete">Nouveau motif (%1)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::EditEntityDialog</name>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="20"/>
        <source>Edit an entity</source>
        <translation>Modifier une entité</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="28"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Enemy</source>
        <translation type="vanished">Ennemi</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="48"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="72"/>
        <source>Layer</source>
        <translation>Couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="126"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <source>,</source>
        <translation type="vanished">,</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="136"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="vanished">x</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="156"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="212"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="219"/>
        <source>Sprite</source>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="234"/>
        <source>Repeat sprite with tiling</source>
        <translation>Répéter le motif du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="243"/>
        <source>Subtype</source>
        <translation>Sous-type</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="263"/>
        <source>Custom entity script</source>
        <translation>Script d&apos;entité custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="273"/>
        <source>Save the state</source>
        <translation>Sauver l&apos;état</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="308"/>
        <source>Treasure</source>
        <translation>Trésor</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="335"/>
        <source>Variant of this item</source>
        <translation>Variante de cet item</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="373"/>
        <source>Save the treasure state</source>
        <translation>Sauver l&apos;état du trésor</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="397"/>
        <source>Price font</source>
        <translation>Police d&apos;écriture du prix</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="447"/>
        <source>Can be lifted</source>
        <translation>Peut être porté</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="508"/>
        <source>Can hurt enemies</source>
        <translation>Peut blesser les ennemis</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="530"/>
        <source>Damage</source>
        <translation>Dégâts</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="601"/>
        <source>By script</source>
        <translation>Par script</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="608"/>
        <source>By hero</source>
        <translation>Par le héros</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="617"/>
        <source>By hero, savegame variable required</source>
        <translation>Par le héros, variable sauvegardée nécessaire</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="647"/>
        <source>Reset/decrement when opening</source>
        <translation>Réinitialiser/décrémenter la variable après l&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="658"/>
        <source>By hero, item required</source>
        <translation>Par le héros, item nécessaire</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="695"/>
        <source>Remove/decrement when opening</source>
        <translation>Retirer/décrémenter l&apos;item après l&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="704"/>
        <source>By explosion</source>
        <translation>Par une explosion</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="781"/>
        <source>Update starting location</source>
        <translation>Sauver l&apos;emplacement de départ</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="788"/>
        <source>Only possible if the destination has a name</source>
        <translation>Uniquement possible si la destination a un nom</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="795"/>
        <source>Pattern</source>
        <translation>Motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="802"/>
        <source>Click to choose another pattern</source>
        <translation>Cliquez ici pour choisir un autre motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="809"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="862"/>
        <source>User properties</source>
        <translation>Propriétés utilisateur</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="884"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="889"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="905"/>
        <source>Add property</source>
        <translation>Nouvelle propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="934"/>
        <source>Change property key</source>
        <translation>Changer la clé de la propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="963"/>
        <source>Delete property</source>
        <translation>Supprimer la propriété</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="992"/>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1021"/>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="831"/>
        <source>Tileset of the map</source>
        <translation>Tileset de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="840"/>
        <source>Other:</source>
        <translation>Autre :</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="579"/>
        <source>Opening mode</source>
        <translation>Mode d&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="714"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="738"/>
        <source>Show a dialog</source>
        <translation>Afficher un dialogue</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="750"/>
        <source>Call the map script</source>
        <translation>Appeler le script de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="759"/>
        <source>Call an item script</source>
        <translation>Appeler le script d&apos;un item</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="295"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="382"/>
        <source>in variable</source>
        <translation>dans la variable</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="60"/>
        <source>Update existing teletransporters</source>
        <translation>Mettre à jour les téléporteurs existants</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="146"/>
        <source>Origin</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="407"/>
        <source>Play a sound</source>
        <translation>Jouer un son</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="417"/>
        <source>Transition</source>
        <translation>Transition</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="427"/>
        <source>Destination map</source>
        <translation>Map de destination</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="437"/>
        <source>Destination</source>
        <translation>Destination</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="469"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <source>Damage on enemies</source>
        <translation type="vanished">Dégâts sur les ennemis</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="569"/>
        <source>Set a special ground</source>
        <translation>Modifier le terrain</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="253"/>
        <source>Breed</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <source>Rank</source>
        <translation type="vanished">Grade</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1060"/>
        <source>Maximum moves</source>
        <translation>Maximum de déplacements</translation>
    </message>
    <message>
        <source>Call a custom entity script</source>
        <translation type="vanished">Appeler un script d&apos;entité custom</translation>
    </message>
    <message>
        <source>Savegame variable</source>
        <translation type="vanished">Variable de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1103"/>
        <source>Initial state</source>
        <translation>État initial</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1096"/>
        <source>Enabled at start</source>
        <translation>Actif au démarrage</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Set as the default destination</source>
        <translation>Destination par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Cutting the object</source>
        <translation>Couper l&apos;objet</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Can be cut</source>
        <translation>Peut être coupé</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Exploding</source>
        <translation>Explosion</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Can explode</source>
        <translation>Peut exploser</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Regeneration</source>
        <translation>Régénération</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Can regenerate</source>
        <translation>Peut se régénérer</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Interactions</source>
        <translation>Interactions</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Can be pushed</source>
        <translation>Peut être poussé</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="527"/>
        <source>Can be pulled</source>
        <translation>Peut être tiré</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Activation</source>
        <translation>Activation</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Requires a block to be activated</source>
        <translation>Nécessite un bloc pour être activé</translation>
    </message>
    <message>
        <source>Stay on switch</source>
        <translation type="vanished">Quitter l&apos;interrupteur</translation>
    </message>
    <message>
        <source>Inactivate the switch when leaving</source>
        <translation type="vanished">Désactiver si le héros s&apos;éloigne</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="198"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="238"/>
        <source>The property &apos;%1&apos; already exists</source>
        <translation>La propriété &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="223"/>
        <source>Change user property key</source>
        <translation>Changer la clé d&apos;une propriété utilisateur</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="224"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>Changer la clé de la propriété &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="232"/>
        <source>The property key cannot be empty</source>
        <translation>La clé de propriété ne peut pas être vide</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="244"/>
        <source>The key &apos;%1&apos; is invalid</source>
        <translation>Clé &apos;%1&apos; invalide</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Leaving the switch</source>
        <translation>Quitter l&apos;interrupteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Deactivate when leaving</source>
        <translation>Désactiver si le héros s&apos;éloigne</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Hero</source>
        <translation>Héros</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Obstacle for the hero</source>
        <translation>Obstacle pour le héros</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Enemies</source>
        <translation>Ennemis</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Obstacle for enemies</source>
        <translation>Obstacle pour les ennemis</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>NPCs</source>
        <translation>PNJ</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>Obstacle for NPCs</source>
        <translation>Obstacle pour les PNJ</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Blocks</source>
        <translation>Blocs</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Obstacle for blocks</source>
        <translation>Obstacle pour les blocs</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Projectiles</source>
        <translation>Projectiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Obstacle for projectiles</source>
        <translation>Obstacle pour les projectiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Movements</source>
        <translation>Mouvements</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Allow to move</source>
        <translation>Autoriser les mouvements</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Sword</source>
        <translation>Épée</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Allow to use the sword</source>
        <translation>Autoriser l&apos;épée</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Allow to use equipment items</source>
        <translation>Autoriser les items d&apos;équipement</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="582"/>
        <source>Price</source>
        <translation>Prix</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="583"/>
        <source>Jump length</source>
        <translation>Distance de saut</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="584"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="633"/>
        <source>Show a dialog if fails to open</source>
        <translation>Dialogue si l&apos;ouverture échoue</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="634"/>
        <source>Description dialog id</source>
        <translation>Dialogue de description</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="852"/>
        <source>(Default destination)</source>
        <translation>(Destination par défaut)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="853"/>
        <source>(Same point)</source>
        <translation>(Même point)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="854"/>
        <source>(Side of the map)</source>
        <translation>(Bord de la map)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="918"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="926"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="919"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="928"/>
        <source>Up</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="920"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="930"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="921"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="932"/>
        <source>Down</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="927"/>
        <source>Right-up</source>
        <translation>Haut-droite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="929"/>
        <source>Left-up</source>
        <translation>Haut-gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="931"/>
        <source>Left-down</source>
        <translation>Bas-gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="933"/>
        <source>Right-down</source>
        <translation>Bas-droite</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1002"/>
        <source>(Default)</source>
        <translation>(Défaut)</translation>
    </message>
    <message>
        <source>Cannot move</source>
        <translation type="vanished">Aucun déplacement</translation>
    </message>
    <message>
        <source>1 move only</source>
        <translation type="vanished">1 déplacement uniquement</translation>
    </message>
    <message>
        <source>Unlimited</source>
        <translation type="vanished">Illimité</translation>
    </message>
    <message>
        <source>Unlimited moves</source>
        <translation type="vanished">Illimité</translation>
    </message>
    <message>
        <source>One move only</source>
        <translation type="vanished">Un déplacement uniquement</translation>
    </message>
    <message>
        <source>Infinite moves</source>
        <translation type="vanished">Déplacement illimités</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">Normal</translation>
    </message>
    <message>
        <source>Miniboss</source>
        <translation type="vanished">Mini-boss</translation>
    </message>
    <message>
        <source>Boss</source>
        <translation type="vanished">Boss</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1418"/>
        <source>Save the enemy state</source>
        <translation>Sauver l&apos;état de l&apos;ennemi</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1419"/>
        <source>Save the door state</source>
        <translation>Sauver l&apos;état de la porte</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1510"/>
        <source>Play a sound when destroyed</source>
        <translation>Son à la destruction</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1787"/>
        <source>(None)</source>
        <translation>(Aucun)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Editor</name>
    <message>
        <location filename="../src/widgets/editor.cpp" line="172"/>
        <source>File &apos;%1&apos; has been modified. Save changes?</source>
        <translation>Le fichier &apos;%1&apos; a été modifé. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/editor.cpp" line="466"/>
        <source>Save changes</source>
        <translation>Enregistrer les changements</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ExternalScriptDialog</name>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="14"/>
        <source>Running script</source>
        <translation>Exécution du script</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="24"/>
        <source>Running script...</source>
        <translation>Exécution du script...</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="40"/>
        <location filename="../src/widgets/external_script_dialog.cpp" line="169"/>
        <source>In progress</source>
        <translation>en cours</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="62"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="56"/>
        <source>Cannot determine the directory of script &apos;%1&apos;</source>
        <translation>Impossible de déterminer le dossier du script &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="201"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="244"/>
        <source>Successful!</source>
        <translation>Succès !</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="248"/>
        <source>Failure</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::FindTextDialog</name>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="14"/>
        <source>Find text</source>
        <translation>Rechercher texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="25"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="32"/>
        <source>Find</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="42"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="37"/>
        <source>Replace</source>
        <translation>Remplacer</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::GetAnimationNameDialog</name>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="34"/>
        <source>New animation</source>
        <translation>Nouvelle animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="50"/>
        <source>Change animation name</source>
        <translation>Renommer l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="84"/>
        <source>Empty animation name</source>
        <translation>Nom d&apos;animation vide</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="91"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>L&apos;animation &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="105"/>
        <source>Animation name:</source>
        <translation>Nom d&apos;animation:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImageView</name>
    <message>
        <location filename="../src/widgets/image_view.cpp" line="41"/>
        <source>Failed to load image &apos;%1&apos;</source>
        <translation>Impossible de charger l&apos;image &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImportDialog</name>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="14"/>
        <source>Import files from another quest</source>
        <translation>Importer des fichiers d&apos;une autre quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="36"/>
        <source>Source quest</source>
        <translation>Quête source</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="52"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="91"/>
        <source>Identify missing</source>
        <translation>Identifier manquants</translation>
    </message>
    <message>
        <source>Find missing</source>
        <translation type="vanished">Trouver manquants</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="102"/>
        <source>No missing files found</source>
        <translation>Aucun fichier
manquant trouvé</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">Importer</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="68"/>
        <source>Destination quest</source>
        <translation>Quête destination</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="57"/>
        <source>Import files</source>
        <translation>Importer les fichiers</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="120"/>
        <source>Select a quest where to import from</source>
        <translation>Sélectionner la quête depuis laquelle importer</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="136"/>
        <source>Source and destination quest are the same</source>
        <translation>Quêtes source et destination identiques</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="209"/>
        <source>No candidates found</source>
        <translation>Aucun candidat trouvé</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="210"/>
        <source>%1 candidates found</source>
        <translation>%1 candidats trouvés</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="272"/>
        <source>Import 1 item</source>
        <translation>Importer 1 élément</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="275"/>
        <source>Import %1 items</source>
        <translation>Importer %1 éléments</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="309"/>
        <source>Import confirmation</source>
        <translation>Confirmation d&apos;import</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="310"/>
        <source>%1 items will be imported to your quest.</source>
        <translation>%1 éléments seront importés dans votre quête.</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="344"/>
        <source>Cannot import symbolic link &apos;%1&apos;</source>
        <translation>Impossible d&apos;importer le lien symbolique &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="382"/>
        <source>Destination path already exists and is a folder: &apos;%1&apos;</source>
        <translation>La destination existe déjà et est un dossier : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="483"/>
        <source>The destination directory &apos;%1&apos; already exists.
Do you want to overwrite its content?</source>
        <translation>Le dossier destination &apos;%1&apos; existe déjà.
Voulez-vous écraser son contenu ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="484"/>
        <source>Apply this choice for remaining directories</source>
        <translation>Appliquer ce choix pour les dossiers restants</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="601"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="604"/>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="606"/>
        <source>Skip</source>
        <translation>Passer</translation>
    </message>
    <message>
        <source>Destination folder already exists</source>
        <translation type="vanished">Le dossier destination existe déjà</translation>
    </message>
    <message>
        <source>The destination folder &apos;%1&apos; already exists.
Do you want to merge it with the contents from the source folder?</source>
        <translation type="vanished">Le dossier destination &apos;%1&apos; existe déjà.
Voulez-vous le fusionner avec le contenu du dossier source ?</translation>
    </message>
    <message>
        <source>Destination file already exists</source>
        <translation type="vanished">Le fichier destination existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="143"/>
        <source>No source quest was not found in folder &apos;%1&apos;</source>
        <translation>Aucune quête source n&apos;a été trouvée dans le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="391"/>
        <location filename="../src/widgets/import_dialog.cpp" line="482"/>
        <source>Destination already exists</source>
        <translation>La destination existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="392"/>
        <source>The destination file &apos;%1&apos; already exists.
Do you want to overwrite it?</source>
        <translation>Le fichier destination &apos;%1&apos; existe déjà.
Voulez-vous l&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="393"/>
        <source>Apply this choice for remaining files</source>
        <translation>Appliquer ce choix pour les fichiers restants</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="420"/>
        <source>Failed to remove existing file &apos;%1&apos;</source>
        <translation>Impossible de supprimer le fichier existant &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="436"/>
        <source>Failed to copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Impossible de copier le fichier &apos;%1&apos; vers &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="471"/>
        <source>Destination path already exists and is not a directory: &apos;%1&apos;</source>
        <translation>La destination existe déjà est n&apos;est pas un dossier : &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::InputDialogWithCheckBox</name>
    <message>
        <location filename="../src/widgets/input_dialog_with_check_box.ui" line="21"/>
        <source>New value:</source>
        <translation>Nouvelle valeur :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MainWindow</name>
    <message>
        <location filename="../src/widgets/main_window.ui" line="20"/>
        <source>Solarus Quest Editor</source>
        <translation>Solarus Quest Editor</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="77"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="96"/>
        <source>Edit</source>
        <translation>Édition</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="110"/>
        <source>Run</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="117"/>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="130"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="139"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="146"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="188"/>
        <source>New quest...</source>
        <translation>Nouvelle quête...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="193"/>
        <source>Load quest...</source>
        <translation>Ouvrir une quête...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="196"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="201"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="210"/>
        <location filename="../src/widgets/main_window.cpp" line="1736"/>
        <source>Run quest</source>
        <translation>Exécuter la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="213"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="284"/>
        <location filename="../src/widgets/main_window.ui" line="287"/>
        <source>Show layer 0</source>
        <translation>Afficher la couche 0</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="290"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="305"/>
        <location filename="../src/widgets/main_window.ui" line="308"/>
        <source>Show layer 1</source>
        <translation>Afficher la couche 1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="311"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="326"/>
        <location filename="../src/widgets/main_window.ui" line="329"/>
        <source>Show layer 2</source>
        <translation>Afficher la couche 2</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="332"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="358"/>
        <source>Find / Replace</source>
        <translation>Rechercher / Remplacer</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="372"/>
        <source>Select all</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="377"/>
        <source>Save all</source>
        <translation>Enregistrer tout</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="380"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="385"/>
        <source>Close all</source>
        <translation>Fermer tout</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="388"/>
        <source>Ctrl+Shift+W</source>
        <translation>Ctrl+Shift+W</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="396"/>
        <source>Show console</source>
        <translation>Afficher la console</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="399"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="407"/>
        <source>Unselect all</source>
        <translation>Déselectionner tout</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="412"/>
        <source>Close quest</source>
        <translation>Fermer la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="421"/>
        <location filename="../src/widgets/main_window.cpp" line="1785"/>
        <location filename="../src/widgets/main_window.cpp" line="1792"/>
        <source>Pause music</source>
        <translation>Mettre la musique en pause</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="430"/>
        <location filename="../src/widgets/main_window.cpp" line="1781"/>
        <source>Stop music</source>
        <translation>Arrêter la musique</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="442"/>
        <source>Show traversable entities</source>
        <translation>Afficher les entités traversables</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="454"/>
        <source>Show obstacle entities</source>
        <translation>Afficher les entités obstacles</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="459"/>
        <source>Quest properties</source>
        <translation>Propriétés de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="462"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="467"/>
        <source>Import from a quest...</source>
        <translation>Importer d&apos;une quête...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="470"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="475"/>
        <source>Build quest package...</source>
        <translation>Construire le package</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="501"/>
        <location filename="../src/widgets/main_window.ui" line="504"/>
        <source>Run current map</source>
        <translation>Exécuter la map actuelle</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="507"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="512"/>
        <source>Re-open closed tab</source>
        <translation>Réouvrir le dernier onglet fermé</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="515"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation type="vanished">Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="484"/>
        <source>Export to image...</source>
        <translation>Exporter comme image...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="487"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="492"/>
        <source>About</source>
        <translation>À propos de</translation>
    </message>
    <message>
        <source>Find...</source>
        <translation type="vanished">Rechercher...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="363"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paramètres</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="222"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="231"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="240"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="245"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="254"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="266"/>
        <source>Show grid</source>
        <translation>Afficher la grille</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="269"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <source>Show low layer</source>
        <translation type="vanished">Afficher la couche basse</translation>
    </message>
    <message>
        <source>Ctrl+1</source>
        <translation type="vanished">Ctrl+1</translation>
    </message>
    <message>
        <source>Show intermediate layer</source>
        <translation type="vanished">Afficher la couche intermédiaire</translation>
    </message>
    <message>
        <source>Ctrl+2</source>
        <translation type="vanished">Ctrl+2</translation>
    </message>
    <message>
        <source>Show high layer</source>
        <translation type="vanished">Afficher la couche haute</translation>
    </message>
    <message>
        <source>Ctrl+3</source>
        <translation type="vanished">Ctrl+3</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="341"/>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="344"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="353"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="845"/>
        <source>Select quest directory</source>
        <translation>Choisir le dossier de la quête</translation>
    </message>
    <message>
        <source>Quest created</source>
        <translation type="vanished">Quête créée</translation>
    </message>
    <message>
        <source>Quest successfully created!
The next step is to manually edit your quest properties in quest.dat
(sorry, this is not fully supported by the editor yet).
</source>
        <translation type="vanished">Quête créée avec succès !
La prochaine étape est de modifier manuellement les propriétés de votre quête dans quest.dat
(désolé, l&apos;éditeur ne sait pas encore le faire interactivement).</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="615"/>
        <source>No quest was found in directory
&apos;%1&apos;</source>
        <translation>Aucune quête n&apos;a été trouvée dans le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="130"/>
        <location filename="../src/widgets/main_window.cpp" line="331"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <source>Show entity types</source>
        <translation type="vanished">Afficher les types d&apos;entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="333"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="334"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="335"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="336"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="337"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
    <message>
        <source>Show all</source>
        <translation type="vanished">Afficher tout</translation>
    </message>
    <message>
        <source>Hide all</source>
        <translation type="vanished">Cacher tout</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="104"/>
        <source>Recent quests</source>
        <translation>Quêtes récentes</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="150"/>
        <source>Show/hide more layers</source>
        <translation>Afficher/cacher plus de couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="154"/>
        <source>Lock/unlock layers</source>
        <translation>Verrouiller/déverrouiller les couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="162"/>
        <location filename="../src/widgets/main_window.cpp" line="481"/>
        <source>Show/hide entity types</source>
        <translation>Afficher/cacher des types d&apos;entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="265"/>
        <source>Could not locate the assets directory.
Some features like creating a new quest will not be available.
Please make sure that Solarus Quest Editor is correctly installed.</source>
        <translation>Impossible de trouver le dossier &quot;assets&quot;.
Certaines fonctionnalités commes créer une nouvelle quête ne seront pas disponibles.
Vérifiez que Solarus Quest Editor est correctement installé.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="373"/>
        <source>Show all layers</source>
        <translation>Afficher toutes les couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="383"/>
        <source>Hide all layers</source>
        <translation>Cacher toutes les couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="402"/>
        <source>Show layer %1</source>
        <translation>Afficher la couche %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="453"/>
        <source>Lock layer %1</source>
        <translation>Verrouiller la couche %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="458"/>
        <source>Ctrl+%1</source>
        <translation>Ctrl+%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="510"/>
        <source>Show all entities</source>
        <translation>Afficher toutes les entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="520"/>
        <source>Hide all entities</source>
        <translation>Cacher toutes les entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="688"/>
        <source>Obsolete quest</source>
        <translation>Quête obsolète</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="689"/>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.
Would you like to update this quest?</source>
        <translation>Le format de cette quête (%1) is obsolète.
Vos fichiers de données vont être automatiquement mis à jour vers Solarus %2.
Voulez-vous mettre à jour cette quête ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="696"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="737"/>
        <source>An error occurred while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation>Une erreur s&apos;est produite lors de la mise à jour de la quête.
Votre quête a été conservée inchangée au format %1.</translation>
    </message>
    <message>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.</source>
        <translation type="vanished">Le format de cette quête (%1) is obsolète.
Vos fichiers de données vont être automatiquement mis à jour vers Solarus %2.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="720"/>
        <source>Upgrading quest data files</source>
        <translation>Mise à jour des données de la quête</translation>
    </message>
    <message>
        <source>An error occured while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation type="vanished">Une erreur s&apos;est produite lors de la mise à jour de la quête.
Votre quête a été conservée inchangée au format %1.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="785"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>Impossible de trouver le dossier &quot;assets&quot;.\nVérifiez que Solarus Quest Editor est correctement installé.</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1082"/>
        <source>Files are modified</source>
        <translation>Modifications non sauvegardées</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1083"/>
        <source>Do you want to save modifications before running the quest?</source>
        <translation>Enregistrer les modifications avant de lancer la quête ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1313"/>
        <source>Local Documentation Not Found</source>
        <translation>Documentation hors ligne non disponible</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1314"/>
        <source>The local copy of Solarus Documentation could not be found. Would you like to try going on line to find the documentaion?</source>
        <translation>La copie locale de la documentation de Solarus n&apos;a pas pu être trouvée. Voulez-vous consulter la documentation en ligne ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1733"/>
        <source>Stop quest</source>
        <translation>Arrêter la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1967"/>
        <source>Update existing sprites using this image</source>
        <translation>Mettre à jour les sprites utilisant cette image</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2034"/>
        <source>Unsaved changes</source>
        <translation>Changements non sauvegardés</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2035"/>
        <source>All files must be saved before this operation.
Do you want to save them now?</source>
        <translation>Tous les fichiers doivent être sauvegardés avant d&apos;effectuer cette opération.
Voulez-vous les sauvegarder maintenant ?</translation>
    </message>
    <message>
        <source>Quest terminated unexpectedly: %1</source>
        <translation type="vanished">La quête s&apos;est terminée avec une erreur : %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1900"/>
        <source>File modified</source>
        <translation>Fichier en cours de modification</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1901"/>
        <source>This file is open and has unsaved changes.
Please save it or close it before renaming.</source>
        <translation>Ce fichier est ouvert et a été modifié.\nVeuillez le sauvegarder ou le fermer avant de le renommer.</translation>
    </message>
    <message>
        <source>Rename resource</source>
        <translation type="vanished">Renommer la ressource</translation>
    </message>
    <message>
        <source>New id for %1 &apos;%2&apos;:</source>
        <translation type="vanished">Nouvel id pour %1 &apos;%2&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1965"/>
        <location filename="../src/widgets/main_window.cpp" line="1991"/>
        <source>Rename file</source>
        <translation>Renommer le fichier</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1966"/>
        <location filename="../src/widgets/main_window.cpp" line="1992"/>
        <source>New name for file &apos;%1&apos;:</source>
        <translation>Nouveau nom pour le fichier &apos;%1&apos; :</translation>
    </message>
    <message>
        <source>Invalid layer: %1</source>
        <translation type="vanished">Couche invalide : %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1716"/>
        <source>Missing show entity type action</source>
        <translation>Action d&apos;affichage d&apos;entité introuvable</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1789"/>
        <source>Play selected music</source>
        <translation>Jouer la musique sélectionnée</translation>
    </message>
    <message>
        <source>Solarus Quest Editor %1</source>
        <translation type="vanished">Solarus Quest Editor %1</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation type="vanished">Ctrl+N</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation type="vanished">Ctrl+Q</translation>
    </message>
    <message>
        <source>Quest</source>
        <translation type="vanished">Quête</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapEditor</name>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="14"/>
        <source>Map editor</source>
        <translation>Éditeur de map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="103"/>
        <source>Map id</source>
        <translation>Id de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="118"/>
        <source>Filename of the map (without extension)</source>
        <translation>Nom de fichier de la map (sans extension)</translation>
    </message>
    <message>
        <source>Open map script</source>
        <translation type="vanished">Ouvrir le script de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="131"/>
        <source>Open map script (F4)</source>
        <translation>Ouvrir le script de la map (F4)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="156"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="169"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="176"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="386"/>
        <source>Patterns</source>
        <translation>Motifs</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="425"/>
        <location filename="../src/widgets/map_editor.ui" line="495"/>
        <source>Edit tileset (Ctrl-T)</source>
        <translation>Modifier le tileset (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="441"/>
        <location filename="../src/widgets/map_editor.ui" line="511"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="465"/>
        <source>Contour generator</source>
        <translation>Générateur de contours</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="471"/>
        <source>Generate contour tiles around the selection</source>
        <translation>Générer des tiles de contour autour de la sélection</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="520"/>
        <source>Contour</source>
        <translation>Contour</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="570"/>
        <source>Generate contour tiles around the selection (Ctrl+B)</source>
        <translation>Générer des tiles de contour autour de la sélection (Ctrl+B)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="573"/>
        <source>Generate tiles</source>
        <translation>Générer les tiles</translation>
    </message>
    <message>
        <source>Countour</source>
        <translation type="vanished">Contour</translation>
    </message>
    <message>
        <source>Generate contour tiles</source>
        <translation type="vanished">Générer les tiles de contour</translation>
    </message>
    <message>
        <source>Border generator</source>
        <translation type="vanished">Générateur de bordures</translation>
    </message>
    <message>
        <source>Generate borders around the selection</source>
        <translation type="vanished">Générer des bordures autour de la sélection</translation>
    </message>
    <message>
        <source>Border set</source>
        <translation type="vanished">Border set</translation>
    </message>
    <message>
        <source>Generate border tiles around the selection (Ctrl+B)</source>
        <translation type="vanished">Générer des tiles de bordures autour de la sélection (Ctrl+B)</translation>
    </message>
    <message>
        <source>Generate borders</source>
        <translation type="vanished">Générer des bordures</translation>
    </message>
    <message>
        <source>Tiles</source>
        <translation type="vanished">Tiles</translation>
    </message>
    <message>
        <source>Autotiles</source>
        <translation type="vanished">Autotiles</translation>
    </message>
    <message>
        <source>Current border set</source>
        <translation type="vanished">Border set actif</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1140"/>
        <source>Width of the map in pixels</source>
        <translation>Largeur de la map en pixels</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="vanished">x</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1141"/>
        <source>Height of the map in pixels</source>
        <translation>Hauteur de la map en pixels</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="258"/>
        <source>Set a world</source>
        <translation>Monde</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="265"/>
        <source>A name to group maps together</source>
        <translation>Nom permettant de regrouper des maps</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="272"/>
        <source>Set a floor</source>
        <translation>Étage</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="293"/>
        <source>Floor number of the map</source>
        <translation>Numéro d&apos;étage de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="321"/>
        <source>Location in its world</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1145"/>
        <location filename="../src/widgets/map_editor.cpp" line="1146"/>
        <source>Coordinates of the map in its world (useful to make adjacent scrolling maps)</source>
        <translation>Coordonnées de la map dans son monde (utile pour faire des maps adjacentes avec scrolling)</translation>
    </message>
    <message>
        <source>,</source>
        <translation type="vanished">,</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="341"/>
        <location filename="../src/widgets/map_editor.ui" line="479"/>
        <location filename="../src/widgets/map_editor.cpp" line="241"/>
        <source>Tileset</source>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="350"/>
        <location filename="../src/widgets/map_editor.ui" line="418"/>
        <location filename="../src/widgets/map_editor.ui" line="488"/>
        <source>Tileset of the map</source>
        <translation>Tileset de la map</translation>
    </message>
    <message>
        <source>Refresh tileset</source>
        <translation type="vanished">Recharger le tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="134"/>
        <location filename="../src/widgets/map_editor.ui" line="357"/>
        <location filename="../src/widgets/map_editor.ui" line="428"/>
        <location filename="../src/widgets/map_editor.ui" line="498"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="186"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="217"/>
        <source>to</source>
        <translation>à</translation>
    </message>
    <message>
        <source>Edit tileset</source>
        <translation type="vanished">Modifier le tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="331"/>
        <location filename="../src/widgets/map_editor.cpp" line="259"/>
        <source>Music</source>
        <translation>Musique</translation>
    </message>
    <message>
        <source>Background music of the map</source>
        <translation type="vanished">Musique de fond de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="113"/>
        <source>Map size</source>
        <translation>Taille de la map</translation>
    </message>
    <message>
        <source>Number of layers</source>
        <translation type="vanished">Nombre de couches</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="131"/>
        <source>Lowest layer</source>
        <translation>Couche la plus basse</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="159"/>
        <source>Highest layer</source>
        <translation>Couche la plus haute</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="187"/>
        <source>Map world</source>
        <translation>Monde de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="205"/>
        <source>Map floor</source>
        <translation>Étage de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="223"/>
        <source>Map location</source>
        <translation>Coordonnées de la map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="277"/>
        <source>Edit entity</source>
        <translation>Modifier entité</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="362"/>
        <source>Move entities</source>
        <translation>Déplacer entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="414"/>
        <source>Resize entities</source>
        <translation>Redimensionner entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="496"/>
        <location filename="../src/widgets/map_editor.cpp" line="553"/>
        <source>Convert tiles</source>
        <translation>Convertir tiles</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="615"/>
        <source>Change pattern</source>
        <translation>Modifier le motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="669"/>
        <source>Set direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="731"/>
        <source>Set layer</source>
        <translation>Couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="775"/>
        <source>Increment layer</source>
        <translation>Monter d&apos;une couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="820"/>
        <source>Decrement layer</source>
        <translation>Descendre d&apos;une couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="865"/>
        <source>Bring to front</source>
        <translation>Mettre au premier plan</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="923"/>
        <source>Bring to back</source>
        <translation>Mettre en arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="983"/>
        <source>Add entities</source>
        <translation>Ajouter entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1030"/>
        <source>Delete entities</source>
        <translation>Supprimer entités</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1085"/>
        <source>File &apos;%1&apos; is not a map</source>
        <translation>Le fichier &apos;%1&apos; n&apos;est pas une map</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1090"/>
        <source>Map %1</source>
        <translation>Map %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1093"/>
        <source>Map &apos;%1&apos; has been modified. Save changes?</source>
        <translation>La map &apos;%1&apos; a été modifiée. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1105"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1121"/>
        <source>&lt;No music&gt;</source>
        <translation>&lt;Pas de musique&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1122"/>
        <source>&lt;Same as before&gt;</source>
        <translation>&lt;Garder la même musique&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1127"/>
        <location filename="../src/widgets/map_editor.cpp" line="1131"/>
        <source>(Tileset of the map)</source>
        <translation>(Tileset de la map)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1294"/>
        <source>Add tile</source>
        <translation>Ajouter un tile</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1295"/>
        <source>Add destination</source>
        <translation>Ajouter une destination</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1296"/>
        <source>Add teletransporter</source>
        <translation>Ajouter un téléporteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1297"/>
        <source>Add pickable</source>
        <translation>Ajouter un trésor ramassable</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1298"/>
        <source>Add destructible</source>
        <translation>Ajouter un destructible</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1299"/>
        <source>Add chest</source>
        <translation>Ajouter un coffre</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1300"/>
        <source>Add jumper</source>
        <translation>Ajouter un sauteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1301"/>
        <source>Add enemy</source>
        <translation>Ajouter un ennemi</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1302"/>
        <source>Add non-playing character</source>
        <translation>Ajouter un personnage non-joueur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1303"/>
        <source>Add block</source>
        <translation>Ajouter un bloc</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1304"/>
        <source>Add switch</source>
        <translation>Ajouter un bouton</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1305"/>
        <source>Add wall</source>
        <translation>Ajouter un mur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1306"/>
        <source>Add sensor</source>
        <translation>Ajouter un capteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1307"/>
        <source>Add crystal</source>
        <translation>Ajouter un cristal</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1308"/>
        <source>Add crystal block</source>
        <translation>Ajouter un plot de cristal</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1309"/>
        <source>Add shop treasure</source>
        <translation>Ajouter un article de magasin</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1310"/>
        <source>Add stream</source>
        <translation>Ajouter un flux</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1311"/>
        <source>Add door</source>
        <translation>Ajouter une porte</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1312"/>
        <source>Add stairs</source>
        <translation>Ajouter un escalier</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1313"/>
        <source>Add separator</source>
        <translation>Ajouter un séparateur</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1314"/>
        <source>Add custom entity</source>
        <translation>Ajouter une entité custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1367"/>
        <source>Save map as PNG file</source>
        <translation>Enregistrer la map vers un fichier PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1369"/>
        <source>PNG image (*.png)</source>
        <translation>Image PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1606"/>
        <location filename="../src/widgets/map_editor.cpp" line="1677"/>
        <source>Layer not empty</source>
        <translation>Couche non vide</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1607"/>
        <location filename="../src/widgets/map_editor.cpp" line="1678"/>
        <source>This layer is not empty: %1 entities will be destroyed.</source>
        <translation>Cette couche n&apos;est pas vide : %1 entités vont être détruites.</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2151"/>
        <source>%1,%2,%3 </source>
        <translation>%1,%2,%3 </translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2459"/>
        <source>Cannot generate tiles: some patterns of the contour &apos;%1&apos; are missing.
Please fix it in the tileset.</source>
        <translation>Impossible de générer les tiles : certains motifs du contour &apos;%1&apos; sont manquants.
Veuillez les corriger dans le tileset.</translation>
    </message>
    <message>
        <source>%1,%2 </source>
        <translation type="vanished">%1,%2 </translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2138"/>
        <source> - %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2140"/>
        <source>: %1</source>
        <translation> : %1</translation>
    </message>
    <message>
        <source>%1,%2</source>
        <translation type="vanished">%1,%2</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1535"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapModel</name>
    <message>
        <location filename="../src/map_model.cpp" line="51"/>
        <source>Cannot open map data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de map &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/map_model.cpp" line="102"/>
        <source>Cannot save map data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le fichier de map &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Entity exists in the editor but not in the data</source>
        <translation type="vanished">L&apos;entité existe dans l&apos;éditeur mais pas dans les données</translation>
    </message>
    <message>
        <source>Entity exists in the data but not in the editor</source>
        <translation type="vanished">L&apos;entité existe dans les données mais pas dans l&apos;éditeur</translation>
    </message>
    <message>
        <source>Missing entity</source>
        <translation type="vanished">Entité manquante</translation>
    </message>
    <message>
        <source>This entity is already on the map</source>
        <translation type="vanished">Cette entité est déjà sur la map</translation>
    </message>
    <message>
        <source>Invalid index of entity to add</source>
        <translation type="vanished">Index invalide pour l&apos;entité à ajouter</translation>
    </message>
    <message>
        <source>Cannot find entity to remove</source>
        <translation type="vanished">Impossible de trouver l&apos;entité à supprimer</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="79"/>
        <source>No direction</source>
        <translation>Aucune direction</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="297"/>
        <source>Unexpected entity type (not allowed in map files): %1</source>
        <translation>Type d&apos;entité inattendu (non autorisé dans les fichiers de map) : %1</translation>
    </message>
    <message>
        <source>Missing index of entity added</source>
        <translation type="vanished">Index de l&apos;entité à ajouter manquant</translation>
    </message>
    <message>
        <source>This entity already has a map index</source>
        <translation type="vanished">Cette entité a déjà un index</translation>
    </message>
    <message>
        <source>Entity being removed is not on the map</source>
        <translation type="vanished">L&apos;entité à supprimer n&apos;est pas sur la map</translation>
    </message>
    <message>
        <source>This entity is not on the map</source>
        <translation type="vanished">Cette entité n&apos;est pas sur la map</translation>
    </message>
    <message>
        <source>Missing new index of entity</source>
        <translation type="vanished">Nouvel index manquant pour l&apos;entité</translation>
    </message>
    <message>
        <location filename="../src/entities/block.cpp" line="34"/>
        <location filename="../src/entities/npc.cpp" line="34"/>
        <source>Any</source>
        <translation>Toutes</translation>
    </message>
    <message>
        <location filename="../src/entities/destination.cpp" line="35"/>
        <source>Keep the same direction</source>
        <translation>Conserver la même direction</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="34"/>
        <source>Initially lowered</source>
        <translation>Initialement baissé</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="35"/>
        <source>Initially raised</source>
        <translation>Initialement levé</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="37"/>
        <source>Generalized NPC (something)</source>
        <translation>PNJ généralisé (quelque chose)</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="38"/>
        <source>Usual NPC (somebody)</source>
        <translation>PNJ normal (quelqu&apos;un)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="33"/>
        <source>Spiral staircase (going upstairs)</source>
        <translation>Escalier en colimaçon (monte)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="34"/>
        <source>Spiral staircase (going downstairs)</source>
        <translation>Escalier en colimaçon (descend)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="35"/>
        <source>Straight staircase (going upstairs)</source>
        <translation>Escalier droit (monte)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="36"/>
        <source>Straight staircase (going downstairs)</source>
        <translation>Escalier droit (descend)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="37"/>
        <source>Platform stairs (same map)</source>
        <translation>Escalier de plate-forme (même map)</translation>
    </message>
    <message>
        <source>Inside a single floor</source>
        <translation type="vanished">Escalier de plate-forme</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="31"/>
        <source>Walkable</source>
        <translation>Bouton au sol</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="32"/>
        <source>Arrow target</source>
        <translation>Cible pour flèche</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="33"/>
        <source>Solid</source>
        <translation>Solide</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapScene</name>
    <message>
        <source>This entity is not on the map</source>
        <translation type="vanished">Cette entité n&apos;est pas sur la map</translation>
    </message>
    <message>
        <source>Cannot find added entity</source>
        <translation type="vanished">L&apos;entité ajoutée est introuvable</translation>
    </message>
    <message>
        <source>Inconsistent index of entity added</source>
        <translation type="vanished">L&apos;index de l&apos;entité ajoutée est incohérent</translation>
    </message>
    <message>
        <source>Missing entity graphics item</source>
        <translation type="vanished">Item graphique d&apos;entité manquant</translation>
    </message>
    <message>
        <source>Inconsistent index of entity being removed</source>
        <translation type="vanished">L&apos;index de l&apos;entité à supprimer est incohérent</translation>
    </message>
    <message>
        <source>Wrong entity item at this index</source>
        <translation type="vanished">L&apos;entité à cet index a un index incorrect</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapView</name>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="542"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">Entrée</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="550"/>
        <source>Resize</source>
        <translation>Redimensionner</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="551"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="558"/>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tile</source>
        <translation>Convertir en tile dynamique</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="564"/>
        <source>Change pattern...</source>
        <translation>Modifier le motif...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="571"/>
        <source>Change pattern of similar tiles...</source>
        <translation>Modifier le motif des tiles similaires...</translation>
    </message>
    <message>
        <source>Add border tiles</source>
        <translation type="vanished">Générer des tiles de bordure</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="577"/>
        <source>Generate borders around selection</source>
        <translation>Générer des bordures autour de la sélection</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="578"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="587"/>
        <source>One layer up</source>
        <translation>Monter d&apos;une couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="588"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="597"/>
        <source>One layer down</source>
        <translation>Descendre d&apos;une couche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="598"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="607"/>
        <source>Bring to front</source>
        <translation>Mettre au premier plan</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="608"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="617"/>
        <source>Bring to back</source>
        <translation>Mettre en arrière-plan</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="618"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="627"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="634"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="663"/>
        <source>Layer %1</source>
        <translation>Couche %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tiles</source>
        <translation>Convertir en tiles dynamiques</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tile</source>
        <translation>Convertir en tile statique</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tiles</source>
        <translation>Convertir en tiles statiques</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="814"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="833"/>
        <location filename="../src/widgets/map_view.cpp" line="841"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="834"/>
        <location filename="../src/widgets/map_view.cpp" line="843"/>
        <source>Up</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="835"/>
        <location filename="../src/widgets/map_view.cpp" line="845"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="836"/>
        <location filename="../src/widgets/map_view.cpp" line="847"/>
        <source>Down</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="842"/>
        <source>Right-up</source>
        <translation>Haut-droite</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="844"/>
        <source>Left-up</source>
        <translation>Haut-gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="846"/>
        <source>Left-down</source>
        <translation>Bas-gauche</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="848"/>
        <source>Right-down</source>
        <translation>Bas-droite</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MovingPatternsState</name>
    <message>
        <source>Move here</source>
        <translation type="obsolete">Déplacer ici</translation>
    </message>
    <message>
        <source>Duplicate here</source>
        <translation type="obsolete">Dupliquer ici</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MusicChooser</name>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="161"/>
        <source>Play music</source>
        <translation>Jouer la musique</translation>
    </message>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="182"/>
        <source>Stop music</source>
        <translation>Arrêter la musique</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewElementDialog</name>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="14"/>
        <source>New element</source>
        <translation>Nouvel élément</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="22"/>
        <source>License:</source>
        <translation>Licence :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="29"/>
        <location filename="../src/widgets/new_element_dialog.cpp" line="57"/>
        <source>Script name:</source>
        <translation>Nom du script :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="36"/>
        <source>Author:</source>
        <translation>Auteur :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="53"/>
        <source>New GLSL file</source>
        <translation>Nouveau fichier GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="54"/>
        <source>GLSL file name:</source>
        <translation>Nom de fichier GLSL :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="56"/>
        <source>New Lua script</source>
        <translation>Nouveau script Lua</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="59"/>
        <source>New folder</source>
        <translation>Nouveau dossier</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="60"/>
        <source>Folder name:</source>
        <translation>Nom du dossier :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewEntityUserPropertyDialog</name>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="14"/>
        <source>New user property</source>
        <translation>Nouvelle propriété utilisateur</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="24"/>
        <source>Key:</source>
        <translation>Clé :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="34"/>
        <source>Value:</source>
        <translation>Valeur :</translation>
    </message>
    <message>
        <source>New property key:</source>
        <translation type="vanished">Clé :</translation>
    </message>
    <message>
        <source>New property value:</source>
        <translation type="vanished">Valeur :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialog</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog.ui" line="14"/>
        <source>Create New Quest</source>
        <translation>Créer une nouvelle quête</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogContentsPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation>Page d&apos;assistant</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="17"/>
        <source>Initial Quest Contents</source>
        <translation>Contenu initial de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="20"/>
        <source>Choose the initial set of files for your quest.</source>
        <translation>Sélectionnez l&apos;ensemble de fichiers initial pour votre quête.</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="42"/>
        <source>Community Resources</source>
        <translation>Ressources communautaires</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="60"/>
        <source>Empty Quest</source>
        <translation>Quête vide</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start with default free resources and scripts.&lt;br/&gt;Uses GPL and Creative Commons licenses.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Commencer avec les ressources et scripts libres par défaut.&lt;br/&gt;Licences GPL et Creative Commons.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start from no quest data.&lt;br/&gt;You will have to create data files from scratch.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Commencer sans données de quête.&lt;br/&gt;Vous devrez créer les fichiers de données à partir de zéro.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogDirectoryPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation>Page d&apos;assistant</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="17"/>
        <source>Quest Directory</source>
        <translation>Dossier de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="20"/>
        <source>Choose a location on disk for the new quest.</source>
        <translation>Choisissez un emplacement sur le disque pour votre quête.</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="31"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="40"/>
        <source>Error Message</source>
        <translation>Message d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="122"/>
        <source>Directory not empty</source>
        <translation>Dossier existant</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="123"/>
        <source>The quest directory is not empty, are you sure you wish to continue?</source>
        <translation>Le dossier de la quête n&apos;est pas vide, voulez-vous vraiment continuer ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="148"/>
        <source>Select quest directory</source>
        <translation>Choisir le dossier de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="171"/>
        <source>A quest already exists in this directory.</source>
        <translation>Une quête existe déjà dans ce dossier.</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="177"/>
        <source>Parent directory does not exist.</source>
        <translation>Le dossier parent n&apos;existe pas.</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogTitlePage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation>Page d&apos;assistant</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="17"/>
        <source>Quest Title</source>
        <translation>Titre de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="20"/>
        <source>Enter the title of the new quest.</source>
        <translation>Entrez le titre de la nouvelle quête.</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewResourceElementDialog</name>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="20"/>
        <source>Create resource</source>
        <translation>Nouvelle ressource</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="31"/>
        <source>Resource id (filename)</source>
        <translation>Id de la ressource (nom de fichier)</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="41"/>
        <source>[required]</source>
        <translation>[obligatoire]</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="48"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="55"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="90"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="104"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="118"/>
        <source>[optional]</source>
        <translation>[optionnel]</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="62"/>
        <source>Create with default code</source>
        <translation>Créer avec le code par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="74"/>
        <source>Ownership</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="83"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="97"/>
        <source>License (data file)</source>
        <translation>Licence (fichier de données)</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="111"/>
        <source>License (script file)</source>
        <translation>Licence (fichier de script)</translation>
    </message>
    <message>
        <source>Resource id (filename):</source>
        <translation type="vanished">Id de la ressource (nom de fichier) :</translation>
    </message>
    <message>
        <source>Licence:</source>
        <translation type="vanished">Licence :</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation type="vanished">Auteur :</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">Description :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="48"/>
        <source>New map</source>
        <translation>Nouvelle map</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="49"/>
        <source>Map id (filename):</source>
        <translation>Id de la map (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="56"/>
        <source>New tileset</source>
        <translation>Nouveau tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="57"/>
        <source>Tileset id (filename):</source>
        <translation>Id du tileset (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="62"/>
        <source>New sprite</source>
        <translation>Nouveau sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="63"/>
        <source>Sprite id (filename):</source>
        <translation>Id du sprite (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="68"/>
        <source>New music</source>
        <translation>Nouvelle musique</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="69"/>
        <source>Music id (filename):</source>
        <translation>Id de la musique (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="74"/>
        <source>New sound</source>
        <translation>Nouveau son</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="75"/>
        <source>Sound id (filename):</source>
        <translation>Id du son (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="80"/>
        <source>New item</source>
        <translation>Nouvel item</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="81"/>
        <source>Item id (filename):</source>
        <translation>Id de l&apos;item (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="87"/>
        <source>New enemy</source>
        <translation>Nouvel ennemi</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="88"/>
        <source>Enemy id (filename):</source>
        <translation>Id de l&apos;ennemi (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="94"/>
        <source>New custom entity</source>
        <translation>Nouvelle entité custom</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="95"/>
        <source>Custom entity id (filename):</source>
        <translation>Id de l&apos;entité custom (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="101"/>
        <source>New language</source>
        <translation>Nouvelle langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="102"/>
        <source>Language id (filename):</source>
        <translation>Id de la langue (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="107"/>
        <source>New font</source>
        <translation>Nouvelle police</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="108"/>
        <source>Font id (filename):</source>
        <translation>Id de la police (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="113"/>
        <source>New shader</source>
        <translation>Nouveau shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="114"/>
        <source>Shader id (filename):</source>
        <translation>Id du shader (nom de fichier) :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="129"/>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="140"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewStringDialog</name>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="14"/>
        <source>New string</source>
        <translation>Nouveau texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="24"/>
        <source>New string key:</source>
        <translation>Nouvelle clé de texte :</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="34"/>
        <source>New string value:</source>
        <translation>Nouvelle valeur de texte :</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PackageDialog</name>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="14"/>
        <source>Build Solarus Package</source>
        <translation>Construire le package de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="27"/>
        <source>Save quest package to:</source>
        <translation>Sauvegarder le package vers :</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="43"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="69"/>
        <source>Building quest package...</source>
        <translation>Construction de l&apos;archive de la quête...</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="90"/>
        <source>Build successful!</source>
        <translation>Construction réussie !</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="116"/>
        <source>Build failed</source>
        <translation>Échec de la construction</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="138"/>
        <source>Exit Code:</source>
        <translation>Code de retour :</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="145"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="59"/>
        <source>Build</source>
        <translation>Construire</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="90"/>
        <source>Starting...
</source>
        <translation>Démarrage...</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="103"/>
        <source>Crashed</source>
        <translation>Crash</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="115"/>
        <source>Quest package location:</source>
        <translation>Emplacement du package de la quête :</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="117"/>
        <source>Solarus Packages (*.solarus)</source>
        <translation>Packages Solarus (*.solarus)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PatternPickerDialog</name>
    <message>
        <location filename="../src/widgets/pattern_picker_dialog.ui" line="14"/>
        <source>Pick a pattern</source>
        <translation>Sélectionner un motif</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Quest</name>
    <message>
        <location filename="../src/quest.cpp" line="289"/>
        <source>Unknown resource type</source>
        <translation>Type de ressource inconnu</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1199"/>
        <source>Invalid file name: &apos;%1&apos;</source>
        <translation>Nom de fichier invalide : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1257"/>
        <source>File &apos;%1&apos; does not exist</source>
        <translation>Le fichier &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1270"/>
        <source>File &apos;%1&apos; already exists</source>
        <translation>Le fichier &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1331"/>
        <source>Wrong script name: &apos;%1&apos; (should end with &apos;.lua&apos;)</source>
        <translation>Nom de script incorrect : &apos;%1&apos; (extension &apos;.lua&apos; attendue)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1407"/>
        <source>Cannot create file &apos;%1&apos;</source>
        <translation>Impossible de créer le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1196"/>
        <source>Empty file name</source>
        <translation>Nom de fichier vide</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="140"/>
        <source>No quest</source>
        <translation>Pas de quête</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="145"/>
        <source>Missing Solarus version in quest.dat</source>
        <translation>Version de Solarus manquante dans quest.dat</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1293"/>
        <source>File &apos;%1&apos; is not a folder</source>
        <translation>Le fichier &apos;%1&apos; n&apos;est pas un dossier</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1306"/>
        <source>File &apos;%1&apos; is a folder</source>
        <translation>Le fichier &apos;%1&apos; est un dossier</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1456"/>
        <source>Cannot read file &apos;%1&apos;</source>
        <translation>Impossible de lire le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1427"/>
        <location filename="../src/quest.cpp" line="1464"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>Impossible d&apos;écrire le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1357"/>
        <source>Wrong GLSL shader file name: &apos;%1&apos; (should end with &apos;.glsl&apos;)</source>
        <translation>Nom de fichier GLSL invalide : &apos;%1&apos; (extension &apos;.glsl&apos; attendue)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1392"/>
        <source>Wrong image file name: &apos;%1&apos; (should end with &apos;.png&apos;)</source>
        <translation>Nom fichier image incorrect : &apos;%1&apos; (extension &apos;.png&apos; attendue)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1779"/>
        <source>Cannot load image file: &apos;%1&apos;</source>
        <translation>Impossible de charger le fichier d&apos;image : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1784"/>
        <source>This sprite already exists: &apos;%1&apos;</source>
        <translation>Le sprite existe déjà : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1806"/>
        <source>Cannot create folder &apos;%1&apos;: parent folder does not exist</source>
        <translation>Impossible de créer le dossier &apos;%1&apos; : le dossier parent n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1812"/>
        <location filename="../src/quest.cpp" line="1849"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>Impossible de créer le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1982"/>
        <source>Resource &apos;%1&apos; already exists</source>
        <translation>La ressource &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1999"/>
        <location filename="../src/quest.cpp" line="2053"/>
        <source>Cannot rename file &apos;%1&apos;</source>
        <translation>Impossible de renommer le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2191"/>
        <source>Cannot delete file &apos;%1&apos;</source>
        <translation>Impossible de supprimer le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2231"/>
        <source>Cannot delete folder &apos;%1&apos;</source>
        <translation>Impossible de supprimer le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2268"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>Impossible de supprimer le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2274"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>Impossible de supprimer le dossier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2121"/>
        <source>Same source and destination id</source>
        <translation>Id source et destination identiques</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2128"/>
        <source>A resource with id &apos;%1&apos; already exists</source>
        <translation>Une ressource avec l&apos;id &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2133"/>
        <location filename="../src/quest.cpp" line="2177"/>
        <source>No such resource: &apos;%1&apos;</source>
        <translation>Ressource inexistante : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1234"/>
        <source>File &apos;%1&apos; is not in this quest</source>
        <translation>Le fichier &apos;%1&apos; ne fait pas partie de cette quête</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestDatabase</name>
    <message>
        <location filename="../src/quest_database.cpp" line="36"/>
        <source>Map</source>
        <comment>resource_type</comment>
        <extracomment>To describe the type of resource itself like: this is a Map.</extracomment>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="37"/>
        <source>Tileset</source>
        <comment>resource_type</comment>
        <translation>Tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="38"/>
        <source>Sprite</source>
        <comment>resource_type</comment>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="39"/>
        <source>Music</source>
        <comment>resource_type</comment>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="40"/>
        <source>Sound</source>
        <comment>resource_type</comment>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="41"/>
        <source>Item</source>
        <comment>resource_type</comment>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="42"/>
        <source>Enemy</source>
        <comment>resource_type</comment>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="43"/>
        <source>Custom entity</source>
        <comment>resource_type</comment>
        <translation>Entité custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="44"/>
        <source>Language</source>
        <comment>resource_type</comment>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="45"/>
        <source>Font</source>
        <comment>resource_type</comment>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="46"/>
        <source>Shader</source>
        <comment>resource_type</comment>
        <translation>Shader</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="51"/>
        <source>Map</source>
        <comment>resource_element</comment>
        <extracomment>To be used with a specific element id like: Rename Map X.</extracomment>
        <translation>la Map</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="52"/>
        <source>Tileset</source>
        <comment>resource_element</comment>
        <translation>le Tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="53"/>
        <source>Sprite</source>
        <comment>resource_element</comment>
        <translation>le Sprite</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="54"/>
        <source>Music</source>
        <comment>resource_element</comment>
        <translation>la Musique</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="55"/>
        <source>Sound</source>
        <comment>resource_element</comment>
        <translation>le Son</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="56"/>
        <source>Item</source>
        <comment>resource_element</comment>
        <translation>l&apos;Item</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="57"/>
        <source>Enemy</source>
        <comment>resource_element</comment>
        <translation>l&apos;Ennemi</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="58"/>
        <source>Custom entity</source>
        <comment>resource_element</comment>
        <translation>l&apos;Entité custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="59"/>
        <source>Language</source>
        <comment>resource_element</comment>
        <translation>la Langue</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="60"/>
        <source>Font</source>
        <comment>resource_element</comment>
        <translation>la Police</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="61"/>
        <source>Shader</source>
        <comment>resource_element</comment>
        <translation>le Shader</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="65"/>
        <source>Maps folder</source>
        <translation>Dossier des Maps</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="66"/>
        <source>Tilesets folder</source>
        <translation>Dossier des Tilesets</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="67"/>
        <source>Sprites folder</source>
        <translation>Dossier des Sprites</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="68"/>
        <source>Musics folder</source>
        <translation>Dossier des Musiques</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="69"/>
        <source>Sounds folder</source>
        <translation>Dossier des Sons</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="70"/>
        <source>Items folder</source>
        <translation>Dossier des Items</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="71"/>
        <source>Enemies folder</source>
        <translation>Dossier des Ennemis</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="72"/>
        <source>Custom entities folder</source>
        <translation>Dossier des Entités custom</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="73"/>
        <source>Languages folder</source>
        <translation>Dossier des Langues</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="74"/>
        <source>Fonts folder</source>
        <translation>Dossier des Polices</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="75"/>
        <source>Shaders folder</source>
        <translation>Dossier des Shaders</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="79"/>
        <source>New map...</source>
        <translation>Nouvelle map...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="80"/>
        <source>New tileset...</source>
        <translation>Nouveau tileset...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="81"/>
        <source>New sprite...</source>
        <translation>Nouveau sprite...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="82"/>
        <source>New music...</source>
        <translation>Nouvelle musique...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="83"/>
        <source>New sound...</source>
        <translation>Nouveau son...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="84"/>
        <source>New item...</source>
        <translation>Nouvel item...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="85"/>
        <source>New enemy breed...</source>
        <translation>Nouveau modèle d&apos;ennemi...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="86"/>
        <source>New custom entity model...</source>
        <translation>Nouveau modèle d&apos;entité custom...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="87"/>
        <source>New language...</source>
        <translation>Nouvelle langue...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="88"/>
        <source>New font...</source>
        <translation>Nouvelle police...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="89"/>
        <source>New shader...</source>
        <translation>Nouveau shader...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="125"/>
        <source>No quest</source>
        <translation>Pas de quête</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="130"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>Impossible d&apos;écrire le fichier &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestFilesModel</name>
    <message>
        <source>Resource</source>
        <translation type="vanished">Ressource</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="362"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="359"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Resource description</source>
        <translation type="vanished">Description de la resource</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="365"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="368"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="371"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="591"/>
        <source>Quest</source>
        <translation>Quête</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="596"/>
        <source>Main Lua script</source>
        <translation>Script Lua principal</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="610"/>
        <source>Dialogs file</source>
        <translation>Fichier des dialogues</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="614"/>
        <source>Strings file</source>
        <translation>Fichier des textes</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="622"/>
        <source>Map script</source>
        <translation>Script de map</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="625"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="629"/>
        <source>GLSL shader code</source>
        <translation>Code de shader GLSL</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="637"/>
        <source>Tileset tiles image</source>
        <translation>Image des tiles de tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="641"/>
        <source>Tileset sprites image</source>
        <translation>Image des sprites de tileset</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="644"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="649"/>
        <source>Data file</source>
        <translation>Fichier de données</translation>
    </message>
    <message>
        <source>Lua script</source>
        <translation type="vanished">Script Lua</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="778"/>
        <source>%1 (file not found)</source>
        <translation>%1 (fichier introuvable)</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="788"/>
        <source>%1 (not in the quest)</source>
        <translation>%1 (non inclus dans la quête)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestProperties</name>
    <message>
        <location filename="../src/quest_properties.cpp" line="47"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="58"/>
        <source>No quest</source>
        <translation>Pas de quête</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="63"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>Impossible d&apos;écrire le fichier &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestPropertiesEditor</name>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="62"/>
        <source>Quest information</source>
        <translation>Informations de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="81"/>
        <source>Solarus version</source>
        <translation>Version de Solarus</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="88"/>
        <source>Version of the engine your data files are compatible with</source>
        <translation>Version du moteur avec lequel vos fichiers de quête sont compatible</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="103"/>
        <source>Write directory</source>
        <translation>Dossier d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="115"/>
        <source>Folder where to write savegames, relative to &quot;$HOME/.solarus/&quot;.
Must identify your quest to avoid confusion with other quests.</source>
        <translation>Dossier où écrire les sauvegardes, par rapport à &quot;$HOME/.solarus/&quot;.
Permet d&apos;identifier votre quête pour éviter toute confusion avec d&apos;autres quêtes.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="133"/>
        <source>Quest title</source>
        <translation>Titre de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="145"/>
        <source>The name of your quest.</source>
        <translation>Le nom de votre quête.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="159"/>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="171"/>
        <source>One line describing your quest.</source>
        <translation>Une ligne qui décrit votre quête.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="185"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="224"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="236"/>
        <source>People who develop this quest.</source>
        <translation>Les personnes qui développent ce projet.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="250"/>
        <source>Quest version</source>
        <translation>Version de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="262"/>
        <source>Current release of your quest.</source>
        <translation>Version actuelle de votre jeu.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="276"/>
        <source>Release date</source>
        <translation>Date de sortie</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="287"/>
        <source>In progress</source>
        <translation>En développement</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="294"/>
        <source>Released</source>
        <translation>Sortie le</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="329"/>
        <source>Status and date of the current release.</source>
        <translation>Statut et date de sortie de la version actuelle.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="343"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="355"/>
        <source>Official website of the quest.</source>
        <translation>Site officiel de votre projet.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="378"/>
        <source>Video</source>
        <translation>Vidéo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="389"/>
        <source>Video options</source>
        <translation>Options vidéo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="398"/>
        <source>Smooth camera scrolling</source>
        <translation>Lisser le défilement de la caméra</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="405"/>
        <source>Enables subpixel smoothing of camera scrolling movement.</source>
        <translation>Active le lissage sous-pixel lors du défilement de la caméra</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="415"/>
        <source>Synchronize update rate with video</source>
        <translation>Synchroniser le taux de rafraîchissement avec la vidéo</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="422"/>
        <source>Keeps the main loop update rate the same as the video refresh rate.</source>
        <translation>Applique le taux de rafraîchissement de la boucle principale selon celui de la vidéo.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="210"/>
        <source>A more detailed description of your quest.</source>
        <translation>Une description plus détaillée de votre quête.</translation>
    </message>
    <message>
        <source>Quest size</source>
        <translation type="vanished">Taille de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="507"/>
        <source>Size of the logical game area (before any scaling).
This will be the visible space of the current map.</source>
        <translation>Taille de l&apos;espace logique de jeu (avant tout agrandissement).
Définit l&apos;espace visible de la map actuelle.</translation>
    </message>
    <message>
        <source>Window title</source>
        <translation type="vanished">Titre de la fenêtre</translation>
    </message>
    <message>
        <source>Usually the title of your game.</source>
        <translation type="vanished">En général le titre de votre jeu.</translation>
    </message>
    <message>
        <source>Directory where your quest will write its savegames and setting files</source>
        <translation type="vanished">Dossier dans lequel votre quête va enregistrer ses sauvegardes de jeu et fichiers de configuration</translation>
    </message>
    <message>
        <source>Title bar</source>
        <translation type="vanished">Barre de titre</translation>
    </message>
    <message>
        <source>Title of the window</source>
        <translation type="vanished">Titre de la fenêtre</translation>
    </message>
    <message>
        <source>Folder where to write savegames, relative to &quot;$HOME/.solarus/&quot;.
Should identify your quest to avoid confusion with other quests.</source>
        <translation type="vanished">Dossier où écrire les sauvegardes, par rapport à &quot;$HOME/.solarus/&quot;.
Permet d&apos;identifier votre quête pour éviter toute confusion avec d&apos;autres quêtes.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="436"/>
        <source>Normal quest size</source>
        <translation>Taille normale de la quête</translation>
    </message>
    <message>
        <source>Size of the logical game area (before any scaling).
This is the visible space of the current map.</source>
        <translation type="vanished">Taille de l&apos;espace logique de jeu (avant tout agrandissement).</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="525"/>
        <source>Minimum quest size</source>
        <translation>Taille minimum de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="596"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="681"/>
        <source>Only useful to support a range of logical sizes.</source>
        <translation>Utile uniquement pour supporter plusieurs tailles.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="610"/>
        <source>Maximum quest size</source>
        <translation>Taille maximum de la quête</translation>
    </message>
    <message>
        <source>Usual size of the game screen in pixels</source>
        <translation type="vanished">Taille courante de l&apos;écran de jeu en pixels</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="466"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="555"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="640"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <source>Minimum size of the game screen in pixels</source>
        <translation type="vanished">Taille minimum de l&apos;écran de jeu en pixels</translation>
    </message>
    <message>
        <source>Maximum size of the game screen in pixels</source>
        <translation type="vanished">Taille maximum de l&apos;écran de jeu en pixels</translation>
    </message>
    <message>
        <source># Write directory #
Directory where your quest will write its savegames and setting files.
It will be a subdirectory of &apos;$HOME/.solarus/&apos;, automatically created by the engine. 
Its name should identify your quest, to avoid confusion with other Solarus quests that might also be installed on the user&apos;s machine. 
You must define it before you can use savegames or setting files.

# Title bar #
Title of the window. You should probably put the title of your game here.
</source>
        <translation type="vanished"># Dossier d&apos;écriture #
Dossier dans lequel votre quête va enregistrer ses sauvegardes de jeu et fichiers de configuration.
Il sera un sous-dossier de &apos;$HOME/.solarus/&apos;, crée automatiquement par le moteur.
Son nom devrait identifier votre quête, pour éviter la confusion avec d&apos;autres quête de Solarus qui pourrait aussi être installé sur la machine de l&apos;utilisateur.
Vous devez le définir avant de pouvoir utiliser les sauvegardes de jeu ou les fichiers de configuration.

# Barre de titre #
Titre de la fenêtre. Vous devriez probablement mettre le titre de votre jeu ici.
</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="63"/>
        <source>Change write directory</source>
        <translation>Changer dossier d&apos;enregistrement</translation>
    </message>
    <message>
        <source>Change title bar</source>
        <translation type="vanished">Changer barre de titre</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="93"/>
        <source>Change title</source>
        <translation>Changer le titre</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="122"/>
        <source>Change summary</source>
        <translation>Changer le résumé</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="148"/>
        <source>Change detailed description</source>
        <translation>Changer la description détaillée</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="174"/>
        <source>Change author</source>
        <translation>Changer l&apos;auteur</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="200"/>
        <source>Change quest version</source>
        <translation>Changer la version</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="226"/>
        <source>Change release date</source>
        <translation>Changer la date de sortie</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="252"/>
        <source>Change website</source>
        <translation>Changer le site web</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="279"/>
        <source>Change normal size</source>
        <translation>Changer taille normale</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="309"/>
        <source>Change minimum size</source>
        <translation>Changer taille minimum</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="353"/>
        <source>Change maximum size</source>
        <translation>Changer taille maximum</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="397"/>
        <source>Set subpixel camera</source>
        <translation>Changer le défilement lisse</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="424"/>
        <source>Set dynamic timestep</source>
        <translation>Changer la synchronisation du taux de rafraîchissement</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="463"/>
        <source>Quest properties</source>
        <translation>Propriétés de la quête</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="466"/>
        <source>Quest properties have been modified. Save changes?</source>
        <translation>Les propriétés de la quête ont été modifiées. Enregistrer les changements ?</translation>
    </message>
    <message>
        <source>Quest properties has been modified. Save changes?</source>
        <translation type="vanished">Les propriétés de la quête ont été modifié. Enregistrer les changements ?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestResources</name>
    <message>
        <source>Map</source>
        <comment>resource_type</comment>
        <extracomment>To describe the type of resource itself like: New Map.</extracomment>
        <translation type="vanished">Map</translation>
    </message>
    <message>
        <source>Tileset</source>
        <comment>resource_type</comment>
        <translation type="vanished">Tileset</translation>
    </message>
    <message>
        <source>Sprite</source>
        <comment>resource_type</comment>
        <translation type="vanished">Sprite</translation>
    </message>
    <message>
        <source>Music</source>
        <comment>resource_type</comment>
        <translation type="vanished">Musique</translation>
    </message>
    <message>
        <source>Sound</source>
        <comment>resource_type</comment>
        <translation type="vanished">Son</translation>
    </message>
    <message>
        <source>Item</source>
        <comment>resource_type</comment>
        <translation type="vanished">Item</translation>
    </message>
    <message>
        <source>Enemy</source>
        <comment>resource_type</comment>
        <translation type="vanished">Ennemi</translation>
    </message>
    <message>
        <source>Custom entity</source>
        <comment>resource_type</comment>
        <translation type="vanished">Entité custom</translation>
    </message>
    <message>
        <source>Language</source>
        <comment>resource_type</comment>
        <translation type="vanished">Langue</translation>
    </message>
    <message>
        <source>Font</source>
        <comment>resource_type</comment>
        <translation type="vanished">Police</translation>
    </message>
    <message>
        <source>Map</source>
        <comment>resource_element</comment>
        <extracomment>To be used with a specific element id like: Rename Map X.</extracomment>
        <translation type="vanished">la Map</translation>
    </message>
    <message>
        <source>Tileset</source>
        <comment>resource_element</comment>
        <translation type="vanished">le Tileset</translation>
    </message>
    <message>
        <source>Sprite</source>
        <comment>resource_element</comment>
        <translation type="vanished">le Sprite</translation>
    </message>
    <message>
        <source>Music</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Musique</translation>
    </message>
    <message>
        <source>Sound</source>
        <comment>resource_element</comment>
        <translation type="vanished">le Son</translation>
    </message>
    <message>
        <source>Item</source>
        <comment>resource_element</comment>
        <translation type="vanished">l&apos;Item</translation>
    </message>
    <message>
        <source>Enemy</source>
        <comment>resource_element</comment>
        <translation type="vanished">l&apos;Ennemi</translation>
    </message>
    <message>
        <source>Custom entity</source>
        <comment>resource_element</comment>
        <translation type="vanished">l&apos;Entité custom</translation>
    </message>
    <message>
        <source>Language</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Langue</translation>
    </message>
    <message>
        <source>Font</source>
        <comment>resource_element</comment>
        <translation type="vanished">la Police</translation>
    </message>
    <message>
        <source>Map folder</source>
        <translation type="vanished">Dossier des Maps</translation>
    </message>
    <message>
        <source>Tileset folder</source>
        <translation type="vanished">Dossier des Tilesets</translation>
    </message>
    <message>
        <source>Sprite folder</source>
        <translation type="vanished">Dossier des Sprites</translation>
    </message>
    <message>
        <source>Music folder</source>
        <translation type="vanished">Dossier des Musiques</translation>
    </message>
    <message>
        <source>Sound folder</source>
        <translation type="vanished">Dossier des Sons</translation>
    </message>
    <message>
        <source>Item folder</source>
        <translation type="vanished">Dossier des Items</translation>
    </message>
    <message>
        <source>Enemy folder</source>
        <translation type="vanished">Dossier des Ennemis</translation>
    </message>
    <message>
        <source>Custom entity folder</source>
        <translation type="vanished">Dossier des Entités custom</translation>
    </message>
    <message>
        <source>Language folder</source>
        <translation type="vanished">Dossier des Langues</translation>
    </message>
    <message>
        <source>Font folder</source>
        <translation type="vanished">Dossier des Polices</translation>
    </message>
    <message>
        <source>New map...</source>
        <translation type="vanished">Nouvelle map...</translation>
    </message>
    <message>
        <source>New tileset...</source>
        <translation type="vanished">Nouveau tileset...</translation>
    </message>
    <message>
        <source>New sprite...</source>
        <translation type="vanished">Nouveau sprite...</translation>
    </message>
    <message>
        <source>New music...</source>
        <translation type="vanished">Nouvelle musique...</translation>
    </message>
    <message>
        <source>New sound...</source>
        <translation type="vanished">Nouveau son...</translation>
    </message>
    <message>
        <source>New item...</source>
        <translation type="vanished">Nouvel item...</translation>
    </message>
    <message>
        <source>New enemy breed...</source>
        <translation type="vanished">Nouveau modèle d&apos;ennemi...</translation>
    </message>
    <message>
        <source>New custom entity model...</source>
        <translation type="vanished">Nouveau modèle d&apos;entité custom...</translation>
    </message>
    <message>
        <source>New language...</source>
        <translation type="vanished">Nouvelle langue...</translation>
    </message>
    <message>
        <source>New font...</source>
        <translation type="vanished">Nouvelle police...</translation>
    </message>
    <message>
        <source>No quest</source>
        <translation type="vanished">Pas de quête</translation>
    </message>
    <message>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation type="vanished">Impossible d&apos;écrire le fichier &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestRunner</name>
    <message>
        <source>Failed to run quest</source>
        <translation type="vanished">Impossible d&apos;exécuter la quête</translation>
    </message>
    <message>
        <source>Cannot start quest process: no program name</source>
        <translation type="vanished">Impossible de démarrer le processus de la quête : nom du programme manquant</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestTreeView</name>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="458"/>
        <source>New folder...</source>
        <translation>Nouveau dossier...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="466"/>
        <source>New script...</source>
        <translation>Nouveau script...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="65"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="544"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="59"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="503"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="511"/>
        <source>Play</source>
        <translation>Jouer</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="565"/>
        <source>Open Script</source>
        <translation>Ouvrir le Script</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="576"/>
        <source>Open Dialogs</source>
        <translation>Ouvrir les Dialogues</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="582"/>
        <source>Open Strings</source>
        <translation>Ouvrir les Textes</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="72"/>
        <source>Rename...</source>
        <translation>Renommer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="680"/>
        <source>Change description...</source>
        <translation>Changer la description...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="80"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">Entrée</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="73"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="88"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="405"/>
        <source>Add to quest as %1...</source>
        <translation>Ajouter à la quête en tant que %1...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="429"/>
        <source>New sprite from image...</source>
        <translation>Nouveau sprite à partir de l&apos;image...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="443"/>
        <source>New GLSL file...</source>
        <translation>Nouveau fichier GLSL...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="499"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="625"/>
        <source>Open Properties</source>
        <translation>Ouvrir les propriétés</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="631"/>
        <source>Explore folder</source>
        <translation>Explorer le dossier</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="87"/>
        <source>Author and license...</source>
        <translation>Auteur et licence...</translation>
    </message>
    <message>
        <source>New folder</source>
        <translation type="vanished">Nouveau dossier</translation>
    </message>
    <message>
        <source>Folder name:</source>
        <translation type="vanished">Nom du dossier :</translation>
    </message>
    <message>
        <source>New Lua script</source>
        <translation type="vanished">Nouveau script Lua</translation>
    </message>
    <message>
        <source>File name:</source>
        <translation type="vanished">Nom de fichier :</translation>
    </message>
    <message>
        <source>New GLSL file</source>
        <translation type="vanished">Nouveau fichier GLSL</translation>
    </message>
    <message>
        <source>Rename resource</source>
        <translation type="vanished">Renommer la ressource</translation>
    </message>
    <message>
        <source>New id for %1 &apos;%2&apos;:</source>
        <translation type="vanished">Nouvel id pour %1 &apos;%2&apos; :</translation>
    </message>
    <message>
        <source>Rename file</source>
        <translation type="vanished">Renommer le fichier</translation>
    </message>
    <message>
        <source>New name for file &apos;%1&apos;:</source>
        <translation type="vanished">Nouveau nom pour le fichier &apos;%1&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1101"/>
        <source>Change description</source>
        <translation>Changer la description</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1102"/>
        <source>New description for %1 &apos;%2&apos;:</source>
        <translation>Nouvelle description pour %1 &apos;%2&apos; :</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1139"/>
        <source>File information for &apos;%1&apos;</source>
        <translation>Informations du fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1141"/>
        <source>File information for %1 selected items</source>
        <translation>Informations des %1 éléments sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1222"/>
        <source>Do you really want to delete &apos;%1&apos;?</source>
        <translation>Voulez-vous vraiment supprimer &apos;%1&apos; ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1223"/>
        <source>Do you really want to delete these %1 items?</source>
        <translation>Voulez-vous vraiment supprimer ces %1 éléments ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1225"/>
        <source>Delete confirmation</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <source>Do you really want to delete %1 &apos;%2&apos;?</source>
        <translation type="vanished">Voulez-vous vraiment supprimer %1 &apos;%2&apos; ?</translation>
    </message>
    <message>
        <source>Folder is not empty</source>
        <translation type="vanished">Le dossier n&apos;est pas vide</translation>
    </message>
    <message>
        <source>Do you really want to delete folder &apos;%1&apos;?</source>
        <translation type="vanished">Voulez-vous vraiment supprimer le dossier &apos;%1&apos; ?</translation>
    </message>
    <message>
        <source>Do you really want to delete file &apos;%1&apos;?</source>
        <translation type="vanished">Voulez-vous vraiment supprimer le fichier &apos;%1&apos; ?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SettingsDialog</name>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paramètres</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="14"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="24"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation type="vanished">Répertoire</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="84"/>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="92"/>
        <source>Working directory:</source>
        <translation>Répertoire de travail :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="102"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="111"/>
        <source>Restore open tabs at startup</source>
        <translation>Restaurer les onglets au démarrage</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="121"/>
        <source>Running</source>
        <translation>Exécution</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="129"/>
        <source>Save modified files before running:</source>
        <translation>Enregistrer les fichiers avant de lancer :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="137"/>
        <source>Ask</source>
        <translation>Demander</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="142"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="147"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="170"/>
        <source>No audio</source>
        <translation>Désactiver l&apos;audio</translation>
    </message>
    <message>
        <source>Video acceleration</source>
        <translation type="vanished">Accélération vidéo</translation>
    </message>
    <message>
        <source>Show console (only needed on Windows)</source>
        <translation type="vanished">Afficher une console (nécéssaire uniquement sous Windows)</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="179"/>
        <source>Quest size:</source>
        <translation>Taille de la quête :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="191"/>
        <source>Force Software Rendering</source>
        <translation>Forcer le rendu software</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="198"/>
        <source>Suspend when unfocused</source>
        <translation>Suspendre si la fenêtre perd le focus</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="292"/>
        <source>Tabulation</source>
        <translation>Tabulation</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="298"/>
        <source>Length:</source>
        <translation>Longueur :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="331"/>
        <source>Replace by space</source>
        <translation>Remplacer par des espaces</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="354"/>
        <source>External Editor</source>
        <translation>Éditeur externe</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="366"/>
        <source>Command:</source>
        <translation>Commande :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="376"/>
        <source>executable %f %p</source>
        <translation>éxécutable %f %p</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="395"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="433"/>
        <location filename="../src/widgets/settings_dialog.ui" line="571"/>
        <location filename="../src/widgets/settings_dialog.ui" line="625"/>
        <location filename="../src/widgets/settings_dialog.ui" line="772"/>
        <location filename="../src/widgets/settings_dialog.ui" line="912"/>
        <source>Default zoom:</source>
        <translation>Zoom par défaut :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="458"/>
        <location filename="../src/widgets/settings_dialog.ui" line="650"/>
        <location filename="../src/widgets/settings_dialog.ui" line="937"/>
        <source>Grid:</source>
        <translation>Grille :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="491"/>
        <location filename="../src/widgets/settings_dialog.ui" line="693"/>
        <location filename="../src/widgets/settings_dialog.ui" line="824"/>
        <location filename="../src/widgets/settings_dialog.ui" line="970"/>
        <source>Show at opening</source>
        <translation>Afficher à l&apos;ouverture</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="508"/>
        <location filename="../src/widgets/settings_dialog.ui" line="716"/>
        <location filename="../src/widgets/settings_dialog.ui" line="987"/>
        <source>Default size:</source>
        <translation>Taille par défaut :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="515"/>
        <location filename="../src/widgets/settings_dialog.ui" line="723"/>
        <location filename="../src/widgets/settings_dialog.ui" line="994"/>
        <source>Style:</source>
        <translation>Style :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="522"/>
        <location filename="../src/widgets/settings_dialog.ui" line="730"/>
        <location filename="../src/widgets/settings_dialog.ui" line="831"/>
        <location filename="../src/widgets/settings_dialog.ui" line="1001"/>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="565"/>
        <source>Tileset graphics view</source>
        <translation>Vue graphique du tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="611"/>
        <source>Sprite editor</source>
        <translation>Éditeur de sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="425"/>
        <location filename="../src/widgets/settings_dialog.ui" line="617"/>
        <source>Main graphics view</source>
        <translation>Vue graphique principale</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="30"/>
        <source>Appaerance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="36"/>
        <source>Theme:</source>
        <translation>Thème :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="50"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="55"/>
        <source>Light mode</source>
        <translation>Mode clair</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="60"/>
        <source>Dark mode</source>
        <translation>Mode sombre</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="737"/>
        <source>Auto detect size</source>
        <translation>Détecter la taille automatiquement</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="764"/>
        <source>Previewer graphics view</source>
        <translation>Prévisualisation</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="797"/>
        <source>Origin:</source>
        <translation>Origine :</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="vanished">x</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="222"/>
        <source>Text editor</source>
        <translation>Éditeur de texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="228"/>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="234"/>
        <source>Family:</source>
        <translation>Famille :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="268"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="419"/>
        <source>Map editor</source>
        <translation>Éditeur de map</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="898"/>
        <source>Tileset editor</source>
        <translation>Éditeur de tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="904"/>
        <source>Graphics view</source>
        <translation>Vue graphique</translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="vanished">Couleur de fond :</translation>
    </message>
    <message>
        <source>Default grid size:</source>
        <translation type="vanished">Taille de grille par défaut :</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="108"/>
        <source>Select external editor</source>
        <translation>Sélectionnez un éditeur l&apos;externe</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="186"/>
        <source>Restore default settings</source>
        <translation>Restaurer les paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="187"/>
        <source>Do you really want to restore default settings?</source>
        <translation>Voulez-vous vraiment restaurer les paramètres par défaut ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="355"/>
        <source>Working directory</source>
        <translation>Répertoire de travail</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="994"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="995"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="996"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="997"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="998"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
    <message>
        <source>Select background color</source>
        <translation type="vanished">Sélectionnez une couleur de fond</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderEditor</name>
    <message>
        <source>Shader properties</source>
        <translation type="vanished">Propriétés du shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="81"/>
        <source>Shader id</source>
        <translation>Id du shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="88"/>
        <source>Filename of the shader program (without extension)</source>
        <translation>Nom de fichier du programme de shader (sans extension)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="104"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="117"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="124"/>
        <location filename="../src/widgets/shader_editor.cpp" line="68"/>
        <source>Scaling factor</source>
        <translation>Facteur d&apos;agrandissement</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="153"/>
        <source>Preview settings</source>
        <translation>Options de prévisualisation</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="167"/>
        <source>Preview mode</source>
        <translation>Mode de prévisualisation</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="188"/>
        <source>Picture</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="202"/>
        <source>Map</source>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="213"/>
        <source>Sprite</source>
        <translation>Sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="275"/>
        <source>Open a PNG file</source>
        <translation>Ouvrir un fichier PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="376"/>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="393"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="463"/>
        <source>Vertex shader</source>
        <translation>Vertex shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="481"/>
        <source>Use a vertex shader</source>
        <translation>Utiliser un vertex shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="539"/>
        <source>New vertex shader file</source>
        <translation>Nouveau fichier vertex shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="550"/>
        <source>Save vertex shader file</source>
        <translation>Enregistrer le fichier vertex shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="566"/>
        <source>Open vertex shader file</source>
        <translation>Ouvrir un fichier vertex shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="624"/>
        <source>Fragment shader</source>
        <translation>Fragment shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="642"/>
        <source>Use a fragment shader</source>
        <translation>Utiliser un fragment shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="700"/>
        <source>New fragment shader file</source>
        <translation>Nouveau fragment shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="711"/>
        <source>Save fragment shader file</source>
        <translation>Enregistrer le fichier fragment shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="727"/>
        <source>Open fragment shader file</source>
        <translation>Ouvrir un fichier fragment shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="92"/>
        <source>Shader file</source>
        <translation>Fichier de shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="158"/>
        <source>File &apos;%1&apos; is not a shader</source>
        <translation>Le fichier &apos;%1&apos; n&apos;est pas un shader</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="163"/>
        <source>Shader %1</source>
        <translation>Shader %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="166"/>
        <source>Shader &apos;%1&apos; has been modified. Save changes?</source>
        <translation>Le shader &apos;%1&apos; a été modifé. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="426"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="647"/>
        <source>New GLSL file</source>
        <translation>Nouveau fichier GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="648"/>
        <source>File name:</source>
        <translation>Nom de fichier :</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="688"/>
        <source>Open a GLSL file</source>
        <translation>Ouvrir un fichier GLSL</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="690"/>
        <source>GLSL shader file (*.glsl)</source>
        <translation>Fichier de shader GLSL (*.glsl)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="702"/>
        <source>Shader GLSL files must be in the shaders directory</source>
        <translation>Les fichiers de shader GLSL doivent être dans le dossier shaders</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="836"/>
        <source>Open a PNG picture</source>
        <translation>Ouvrir une image PNG</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="838"/>
        <source>PNG file (*.png)</source>
        <translation>Fichier PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderModel</name>
    <message>
        <location filename="../src/shader_model.cpp" line="44"/>
        <source>Cannot open shader data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de shader &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/shader_model.cpp" line="166"/>
        <source>Cannot save shader &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le shader &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SoundChooser</name>
    <message>
        <location filename="../src/widgets/sound_chooser.cpp" line="37"/>
        <source>Play sound</source>
        <translation>Jouer le son</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteEditor</name>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="20"/>
        <source>Sprite editor</source>
        <translation>Éditeur de sprite</translation>
    </message>
    <message>
        <source>Sprite properties</source>
        <translation type="vanished">Propriétés du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="114"/>
        <source>Sprite id</source>
        <translation>Id du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="121"/>
        <source>Filename of the sprite (without extension)</source>
        <translation>Nom de fichier du sprite (sans extension)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="134"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="141"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="164"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="202"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="240"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="354"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="397"/>
        <source>Animation properties</source>
        <translation>Propriétés de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="409"/>
        <source>Source image</source>
        <translation>Image source</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="445"/>
        <source>Source image of the animation</source>
        <translation>Image source de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="461"/>
        <source>Tileset of the animation</source>
        <translation>Tileset de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="468"/>
        <source>Refresh image</source>
        <translation>Rafraîchir l&apos;image</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="471"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="488"/>
        <source>Change</source>
        <translation>Changer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="513"/>
        <source>Delay in milliseconds between two frames of the animation</source>
        <translation>Délais en millisecondes entre deux frames de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="516"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="532"/>
        <source>Frame delay</source>
        <translation>Délais entre les frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="539"/>
        <source>Loop on frame</source>
        <translation>Boucler sur la frame</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="549"/>
        <source>Index of the frame where you want the animation to come back when the last frame finishes</source>
        <translation>Index de le frame sur laquelle l&apos;animation doit revenir après la dernière frame</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="565"/>
        <source>Default animation</source>
        <translation>Animation par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="572"/>
        <source>Default animation of the sprite</source>
        <translation>Animation par défaut du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="575"/>
        <source>Set as default</source>
        <translation>Définir par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="690"/>
        <source>Direction preview</source>
        <translation>Prévisualisation de la direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="585"/>
        <source>Direction properties</source>
        <translation>Propriétés de la direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="278"/>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="316"/>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="594"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="785"/>
        <source>X coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>Coordonnée en X du coin haut-gauche de la zone contenant les frames dans l&apos;image</translation>
    </message>
    <message>
        <source>,</source>
        <translation type="vanished">,</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="786"/>
        <source>Y coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>Coordonnée en Y du coin haut-gauche de la zone contenant les frames dans l&apos;image</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="601"/>
        <source>Origin</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="608"/>
        <source>Number of frames</source>
        <translation>Nombre de frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="615"/>
        <source>Number of columns</source>
        <translation>Nombre de colonnes</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="790"/>
        <source>X coordinate of the origin point of the sprite</source>
        <translation>Coordonnée en X du point d&apos;origine du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="791"/>
        <source>Y coordinate of the origin point of the sprite</source>
        <translation>Coordonnée en Y du point d&apos;origine du sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="622"/>
        <source>Number of frames of this direction</source>
        <translation>Nombre de frames de la direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="635"/>
        <source>Number of columns of the grid containing the frames of this direction in the image</source>
        <translation>Nombre de colonnes de la grille contenant les frames de cette direction dans l&apos;image</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="648"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="780"/>
        <source>Width of each frame in the image</source>
        <translation>Largeur de chaque frame dans l&apos;image</translation>
    </message>
    <message>
        <source>x</source>
        <translation type="vanished">x</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="781"/>
        <source>Height of each frame in the image</source>
        <translation>Hauteur de chaque frame dans l&apos;image</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="70"/>
        <location filename="../src/widgets/sprite_editor.cpp" line="801"/>
        <source>Create animation</source>
        <translation>Créer animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="107"/>
        <source>Duplicate animation</source>
        <translation>Dupliquer animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="119"/>
        <source> (copy)</source>
        <translation> (copie)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="141"/>
        <source>Change animation name</source>
        <translation>Renommer animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="176"/>
        <source>Delete animation</source>
        <translation>Supprimer animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="213"/>
        <source>Change default animation</source>
        <translation>Changer animation par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="246"/>
        <source>Change source image</source>
        <translation>Changer image source</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="281"/>
        <source>Change frame delay</source>
        <translation>Changer délais entre les frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="315"/>
        <source>Change loop on frame</source>
        <translation>Changer boucler sur la frame</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="350"/>
        <source>Add direction</source>
        <translation>Ajouter direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="387"/>
        <source>Duplicate direction</source>
        <translation>Dupliquer direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="422"/>
        <source>Move direction up</source>
        <translation>Déplacer la direction vers le haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="455"/>
        <source>Move direction down</source>
        <translation>Déplacer la direction vers le bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="488"/>
        <source>Delete direction</source>
        <translation>Supprimer direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="519"/>
        <source>Change direction size</source>
        <translation>Changer taille de direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="554"/>
        <source>Change direction position</source>
        <translation>Changer position de direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="589"/>
        <source>Change direction origin</source>
        <translation>Changer origine de direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="623"/>
        <source>Change number of frames of direction</source>
        <translation>Changer le nombre de frames de la direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="659"/>
        <source>Change number of columns of direction</source>
        <translation>Changer le nombre de colonnes de la direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="695"/>
        <source>Change number of frames/columns of direction</source>
        <translation>Changer le nombre de frames/colonnes de la direction</translation>
    </message>
    <message>
        <source>Change direction num frames</source>
        <translation type="vanished">Changer nombre de frames de direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="749"/>
        <source>File &apos;%1&apos; is not a sprite</source>
        <translation>Le fichier &apos;%1&apos; n&apos;est pas un sprite</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="754"/>
        <source>Sprite %1</source>
        <translation>Sprite %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="757"/>
        <source>Sprite &apos;%1&apos; has been modified. Save changes?</source>
        <translation>Le sprite &apos;%1&apos; a été modifé. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="802"/>
        <source>Create direction</source>
        <translation>Créer direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="992"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteModel</name>
    <message>
        <location filename="../src/sprite_model.cpp" line="51"/>
        <source>Cannot open sprite &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le sprite &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="205"/>
        <source>Cannot save sprite &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le sprite &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="326"/>
        <source> (default)</source>
        <translation> (défaut)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="349"/>
        <source>(right)</source>
        <translation>(droite)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="350"/>
        <source>(up)</source>
        <translation>(haut)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="351"/>
        <source>(left)</source>
        <translation>(gauche)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="352"/>
        <source>(down)</source>
        <translation>(bas)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="355"/>
        <source>Direction %1 %2</source>
        <translation>Direction %1 %2</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="428"/>
        <location filename="../src/sprite_model.cpp" line="487"/>
        <location filename="../src/sprite_model.cpp" line="611"/>
        <source>Animation name cannot be empty</source>
        <translation>Le nom d&apos;animation ne peut pas être vide</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="433"/>
        <location filename="../src/sprite_model.cpp" line="492"/>
        <location filename="../src/sprite_model.cpp" line="615"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>L&apos;animation &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="549"/>
        <location filename="../src/sprite_model.cpp" line="607"/>
        <location filename="../src/sprite_model.cpp" line="876"/>
        <location filename="../src/sprite_model.cpp" line="931"/>
        <source>Animation &apos;%1&apos; does not exist</source>
        <translation>L&apos;animation &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="984"/>
        <location filename="../src/sprite_model.cpp" line="1047"/>
        <source>Direction %1 does not exist in animation &apos;%2&apos;</source>
        <translation>La direction %1 n&apos;existe pas dans l&apos;animation &apos;%2&apos;</translation>
    </message>
    <message>
        <source>Animation &apos;%1&apos; don&apos;t exists</source>
        <translation type="vanished">L&apos;animation &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <source>Direction %1 don&apos;t exists in animation &apos;%2&apos;</source>
        <translation type="vanished">La direction %1 n&apos;existe pas dans l&apos;animation &apos;%2&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpritePreviewer</name>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="33"/>
        <source>Show origin</source>
        <translation>Afficher l&apos;origine</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="40"/>
        <location filename="../src/widgets/sprite_previewer.cpp" line="377"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="86"/>
        <source>Index of the current frame</source>
        <translation>Index de la frame courante</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="114"/>
        <source>Last</source>
        <translation>Dernière</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="146"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="178"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="239"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="271"/>
        <source>First</source>
        <translation>Première</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="187"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="190"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="379"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="380"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="381"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="382"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="383"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteScene</name>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="201"/>
        <source>This tileset has no sprite image.
Please select another tileset.</source>
        <translation>Ce tileset n&apos;a pas d&apos;image de sprites.
Veuillez sélectionner un autre tileset.</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="207"/>
        <source>Missing source image &apos;%1&apos;</source>
        <translation>Image source manquante : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="371"/>
        <source>No such direction index: %1</source>
        <translation>Direction inexistante : %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteTreeView</name>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="38"/>
        <source>Create animation</source>
        <translation>Créer animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="44"/>
        <source>Create direction</source>
        <translation>Créer direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="50"/>
        <source>Rename animation</source>
        <translation>Renommer l&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="51"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="58"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="65"/>
        <source>Move up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="71"/>
        <source>Move down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="77"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Duplicate...</source>
        <translation type="vanished">Dupliquer...</translation>
    </message>
    <message>
        <source>Move up...</source>
        <translation type="vanished">Déplacer vers le haut...</translation>
    </message>
    <message>
        <source>Move down...</source>
        <translation type="vanished">Déplacer vers le bas...</translation>
    </message>
    <message>
        <source>Delete...</source>
        <translation type="vanished">Supprimer...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteView</name>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="50"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="57"/>
        <source>Duplicate...</source>
        <translation>Dupliquer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="64"/>
        <source>Change the number of frames/columns</source>
        <translation>Changer le nombre de frames/colonnes</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="65"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="73"/>
        <source>Change the number of frames</source>
        <translation>Changer le nombre de frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="80"/>
        <source>Change the number of columns</source>
        <translation>Changer le nombre colonnes</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="613"/>
        <source>New multiframe direction</source>
        <translation>Nouvelle direction multi-frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="622"/>
        <location filename="../src/widgets/sprite_view.cpp" line="682"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="608"/>
        <source>New direction</source>
        <translation>Nouvelle direction</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="670"/>
        <source>Move here</source>
        <translation>Déplacer ici</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="676"/>
        <source>Duplicate here</source>
        <translation>Dupliquer ici</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsEditor</name>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="14"/>
        <source>Strings editor</source>
        <translation>Éditeur de textes</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="32"/>
        <source>Language properties</source>
        <translation>Propriétés de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="41"/>
        <source>Language id</source>
        <translation>Id de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="48"/>
        <source>Folder name of the language</source>
        <translation>Nom du dossier de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="61"/>
        <source>Language description</source>
        <translation>Description de la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="68"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="80"/>
        <source>Compare to language</source>
        <translation>Comparer à la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="93"/>
        <source>Refresh language</source>
        <translation>Recharger la langue</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="96"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="145"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="183"/>
        <source>Change key</source>
        <translation>Changer la clé</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="221"/>
        <source>Duplicate string(s)</source>
        <translation>Dupliquer le texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="259"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="67"/>
        <source>Create string</source>
        <translation>Créer texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="98"/>
        <source>Duplicate strings</source>
        <translation>Dupliquer le texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="129"/>
        <source>Change string key</source>
        <translation>Changer clé de texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="162"/>
        <source>Change string key prefix</source>
        <translation>Changer préfixe de clés de texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="200"/>
        <source>Delete string</source>
        <translation>Supprimer texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="230"/>
        <source>Delete strings</source>
        <translation>Supprimer textes</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="264"/>
        <source>Change string value</source>
        <translation>Changer valeur de texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="312"/>
        <source>Strings %1</source>
        <translation>Textes %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="315"/>
        <source>Strings &apos;%1&apos; have been modified. Save changes?</source>
        <translation>Les textes &apos;%1&apos; ont été modifiés. Enregistrer les changements ?</translation>
    </message>
    <message>
        <source>Strings &apos;%1&apos; has been modified. Save changes?</source>
        <translation type="vanished">Les textes &apos;%1&apos; ont été modifié. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="325"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;Aucune langue&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="440"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="499"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="502"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>Le texte &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="576"/>
        <source>Delete confirmation</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="577"/>
        <source>Do you really want to delete all strings prefixed by &apos;%1&apos;?</source>
        <translation>Voulez-vous vraiment supprimer tous les textes préfixés par &apos;%1&apos; ?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsModel</name>
    <message>
        <location filename="../src/strings_model.cpp" line="45"/>
        <location filename="../src/strings_model.cpp" line="791"/>
        <source>Cannot open strings data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de textes &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="83"/>
        <source>Cannot save strings data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le fichier de textes &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="236"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="237"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="240"/>
        <source>Translation (%1)</source>
        <translation>Traduction (%1)</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="242"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="392"/>
        <location filename="../src/strings_model.cpp" line="529"/>
        <location filename="../src/strings_model.cpp" line="642"/>
        <source>Invalid string id: &apos;%1&apos;</source>
        <translation>Id de texte invalide : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Invalid string Key: %1</source>
        <translation type="vanished">Clé de texte invalide: %1</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="396"/>
        <location filename="../src/strings_model.cpp" line="459"/>
        <location filename="../src/strings_model.cpp" line="525"/>
        <location filename="../src/strings_model.cpp" line="605"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>Le texte &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="521"/>
        <source>String &apos;%1&apos; does not exist</source>
        <translation>Le texte &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <source>String &apos;%1&apos; no exists</source>
        <translation type="vanished">Le texte &apos;%1&apos; n&apos;existe pas</translation>
    </message>
    <message>
        <source>Invalid string key: %1</source>
        <translation type="vanished">Clé de texte invalide: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsTreeView</name>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="37"/>
        <source>New string...</source>
        <translation>Nouveau texte...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="43"/>
        <source>Duplicate string(s)...</source>
        <translation>Dupliquer le texte</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="49"/>
        <source>Change key...</source>
        <translation>Changer la clé...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditor</name>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="59"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="101"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="128"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos; en écriture</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditorWidget</name>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="360"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="371"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="382"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="395"/>
        <source>Select all</source>
        <translation>Sélectionner tout</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilePatternsListView</name>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="39"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="46"/>
        <source>Change id...</source>
        <translation>Changer l&apos;id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="47"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetEditor</name>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="14"/>
        <source>Tileset editor</source>
        <translation>Éditeur de tileset</translation>
    </message>
    <message>
        <source>Tileset properties</source>
        <translation type="vanished">Propriétés du tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="102"/>
        <source>Tileset id</source>
        <translation>Id du tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="109"/>
        <source>Filename of the tileset (without extension)</source>
        <translation>Nom de fichier du tileset (sans extension)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="125"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="138"/>
        <source>User-friendly description to show in the editor</source>
        <translation>Description intuitive à utiliser dans l&apos;éditeur</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="161"/>
        <source>Background color applied to maps using this tileset</source>
        <translation>Couleur de fond pour les maps utilisant ce tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="232"/>
        <source>Name identifying the pattern</source>
        <translation>Nom pour identifier le motif</translation>
    </message>
    <message>
        <source>Layer to initially apply to tiles using this pattern on maps</source>
        <translation type="vanished">Couche initialement appliquée aux tiles créés avec ce motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="148"/>
        <source>Background</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="181"/>
        <source>Patterns</source>
        <translation>Motifs</translation>
    </message>
    <message>
        <source>Number of existing tile patterns in the tileset</source>
        <translation type="vanished">Nombre total de motifs dans le tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="196"/>
        <source>Selection properties</source>
        <translation>Propriétés de la sélection</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="208"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="717"/>
        <source>Pattern id</source>
        <translation>Id du motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="242"/>
        <location filename="../src/widgets/tileset_editor.ui" line="488"/>
        <location filename="../src/widgets/tileset_editor.ui" line="585"/>
        <source>Rename (F2)</source>
        <translation>Renommer (F2)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="277"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="210"/>
        <source>Ground</source>
        <translation>Terrain</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="290"/>
        <source>Terrain of the pattern</source>
        <translation>Type de terrain du motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="337"/>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="337"/>
        <source>Scrolling</source>
        <translation>Scrolling</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="350"/>
        <source>Kind of animation of the pattern</source>
        <translation>Type d&apos;animation du motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="360"/>
        <source>Frames</source>
        <translation>Frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="382"/>
        <source>Horizontal or vertical separation (only for multi-frame patterns)</source>
        <translation>Séparation horizontale ou verticale (motifs multi-images uniquement)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="391"/>
        <source>Delay</source>
        <translation>Délai</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="398"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="414"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="500"/>
        <source>Mirror loop</source>
        <translation>Boucler en miroir</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="425"/>
        <source>Contours</source>
        <translation>Contours</translation>
    </message>
    <message>
        <source>Countours</source>
        <translation type="vanished">Contours</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="456"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="887"/>
        <source>Create contour</source>
        <translation>Nouveau contour</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="563"/>
        <source>Contour properties</source>
        <translation>Propriétés du contour</translation>
    </message>
    <message>
        <source>Borders</source>
        <translation type="vanished">Bordures</translation>
    </message>
    <message>
        <source>Create border set</source>
        <translation type="vanished">Créer un border set</translation>
    </message>
    <message>
        <source>Border set properties</source>
        <translation type="vanished">Propriétés du border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="569"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="588"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="595"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="617"/>
        <source>Positioning</source>
        <translation>Positionnement</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="627"/>
        <source>Outside the selection</source>
        <translation>À l&apos;extérieur de la sélection</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="636"/>
        <source>Inside the selection</source>
        <translation>À l&apos;intérieur de la sélection</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="300"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="252"/>
        <source>Default layer</source>
        <translation>Couche par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="320"/>
        <source>Repeatable</source>
        <translation>Répétable</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="76"/>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="104"/>
        <source>Move pattern</source>
        <translation>Déplacer le motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="138"/>
        <source>Resize pattern</source>
        <translation>Redimensionner le motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="172"/>
        <source>Move patterns</source>
        <translation>Déplacer les motifs</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="294"/>
        <source>Repeat mode</source>
        <translation>Mode de répétition</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="381"/>
        <source>Animation separation</source>
        <translation>Séparation d&apos;animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="420"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="460"/>
        <source>Frame delay</source>
        <translation>Délais entre les frames</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="543"/>
        <source>Create pattern</source>
        <translation>Nouveau motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="838"/>
        <source>Delete contour</source>
        <translation>Supprimer un contour</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="915"/>
        <source>Delete contour pattern</source>
        <translation>Supprimer un motif de contour</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1352"/>
        <source>Rename tile pattern</source>
        <translation>Renommer un motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1353"/>
        <source>New pattern id:</source>
        <translation>Nouvel id du motif :</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1354"/>
        <source>Update existing maps using this pattern</source>
        <translation>Mettre à jour les maps utilisant ce motif</translation>
    </message>
    <message>
        <source>Update references in existing maps</source>
        <translation type="obsolete">Mettre à jour les références dans les maps existantes</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="526"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="643"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="966"/>
        <source>File &apos;%1&apos; is not a tileset</source>
        <translation>Le fichier &apos;%1&apos;n&apos;est pas un tileset</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="971"/>
        <source>Tileset %1</source>
        <translation>Tileset %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="974"/>
        <source>Tileset &apos;%1&apos; has been modified. Save changes?</source>
        <translation>Le tileset &apos;%1&apos; a été modifié. Enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1256"/>
        <source>Invalid description</source>
        <translation>Description invalide</translation>
    </message>
    <message>
        <source>Please close all maps before updating tile pattern references.</source>
        <translation type="vanished">Veuillez fermer toutes les maps avant de mettre à jour les références des motifs.</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="579"/>
        <source>Duplicate</source>
        <translation>Dupliquer</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="750"/>
        <source>Border set id</source>
        <translation>Id du border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="777"/>
        <source>Border set inner</source>
        <translation>Positionnement du border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="809"/>
        <source>Border set patterns</source>
        <translation>Motifs du border set</translation>
    </message>
    <message>
        <source>Delete border set</source>
        <translation type="vanished">Supprimer le border set</translation>
    </message>
    <message>
        <source>Delete border set pattern</source>
        <translation type="vanished">Supprimer un motif de border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1437"/>
        <source>Cannot open map file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de map &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1445"/>
        <source>Invalid map file: &apos;%1&apos;</source>
        <translation>Fichier de map invalide : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1470"/>
        <source>Failed to export map after changing pattern id: &apos;%1&apos;</source>
        <translation>Impossible d&apos;exporter la map après le changement d&apos;id de motif</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1478"/>
        <source>Cannot open map file &apos;%1&apos; for writing</source>
        <translation>Impossible d&apos;ouvrir le fichier de map &apos;%1&apos; en écriture</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1891"/>
        <source>Do you really want to delete pattern &apos;%1&apos;?</source>
        <translation>Voulez-vous vraiment supprimer le motif &apos;%1&apos; ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1895"/>
        <source>Do you really want to delete these %1 patterns?</source>
        <translation>Voulez-vous vraiment supprimer ces %1 motifs ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1901"/>
        <source>Delete confirmation</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1938"/>
        <source>Border set name</source>
        <translation>Nom du border set</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1939"/>
        <source>Border set name:</source>
        <translation>Nom du border set :</translation>
    </message>
    <message>
        <source>Image was modified externally</source>
        <translation type="vanished">L&apos;image a été modifée</translation>
    </message>
    <message>
        <source>The tileset image was modified.
Do you want to refresh the tileset?</source>
        <translation type="vanished">L&apos;image du tileset a été modifée.
Voulez-vous recharger l&apos;image ?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetModel</name>
    <message>
        <location filename="../src/tileset_model.cpp" line="118"/>
        <source>Cannot open tileset data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier de tileset &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="165"/>
        <source>Cannot save tileset data file &apos;%1&apos;</source>
        <translation>Impossible d&apos;enregistrer le fichier de tileset &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="184"/>
        <source>Failed to refresh tileset: %1</source>
        <translation>Impossible de recharger le tileset : %1</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="502"/>
        <location filename="../src/tileset_model.cpp" line="567"/>
        <location filename="../src/tileset_model.cpp" line="634"/>
        <source>Invalid tile pattern index: %1</source>
        <translation>Index de motif non valide : %1</translation>
    </message>
    <message>
        <source>Countour already exists: &apos;%1&apos;</source>
        <translation type="vanished">Le contour existe déjà : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1796"/>
        <source>Contour already exists: &apos;%1&apos;</source>
        <translation>Le contour existe déjà : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1800"/>
        <location filename="../src/tileset_model.cpp" line="1851"/>
        <source>Invalid contour id: &apos;%1&apos;</source>
        <translation>Id de contour invalide : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1806"/>
        <source>Failed to create contour &apos;%1&apos;</source>
        <translation>Impossible de créer le contour &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1823"/>
        <location filename="../src/tileset_model.cpp" line="1847"/>
        <location filename="../src/tileset_model.cpp" line="1878"/>
        <location filename="../src/tileset_model.cpp" line="1899"/>
        <location filename="../src/tileset_model.cpp" line="1923"/>
        <location filename="../src/tileset_model.cpp" line="1938"/>
        <location filename="../src/tileset_model.cpp" line="1966"/>
        <location filename="../src/tileset_model.cpp" line="1987"/>
        <location filename="../src/tileset_model.cpp" line="2005"/>
        <source>No such contour: &apos;%1&apos;</source>
        <translation>Contour inexistant : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1829"/>
        <source>Failed to delete contour &apos;%1&apos;</source>
        <translation>Impossible de supprimer le contour &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1855"/>
        <source>Contour id already in use: &apos;%1&apos;</source>
        <translation>Id de contour déjà utilisé : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1861"/>
        <source>Failed to rename contour &apos;%1&apos;</source>
        <translation>Impossible de renommer le contour &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="432"/>
        <location filename="../src/tileset_model.cpp" line="638"/>
        <source>Invalid tile pattern id: &apos;%1&apos;</source>
        <translation>Id de motif non valide : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="436"/>
        <location filename="../src/tileset_model.cpp" line="642"/>
        <source>Tile pattern &apos;%1&apos; already exists</source>
        <translation>Le motif &apos;%1&apos; existe déjà</translation>
    </message>
    <message>
        <source>No such tile pattern: %1</source>
        <translation type="vanished">Motif de tile introuvable : %1</translation>
    </message>
    <message>
        <source>Cannot divide the pattern in 3 frames : the size of each frame must be a multiple of 8 pixels</source>
        <translation type="vanished">Impossible de diviser le motif en 3 images : la taille de chaque image doit être un multiple de 8 pixels</translation>
    </message>
    <message>
        <source>Border set already exists: &apos;%1&apos;</source>
        <translation type="vanished">Le border set existe déjà : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Invalid border set id: &apos;%1&apos;</source>
        <translation type="vanished">Id de border set non valide : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Failed to create border set &apos;%1&apos;</source>
        <translation type="vanished">Impossible de créer le border set &apos;%1&apos;</translation>
    </message>
    <message>
        <source>No such border set: &apos;%1&apos;</source>
        <translation type="vanished">Border set introuvable : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Failed to delete border set &apos;%1&apos;</source>
        <translation type="vanished">Impossible de supprimer le border set &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Border set id already in use: &apos;%1&apos;</source>
        <translation type="vanished">Id de border set déjà utilisé : &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Failed to rename border set &apos;%1&apos;</source>
        <translation type="vanished">Impossible de renommer le border set &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetScene</name>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="177"/>
        <source>Missing tileset image &apos;%1&apos;</source>
        <translation>Image de tileset manquante : &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="454"/>
        <source>No such pattern index: %1</source>
        <translation>Index de motif inexistant : %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetView</name>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="182"/>
        <source>Change id...</source>
        <translation>Changer l&apos;id...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="183"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="190"/>
        <source>Delete...</source>
        <translation>Supprimer...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="160"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <source>Create border set...</source>
        <translation type="vanished">Créer un border set...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="157"/>
        <source>Resize</source>
        <translation>Redimensionner</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="168"/>
        <source>Create contour...</source>
        <translation>Nouveau contour...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="171"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="204"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="205"/>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="206"/>
        <source>V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="207"/>
        <source>N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="667"/>
        <source>Ground</source>
        <translation>Terrain</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="672"/>
        <source>Default layer</source>
        <translation>Couche par défaut</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="677"/>
        <source>Repeatable</source>
        <translation>Répétable</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="682"/>
        <source>Scrolling</source>
        <translation>Scrolling</translation>
    </message>
    <message>
        <source>Animation</source>
        <translation type="vanished">Animation</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="756"/>
        <source>Layer %1</source>
        <translation>Couche %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1351"/>
        <source>New pattern (more options)</source>
        <translation>Nouveau motif (plus d&apos;options)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1357"/>
        <source>New pattern (%1)</source>
        <translation>Nouveau motif (%1)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1367"/>
        <location filename="../src/widgets/tileset_view.cpp" line="1502"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1488"/>
        <source>Move here</source>
        <translation>Déplacer ici</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1494"/>
        <source>Duplicate here</source>
        <translation>Dupliquer ici</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/ground_traits.cpp" line="46"/>
        <source>Empty</source>
        <translation>空白</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="49"/>
        <source>Traversable</source>
        <translation>可穿越的</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="46"/>
        <source>Arrow</source>
        <translation>箭矢</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="49"/>
        <source>Block</source>
        <translation>方塊</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="52"/>
        <source>Bomb</source>
        <translation>炸彈</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="55"/>
        <source>Boomerang</source>
        <translation>迴力鏢</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="58"/>
        <source>Camera</source>
        <translation>相機</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="61"/>
        <source>Carried object</source>
        <translation>攜帶物件</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="64"/>
        <source>Chest</source>
        <translation>寶箱</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="67"/>
        <source>Crystal</source>
        <translation>水晶</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="70"/>
        <source>Crystal block</source>
        <translation>水晶塊</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="73"/>
        <source>Custom entity</source>
        <translation>自訂實體</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="76"/>
        <source>Destination</source>
        <translation>目的地</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="79"/>
        <source>Destructible object</source>
        <translation>可破壞物件</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="82"/>
        <source>Door</source>
        <translation>門</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="85"/>
        <source>Dynamic tile</source>
        <translation>動態圖塊</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="88"/>
        <source>Enemy</source>
        <translation>敵人</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="91"/>
        <source>Explosion</source>
        <translation>爆炸</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="94"/>
        <source>Fire</source>
        <translation>火</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="97"/>
        <source>Hero</source>
        <translation>英雄</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="100"/>
        <source>Hookshot</source>
        <translation>抓鉤</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="103"/>
        <source>Jumper</source>
        <translation>彈跳器</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="106"/>
        <source>NPC</source>
        <translatorcomment>NPC, aka non-player character. no need to translate</translatorcomment>
        <translation>NPC</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="109"/>
        <source>Pickable treasure</source>
        <translation>可拾取寶物</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="112"/>
        <source>Sensor</source>
        <translation>感測器</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="115"/>
        <source>Separator</source>
        <translation>間隔物</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="118"/>
        <source>Shop treasure</source>
        <translation>商店物品</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="121"/>
        <source>Stairs</source>
        <translation>階梯</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="124"/>
        <source>Stream</source>
        <translation>流體</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="127"/>
        <source>Switch</source>
        <translation>機關按鈕</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="130"/>
        <source>Teletransporter</source>
        <translation>傳送門</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="133"/>
        <source>Tile</source>
        <translation>圖塊</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="136"/>
        <location filename="../src/ground_traits.cpp" line="52"/>
        <source>Wall</source>
        <translation>牆</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="194"/>
        <source>Ctrl+E,Ctrl+B</source>
        <translation>Ctrl+E,Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="197"/>
        <source>Ctrl+E,Ctrl+C</source>
        <translation>Ctrl+E,Ctrl+C</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="206"/>
        <source>Ctrl+E,Ctrl+Y</source>
        <translation>Ctrl+E,Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="203"/>
        <source>Ctrl+E,Ctrl+K</source>
        <translation>Ctrl+E,Ctrl+K</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="239"/>
        <source>Ctrl+E,Ctrl+U</source>
        <translation>Ctrl+E,Ctrl+U</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="209"/>
        <source>Ctrl+E,Ctrl+I</source>
        <translation>Ctrl+E,Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="212"/>
        <source>Ctrl+E,Ctrl+D</source>
        <translation>Ctrl+E,Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="215"/>
        <source>Ctrl+E,Ctrl+O</source>
        <translation>Ctrl+E,Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="221"/>
        <source>Ctrl+E,Ctrl+E</source>
        <translation>Ctrl+E,Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="224"/>
        <source>Ctrl+E,Ctrl+J</source>
        <translation>Ctrl+E,Ctrl+J</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="227"/>
        <source>Ctrl+E,Ctrl+N</source>
        <translation>Ctrl+E,Ctrl+N</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="230"/>
        <source>Ctrl+E,Ctrl+P</source>
        <translation>Ctrl+E,Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="233"/>
        <source>Ctrl+E,Ctrl+S</source>
        <translation>Ctrl+E,Ctrl+S</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="236"/>
        <source>Ctrl+E,Ctrl+A</source>
        <translation>Ctrl+E,Ctrl+A</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="242"/>
        <source>Ctrl+E,Ctrl+R</source>
        <translation>Ctrl+E,Ctrl+R</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="245"/>
        <source>Ctrl+E,Ctrl+M</source>
        <translation>Ctrl+E,Ctrl+M</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="248"/>
        <source>Ctrl+E,Ctrl+H</source>
        <translation>Ctrl+E,Ctrl+H</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="200"/>
        <source>Ctrl+E,Ctrl+L</source>
        <translation>Ctrl+E,Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="218"/>
        <source>Ctrl+E,Ctrl+2</source>
        <translation>Ctrl+E,Ctrl+2</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="251"/>
        <source>Ctrl+E,Ctrl+T</source>
        <translation>Ctrl+E,Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="254"/>
        <source>Ctrl+E,Ctrl+1</source>
        <translation>Ctrl+E,Ctrl+1</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_traits.cpp" line="257"/>
        <source>Ctrl+E,Ctrl+W</source>
        <translation>Ctrl+E,Ctrl+W</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="55"/>
        <source>Low wall</source>
        <translation>矮牆</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="58"/>
        <source>Top right</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="61"/>
        <source>Top left</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="64"/>
        <source>Bottom left</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="67"/>
        <source>Bottom right</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="70"/>
        <source>Top right (water)</source>
        <translation>右上(水域)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="73"/>
        <source>Top left (water)</source>
        <translation>左上(水域)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="76"/>
        <source>Bottom left (water)</source>
        <translation>左下(水域)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="79"/>
        <source>Bottom right (water)</source>
        <translation>右下(水域)</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="82"/>
        <source>Deep water</source>
        <translation>深水區</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="85"/>
        <source>Shallow water</source>
        <translation>淺水區</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="88"/>
        <source>Grass</source>
        <translation>草地</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="91"/>
        <source>Hole</source>
        <translation>洞穴</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="94"/>
        <source>Ice</source>
        <translation>冰</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="97"/>
        <source>Ladder</source>
        <translation>梯子</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="100"/>
        <source>Prickles</source>
        <translation>尖刺</translation>
    </message>
    <message>
        <location filename="../src/ground_traits.cpp" line="103"/>
        <source>Lava</source>
        <translation>岩漿</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="46"/>
        <source>None</source>
        <comment>Tile pattern scrolling</comment>
        <translation>無滾動</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="49"/>
        <source>Scrolling on itself</source>
        <translation>自行滾動</translation>
    </message>
    <message>
        <location filename="../src/pattern_scrolling_traits.cpp" line="52"/>
        <source>Parallax scrolling</source>
        <translation>視差滾動</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="43"/>
        <source>Horizontal</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../src/pattern_separation_traits.cpp" line="46"/>
        <source>Vertical</source>
        <translation>垂直</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="110"/>
        <source>Source and destination are the same: &apos;%1&apos;</source>
        <translation>來源和目的地相同: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="121"/>
        <location filename="../src/widgets/import_dialog.cpp" line="375"/>
        <source>Source file cannot be read: &apos;%1&apos;</source>
        <translation>無法讀取原始檔: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="125"/>
        <source>Destination already exists: &apos;%1&apos;</source>
        <translation>目的地已存在: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="117"/>
        <source>No such file or folder: &apos;%1&apos;</source>
        <translation>無此文件或資料夾: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="134"/>
        <source>No such folder: &apos;%1&apos;</source>
        <translation>無此資料夾: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="141"/>
        <source>Cannot copy folder &apos;%1&apos; to one of its own subfolders: &apos;%2&apos;</source>
        <translation>無法將資料夾 &apos;%1&apos; 複製到自己的子資料夾: &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="145"/>
        <location filename="../src/file_tools.cpp" line="221"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>無法建立資料夾 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="190"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>刪除檔案 &apos;%1&apos;失敗</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="204"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>刪除資料夾 &apos;%1&apos;失敗</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="244"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>無法開啟檔案 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="267"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>無法開啟檔案 &apos;%1&apos; 並寫入</translation>
    </message>
    <message>
        <location filename="../src/file_tools.cpp" line="159"/>
        <source>Cannot copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>無法從 &apos;%1&apos; 到 &apos;%2&apos;複製檔案</translation>
    </message>
    <message>
        <location filename="../src/obsolete_editor_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is not supported by this version of the quest editor (%2).
Please download the latest version of the editor on www.solarus-games.org.</source>
        <translation>此版本的任務編輯器 (%2) 不支援此任務 (%1) 的格式。
請在 www.solarus-games.org 下載最新版本的編輯器。</translation>
    </message>
    <message>
        <location filename="../src/obsolete_quest_exception.cpp" line="29"/>
        <source>The format of this quest (%1) is obsolete.
Please upgrade your quest  data files to Solarus %2.</source>
        <translation>此任務 (%1) 的格式已過時。
請將您的任務資料檔升級到 Solarus %2。</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="44"/>
        <source>Immediate</source>
        <translation>立即</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="47"/>
        <source>Fade in/out</source>
        <translation>淡入/淡出</translation>
    </message>
    <message>
        <location filename="../src/transition_traits.cpp" line="50"/>
        <source>Scrolling</source>
        <translation>捲動</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="45"/>
        <source>Plain</source>
        <translation>平滑</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="48"/>
        <source>Dashed</source>
        <translation>虛線</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="51"/>
        <source>Intersections (cross)</source>
        <translation>十字交叉</translation>
    </message>
    <message>
        <location filename="../src/grid_style.cpp" line="54"/>
        <source>Intersections (point)</source>
        <translation>點交叉</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="50"/>
        <source>In both directions</source>
        <translation>兩個方向</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="53"/>
        <source>Horizontally</source>
        <translation>水平的</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="56"/>
        <source>Vertically</source>
        <translation>垂直的</translation>
    </message>
    <message>
        <location filename="../src/pattern_repeat_mode_traits.cpp" line="59"/>
        <source>Non repeatable</source>
        <translation>不重複</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="45"/>
        <source>When the world changes</source>
        <translation>當世界改變時</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="48"/>
        <source>Always</source>
        <translation>總是</translation>
    </message>
    <message>
        <location filename="../src/starting_location_mode_traits.cpp" line="51"/>
        <source>Never</source>
        <translation>從不</translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="45"/>
        <source>Could not create data directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="56"/>
        <source>Untitled Quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="59"/>
        <source>Could not create quest properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/new_quest_builder.cpp" line="101"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>找不到資料目錄。
請確保 Solarus Quest Editor 已正確安裝。</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="61"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="64"/>
        <source>Right side</source>
        <translation>右側</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="67"/>
        <source>Top side</source>
        <translation>頂部</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="70"/>
        <source>Left side</source>
        <translation>左側</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="73"/>
        <source>Bottom side</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="76"/>
        <source>Top-right corner (convex)</source>
        <translation>右上角(突出)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="79"/>
        <source>Top-left corner (convex)</source>
        <translation>左上角(突出)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="82"/>
        <source>Bottom-left corner (convex)</source>
        <translation>左下角(突出)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="85"/>
        <source>Bottom-right corner (convex)</source>
        <translation>右下角(突出)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="88"/>
        <source>Top-right corner (concave)</source>
        <translation>右上角(凹陷)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="91"/>
        <source>Top-left corner (concave)</source>
        <translation>左上角(凹陷)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="94"/>
        <source>Bottom-left corner (concave)</source>
        <translation>左下角(凹陷)</translation>
    </message>
    <message>
        <location filename="../src/border_kind_traits.cpp" line="97"/>
        <source>Bottom-right corner (concave)</source>
        <translation>右下角(凹陷)</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="363"/>
        <source>Source file does not exist: &apos;%1&apos;</source>
        <translation>來源檔案不存在: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="367"/>
        <source>Source path is a folder: &apos;%1&apos;</source>
        <translation>來源路徑是資料夾: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="456"/>
        <source>Source folder does not exist: &apos;%1&apos;</source>
        <translation>來源資料夾不存在: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="460"/>
        <source>Source path is not a folder: &apos;%1&apos;</source>
        <translation>來源路徑不是資料夾: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="464"/>
        <source>Source folder cannot be read: &apos;%1&apos;</source>
        <translation>來源資料夾無法被讀取: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="371"/>
        <source>Source file is a symbolic link: &apos;%1&apos;</source>
        <translation>來源檔案為符號連結: &apos;%1</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="46"/>
        <source>Side by side</source>
        <translation>Side by side</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="49"/>
        <source>Input texture</source>
        <translation>Input texture</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="52"/>
        <source>Output texture</source>
        <translation>Output texture</translation>
    </message>
    <message>
        <location filename="../src/shader_preview_mode_traits.cpp" line="55"/>
        <source>Swipe</source>
        <translation>Swipe</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="35"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="48"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/gui_tools.cpp" line="61"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::AboutDialog</name>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="176"/>
        <source>Integrated development environment for Solarus, a free and open-source ARPG 2D game engine.</source>
        <translation>Solarus 的整合開發環境，一款免費開源的 ARPG 2D 遊戲引擎。</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Website&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.solarus-games.org&quot;&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;官方網站&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.ui" line="240"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;This program licensed is under the &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;This program licensed is under the &lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;&lt;span style=&quot; font-size:9pt; text-decoration: underline; &quot;&gt;GNU Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/about_dialog.cpp" line="37"/>
        <source>About %0</source>
        <translation>關於 %0</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeBorderSetIdDialog</name>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="14"/>
        <source>Border set id</source>
        <translation>設定邊界id</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.ui" line="20"/>
        <source>New contour id:</source>
        <translation>新輪廓id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_border_set_id_dialog.cpp" line="35"/>
        <source>New id for contour &apos;%1&apos;:</source>
        <translation>輪廓 &apos;%1&apos; 的新id:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeDialogIdDialog</name>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="14"/>
        <source>Change dialog id</source>
        <translation>更改對話框 id</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="20"/>
        <source>New dialog id:</source>
        <translation>新對話框 id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.ui" line="30"/>
        <source>Change the id of all dialogs with this id as prefix</source>
        <translation>更改所有以此 id 為前綴的對話框 id</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="41"/>
        <source>New id for dialogs prefixed by &apos;%1&apos;:</source>
        <translation>以&apos;%1&apos;為前綴的對話框的新 id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="44"/>
        <source>New id for dialog &apos;%1&apos;:</source>
        <translation>對話框&apos;%1&apos;的新 id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="101"/>
        <source>Invalid dialog id: %1</source>
        <translation>無效對話框 id: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_dialog_id_dialog.cpp" line="115"/>
        <source>The dialog &apos;%1&apos; already exists</source>
        <translation>對話框 &apos;%1&apos; 已存在</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeFileInfoDialog</name>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="14"/>
        <source>File information</source>
        <translation>文件資訊</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="20"/>
        <source>Set file information</source>
        <translation>設定文件資訊</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="29"/>
        <source>Author:</source>
        <translation>作者:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_file_info_dialog.ui" line="42"/>
        <source>License:</source>
        <translation>授權許可證:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeResourceIdDialog</name>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="14"/>
        <source>Rename resource</source>
        <translation>重新命名資源</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="20"/>
        <source>New id for resource element</source>
        <translation>資源元素的新 ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.ui" line="30"/>
        <source>Update references</source>
        <translation>更新參考</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="46"/>
        <source>New id for %1 &apos;%2&apos;:</source>
        <translation>%1 &apos;%2&apos; 的新 ID:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="52"/>
        <source>Update existing teletransporters leading to this map</source>
        <translation>更新現有通往此地圖的傳送門</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="56"/>
        <source>Update existing maps using this tileset</source>
        <translation>使用此圖塊集更新現有地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="60"/>
        <source>Update existing maps using this music</source>
        <translation>使用此音效更新現有地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="64"/>
        <source>Update existing enemies having this breed</source>
        <translation>更新擁有該效果的現有敵人</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="68"/>
        <source>Update existing custom entities having this model</source>
        <translation>更新具有此模型的現有自訂實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="126"/>
        <source>Empty resource element id</source>
        <translation>資源元素 id 為空</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_resource_id_dialog.cpp" line="131"/>
        <source>Invalid resource element id</source>
        <translation>資源元素 id 無效</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeSourceImageDialog</name>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="14"/>
        <source>Change source image</source>
        <translation>變更來源圖像</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="28"/>
        <source>Image</source>
        <translation>圖像</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.ui" line="35"/>
        <source>Tileset</source>
        <translation>圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_source_image_dialog.cpp" line="125"/>
        <source>No image selected.</source>
        <translation>未選擇影像。</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ChangeStringKeyDialog</name>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="14"/>
        <source>Change string key</source>
        <translation>更改字串鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="20"/>
        <source>New string key:</source>
        <translation>新字串鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.ui" line="30"/>
        <source>Change the key of all strings with this key as prefix</source>
        <translation>更改以該鍵為前綴所有字串的鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="41"/>
        <source>New key for strings prefixed by &apos;%1&apos;:</source>
        <translation>以&apos;%1&apos;為前綴字串的新鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="44"/>
        <source>New key for string &apos;%1&apos;:</source>
        <translation>字串&apos;%1&apos;的新鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="101"/>
        <source>Invalid string key: %1</source>
        <translation>無效的字串鍵:%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="110"/>
        <location filename="../src/widgets/change_string_key_dialog.cpp" line="115"/>
        <source>The string &apos;%1&apos; already exists</source>
        <translation>字串&apos;%1&apos;已存在</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ColorChooser</name>
    <message>
        <location filename="../src/widgets/color_chooser.cpp" line="89"/>
        <source>Select color</source>
        <translation>選擇顏色</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Console</name>
    <message>
        <location filename="../src/widgets/console.ui" line="42"/>
        <source>Run Lua code to the quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="259"/>
        <source>The quest process failed to start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="262"/>
        <source>The quest process crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="265"/>
        <source>The quest process timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/console.cpp" line="273"/>
        <source>An unknown error occurred to the quest process.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogPropertiesTable</name>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="35"/>
        <source>New property...</source>
        <translation>新屬性...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="41"/>
        <source>Change key...</source>
        <translation>改變按鍵...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="42"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="49"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialog_properties_table.cpp" line="57"/>
        <source>Set from translation...</source>
        <translation>設定翻譯...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsEditor</name>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="14"/>
        <source>Dialogs editor</source>
        <translation>對話框編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="35"/>
        <source>Language properties</source>
        <translation>語言屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="44"/>
        <source>Language id</source>
        <translation>語言id</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="51"/>
        <source>Folder name of the language</source>
        <translation>語言的資料夾名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="64"/>
        <source>Language description</source>
        <translation>語言的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="71"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="99"/>
        <source>Compare to language</source>
        <translation>比較語言</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="112"/>
        <source>Refresh language</source>
        <translation>重新整理語言</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="115"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="164"/>
        <source>Add dialog</source>
        <translation>新增對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="202"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="134"/>
        <source>Change dialog id</source>
        <translation>修改對話 id</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="240"/>
        <source>Duplicate dialog(s)</source>
        <translation>重複的對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="278"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="205"/>
        <source>Delete dialog</source>
        <translation>刪除對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="316"/>
        <source>Dialog properties</source>
        <translation>對話屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="324"/>
        <source>Dialog id:</source>
        <translation>對話 id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="371"/>
        <source>Text:</source>
        <translation>文本:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="391"/>
        <source>1,1</source>
        <translation>1,1</translation>
    </message>
    <message>
        <source>Display right margin at column:</source>
        <translation type="vanished">在列顯示右邊距:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="416"/>
        <source>Translation:</source>
        <translation>翻譯:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="445"/>
        <source>Properties :</source>
        <translation>屬性:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="475"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="480"/>
        <source>Translation</source>
        <translation>翻譯</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="496"/>
        <source>Add property</source>
        <translation>新增屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="534"/>
        <source>Change property key</source>
        <translation>修改屬性鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.ui" line="572"/>
        <source>Delete property</source>
        <translation>刪除屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="70"/>
        <source>Create dialog</source>
        <translation>新增對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="103"/>
        <source>Duplicate dialogs</source>
        <translation>複製對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="167"/>
        <source>Change dialog id prefix</source>
        <translation>更改對話 ID 前綴</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="237"/>
        <source>Delete dialogs</source>
        <translation>刪除對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="271"/>
        <source>Change dialog text</source>
        <translation>更改對話文本</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="306"/>
        <source>Create dialog property</source>
        <translation>建立對話屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="340"/>
        <source>Delete dialog property</source>
        <translation>刪除對話框屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="375"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="973"/>
        <source>Change dialog property key</source>
        <translation>更改對話屬性鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="414"/>
        <source>Change dialog property</source>
        <translation>更改對話屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="464"/>
        <source>Dialogs %1</source>
        <translation>對話 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="467"/>
        <source>Dialogs &apos;%1&apos; have been modified. Save changes?</source>
        <translation>對話 %1 被修改了，是否繼續?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="476"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;沒有語言&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="628"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog</source>
        <translation>新對話</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="673"/>
        <source>New dialog id:</source>
        <translation>新對話 id:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="681"/>
        <source>Invalid dialog id: %1</source>
        <translation>無效對話 id: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="686"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="703"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>對話 &apos;%1&apos; 已存在</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="699"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="777"/>
        <source>Delete confirmation</source>
        <translation>刪除確認</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="778"/>
        <source>Do you really want to delete all dialogs prefixed by &apos;%1&apos;?</source>
        <translation>您確實要刪除所有以&apos;%1&apos;為前綴的對話框嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="910"/>
        <source>New dialog property</source>
        <translation>新對話屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="911"/>
        <source>New property key:</source>
        <translation>新屬性鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="930"/>
        <source>The property &apos;%1&apos; already exists in the dialog &apos;%2&apos;</source>
        <translation>對話&apos;%2&apos;中已存在屬性&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="974"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>更改屬性&apos;%1&apos;的鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="1099"/>
        <source>Invalid property key: it should be a valid Lua identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_editor.cpp" line="919"/>
        <location filename="../src/widgets/dialogs_editor.cpp" line="982"/>
        <source>The property key cannot be empty</source>
        <translation>屬性鍵不能為空</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsModel</name>
    <message>
        <location filename="../src/dialogs_model.cpp" line="46"/>
        <location filename="../src/dialogs_model.cpp" line="878"/>
        <source>Cannot open dialogs data file &apos;%1&apos;</source>
        <translation>無法開啟對話資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="84"/>
        <source>Cannot save dialogs data file &apos;%1&apos;</source>
        <translation>無法儲存對話資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="394"/>
        <location filename="../src/dialogs_model.cpp" line="708"/>
        <source>Invalid dialog id: %1</source>
        <translation>無效對話框 id: %1</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="398"/>
        <location filename="../src/dialogs_model.cpp" line="496"/>
        <location filename="../src/dialogs_model.cpp" line="589"/>
        <location filename="../src/dialogs_model.cpp" line="671"/>
        <source>Dialog &apos;%1&apos; already exists</source>
        <translation>對話 &apos;%1&apos; 已存在</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="585"/>
        <source>Dialog &apos;%1&apos; does not exist</source>
        <translation>對話 &apos;%1&apos; 不存在</translation>
    </message>
    <message>
        <location filename="../src/dialogs_model.cpp" line="593"/>
        <source>Invalid dialog id: &apos;%1&apos;</source>
        <translation>無效對話框 id: %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DialogsTreeView</name>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="37"/>
        <source>New dialog...</source>
        <translation>新對話...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="43"/>
        <source>Duplicate dialog(s)...</source>
        <translation>重複的對話...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="49"/>
        <source>Change id...</source>
        <translation>更改ID...</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/dialogs_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::DrawingRectangleState</name>
    <message>
        <source>New pattern (more options)</source>
        <translation type="obsolete">新圖案(更多選項)</translation>
    </message>
    <message>
        <source>New pattern (%1)</source>
        <translation type="obsolete">新圖案 (%1)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::EditEntityDialog</name>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="20"/>
        <source>Edit an entity</source>
        <translation>編輯實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="28"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="48"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="72"/>
        <source>Layer</source>
        <translation>層級</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="126"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="136"/>
        <source>Size</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="156"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="212"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="219"/>
        <source>Sprite</source>
        <translation>精靈圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="234"/>
        <source>Repeat sprite with tiling</source>
        <translation>重複平鋪拼合圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="243"/>
        <source>Subtype</source>
        <translation>子類型</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="263"/>
        <source>Custom entity script</source>
        <translation>自訂實體腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="273"/>
        <source>Save the state</source>
        <translation>保存狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="308"/>
        <source>Treasure</source>
        <translation>寶物</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="335"/>
        <source>Variant of this item</source>
        <translation>物品數量</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="373"/>
        <source>Save the treasure state</source>
        <translation>保存物品狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="397"/>
        <source>Price font</source>
        <translation>價格標籤</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="447"/>
        <source>Can be lifted</source>
        <translation>可以舉起</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="508"/>
        <source>Can hurt enemies</source>
        <translation>可以傷害敵人</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="530"/>
        <source>Damage</source>
        <translation>傷害</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="601"/>
        <source>By script</source>
        <translation>透過腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="608"/>
        <source>By hero</source>
        <translation>透過英雄</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="617"/>
        <source>By hero, savegame variable required</source>
        <translation>透過英雄，需要保存遊戲變數</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="647"/>
        <source>Reset/decrement when opening</source>
        <translation>開啟時重設/遞減</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="658"/>
        <source>By hero, item required</source>
        <translation>透過英雄，需要物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="695"/>
        <source>Remove/decrement when opening</source>
        <translation>打開時刪除/減少</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="704"/>
        <source>By explosion</source>
        <translation>透過爆炸</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="781"/>
        <source>Update starting location</source>
        <translation>更新起始位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="788"/>
        <source>Only possible if the destination has a name</source>
        <translation>僅當目標有名稱時才可能</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="795"/>
        <source>Pattern</source>
        <translation>圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="802"/>
        <source>Click to choose another pattern</source>
        <translation>點擊以選擇其他圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="809"/>
        <source>Tileset</source>
        <translation>圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="862"/>
        <source>User properties</source>
        <translation>使用者屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="884"/>
        <source>Key</source>
        <translation>鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="889"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="905"/>
        <source>Add property</source>
        <translation>新增屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="934"/>
        <source>Change property key</source>
        <translation>修改屬性鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="963"/>
        <source>Delete property</source>
        <translation>刪除屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="992"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1021"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="831"/>
        <source>Tileset of the map</source>
        <translation>地圖的圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="840"/>
        <source>Other:</source>
        <translation>其他:</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="579"/>
        <source>Opening mode</source>
        <translation>開啟方式</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="714"/>
        <source>Action</source>
        <translation>動作</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="738"/>
        <source>Show a dialog</source>
        <translation>顯示對話框</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="750"/>
        <source>Call the map script</source>
        <translation>呼叫地圖腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="759"/>
        <source>Call an item script</source>
        <translation>呼叫專案腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="295"/>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="382"/>
        <source>in variable</source>
        <translation>在變數中</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="60"/>
        <source>Update existing teletransporters</source>
        <translation>更新現有的傳送門</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="146"/>
        <source>Origin</source>
        <translation>來源</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="407"/>
        <source>Play a sound</source>
        <translation>播放聲音</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="417"/>
        <source>Transition</source>
        <translation>過場</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="427"/>
        <source>Destination map</source>
        <translation>目標地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="437"/>
        <source>Destination</source>
        <translation>目的地</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="469"/>
        <source>Weight</source>
        <translation>重量</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="569"/>
        <source>Set a special ground</source>
        <translation>設定特殊的地面</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="253"/>
        <source>Breed</source>
        <translation>種類</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1060"/>
        <source>Maximum moves</source>
        <translation>最大移動次數</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1103"/>
        <source>Initial state</source>
        <translation>初始狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.ui" line="1096"/>
        <source>Enabled at start</source>
        <translation>開始時啟用</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="522"/>
        <source>Set as the default destination</source>
        <translation>設定為預設目的地</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Cutting the object</source>
        <translation>切割物體</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="523"/>
        <source>Can be cut</source>
        <translation>可切割</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Exploding</source>
        <translation>爆炸</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="524"/>
        <source>Can explode</source>
        <translation>會爆炸</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Regeneration</source>
        <translation>再生</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="525"/>
        <source>Can regenerate</source>
        <translation>可以再生</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Interactions</source>
        <translation>互動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="526"/>
        <source>Can be pushed</source>
        <translation>可以被推</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="527"/>
        <source>Can be pulled</source>
        <translation>可被拉動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Activation</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="528"/>
        <source>Requires a block to be activated</source>
        <translation>需要一個方塊以啟動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="198"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="238"/>
        <source>The property &apos;%1&apos; already exists</source>
        <translation>屬性&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="223"/>
        <source>Change user property key</source>
        <translation>更改使用者屬性鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="224"/>
        <source>Change the key of the property &apos;%1&apos;:</source>
        <translation>更改屬性&apos;%1&apos;的鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="232"/>
        <source>The property key cannot be empty</source>
        <translation>屬性鍵不能為空</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="244"/>
        <source>The key &apos;%1&apos; is invalid</source>
        <translation>鍵&apos;%1&apos;無效</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Leaving the switch</source>
        <translation>離開開關</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="529"/>
        <source>Deactivate when leaving</source>
        <translation>離開時停用</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Hero</source>
        <translation>英雄</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="530"/>
        <source>Obstacle for the hero</source>
        <translation>英雄的障礙</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Enemies</source>
        <translation>敵人</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="531"/>
        <source>Obstacle for enemies</source>
        <translation>敵人的障礙</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>NPCs</source>
        <translation>NPC</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="532"/>
        <source>Obstacle for NPCs</source>
        <translation>NPC的障礙</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Blocks</source>
        <translation>方塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="533"/>
        <source>Obstacle for blocks</source>
        <translation>方塊的障礙</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Projectiles</source>
        <translation>發射物</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="534"/>
        <source>Obstacle for projectiles</source>
        <translation>發射物的障礙</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Movements</source>
        <translation>移動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="535"/>
        <source>Allow to move</source>
        <translation>可以移動</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Sword</source>
        <translation>劍</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="536"/>
        <source>Allow to use the sword</source>
        <translation>允許使用劍</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Items</source>
        <translation>物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="537"/>
        <source>Allow to use equipment items</source>
        <translation>允許使用裝備物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="582"/>
        <source>Price</source>
        <translation>價格</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="583"/>
        <source>Jump length</source>
        <translation>跳躍長度</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="584"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="633"/>
        <source>Show a dialog if fails to open</source>
        <translation>如果無法開啟則顯示對話框</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="634"/>
        <source>Description dialog id</source>
        <translation>描述對話框 ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="852"/>
        <source>(Default destination)</source>
        <translation>(預設目的地)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="853"/>
        <source>(Same point)</source>
        <translation>(同一點)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="854"/>
        <source>(Side of the map)</source>
        <translation>(地圖的一側)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="918"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="926"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="919"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="928"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="920"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="930"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="921"/>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="932"/>
        <source>Down</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="927"/>
        <source>Right-up</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="929"/>
        <source>Left-up</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="931"/>
        <source>Left-down</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="933"/>
        <source>Right-down</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1002"/>
        <source>(Default)</source>
        <translation>(預設)</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1418"/>
        <source>Save the enemy state</source>
        <translation>保存敵人狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1419"/>
        <source>Save the door state</source>
        <translation>保存門狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1510"/>
        <source>Play a sound when destroyed</source>
        <translation>被破壞時播放聲音</translation>
    </message>
    <message>
        <location filename="../src/widgets/edit_entity_dialog.cpp" line="1787"/>
        <source>(None)</source>
        <translation>(無)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Editor</name>
    <message>
        <location filename="../src/widgets/editor.cpp" line="172"/>
        <source>File &apos;%1&apos; has been modified. Save changes?</source>
        <translation>文件&apos;%1&apos;已被修改。 儲存變更?</translation>
    </message>
    <message>
        <location filename="../src/widgets/editor.cpp" line="466"/>
        <source>Save changes</source>
        <translation>儲存變更</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ExternalScriptDialog</name>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="14"/>
        <source>Running script</source>
        <translation>運行腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="24"/>
        <source>Running script...</source>
        <translation>運行腳本...</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="40"/>
        <location filename="../src/widgets/external_script_dialog.cpp" line="169"/>
        <source>In progress</source>
        <translation>進行中</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.ui" line="62"/>
        <source>Status</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="56"/>
        <source>Cannot determine the directory of script &apos;%1&apos;</source>
        <translation>無法確定腳本&apos;%1&apos;的目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="201"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>無法開啟檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="244"/>
        <source>Successful!</source>
        <translation>成功!</translation>
    </message>
    <message>
        <location filename="../src/widgets/external_script_dialog.cpp" line="248"/>
        <source>Failure</source>
        <translation>失敗</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::FindTextDialog</name>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="14"/>
        <source>Find text</source>
        <translation>尋找文字</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="25"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="32"/>
        <source>Find</source>
        <translation>尋找</translation>
    </message>
    <message>
        <location filename="../src/widgets/find_text_dialog.ui" line="42"/>
        <location filename="../src/widgets/find_text_dialog.cpp" line="37"/>
        <source>Replace</source>
        <translation>取代</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::GetAnimationNameDialog</name>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="34"/>
        <source>New animation</source>
        <translation>新動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="50"/>
        <source>Change animation name</source>
        <translation>更改動畫名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="84"/>
        <source>Empty animation name</source>
        <translation>動畫名稱為空</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="91"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>動畫&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/widgets/get_animation_name_dialog.cpp" line="105"/>
        <source>Animation name:</source>
        <translation>動畫名稱:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImageView</name>
    <message>
        <location filename="../src/widgets/image_view.cpp" line="41"/>
        <source>Failed to load image &apos;%1&apos;</source>
        <translation>無法載入圖片&apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ImportDialog</name>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="14"/>
        <source>Import files from another quest</source>
        <translation>從另一個任務匯入文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="36"/>
        <source>Source quest</source>
        <translation>來源任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="52"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="91"/>
        <source>Identify missing</source>
        <translation>遺失識別</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="102"/>
        <source>No missing files found</source>
        <translation>未發現遺失文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.ui" line="68"/>
        <source>Destination quest</source>
        <translation>目標任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="57"/>
        <source>Import files</source>
        <translation>導入文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="120"/>
        <source>Select a quest where to import from</source>
        <translation>選擇從哪裡匯入的任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="136"/>
        <source>Source and destination quest are the same</source>
        <translation>來源和目的地任務相同</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="209"/>
        <source>No candidates found</source>
        <translation>沒有發現候選</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="210"/>
        <source>%1 candidates found</source>
        <translation>發現候選%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="272"/>
        <source>Import 1 item</source>
        <translation>導入 1 件物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="275"/>
        <source>Import %1 items</source>
        <translation>導入 %1 件物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="309"/>
        <source>Import confirmation</source>
        <translation>導入確認</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="310"/>
        <source>%1 items will be imported to your quest.</source>
        <translation>%1 個物品將會導入您的任務。</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="344"/>
        <source>Cannot import symbolic link &apos;%1&apos;</source>
        <translation>無法導入符號連結&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="382"/>
        <source>Destination path already exists and is a folder: &apos;%1&apos;</source>
        <translation>目標路徑已存在併且是資料夾:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="483"/>
        <source>The destination directory &apos;%1&apos; already exists.
Do you want to overwrite its content?</source>
        <translation>目標目錄&apos;%1&apos;已存在。
您想覆蓋其內容嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="484"/>
        <source>Apply this choice for remaining directories</source>
        <translation>將此選擇套用至其餘目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="601"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="604"/>
        <source>Overwrite</source>
        <translation>覆蓋</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="606"/>
        <source>Skip</source>
        <translation>跳過</translation>
    </message>
    <message>
        <source>The destination folder &apos;%1&apos; already exists.
Do you want to merge it with the contents from the source folder?</source>
        <translation type="vanished">Le dossier destination &apos;%1&apos; existe déjà.
Voulez-vous le fusionner avec le contenu du dossier source ?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="143"/>
        <source>No source quest was not found in folder &apos;%1&apos;</source>
        <translation>在資料夾&apos;%1&apos;中未找到來源任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="391"/>
        <location filename="../src/widgets/import_dialog.cpp" line="482"/>
        <source>Destination already exists</source>
        <translation>目的地已存在</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="392"/>
        <source>The destination file &apos;%1&apos; already exists.
Do you want to overwrite it?</source>
        <translation>目標檔案&apos;%1&apos;已存在。
您想覆蓋它嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="393"/>
        <source>Apply this choice for remaining files</source>
        <translation>將此選擇用於剩餘文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="420"/>
        <source>Failed to remove existing file &apos;%1&apos;</source>
        <translation>無法刪除現有檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="436"/>
        <source>Failed to copy file &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>無法將檔案&apos;%1&apos;複製到&apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/import_dialog.cpp" line="471"/>
        <source>Destination path already exists and is not a directory: &apos;%1&apos;</source>
        <translation>目標路徑已存在且不是目錄:&apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::InputDialogWithCheckBox</name>
    <message>
        <location filename="../src/widgets/input_dialog_with_check_box.ui" line="21"/>
        <source>New value:</source>
        <translation>新值:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MainWindow</name>
    <message>
        <location filename="../src/widgets/main_window.ui" line="20"/>
        <source>Solarus Quest Editor</source>
        <translation>Solarus 任務編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="77"/>
        <source>File</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="96"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="110"/>
        <source>Run</source>
        <translation>運行</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="117"/>
        <source>View</source>
        <translation>檢視</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="130"/>
        <source>Help</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="139"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="146"/>
        <source>Audio</source>
        <translation>音效</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="188"/>
        <source>New quest...</source>
        <translation>新任務...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="193"/>
        <source>Load quest...</source>
        <translation>開啟任務...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="196"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="201"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="210"/>
        <location filename="../src/widgets/main_window.cpp" line="1736"/>
        <source>Run quest</source>
        <translation>運行任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="213"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="284"/>
        <location filename="../src/widgets/main_window.ui" line="287"/>
        <source>Show layer 0</source>
        <translation>顯示第 0 層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="290"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="305"/>
        <location filename="../src/widgets/main_window.ui" line="308"/>
        <source>Show layer 1</source>
        <translation>顯示第 1 層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="311"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="326"/>
        <location filename="../src/widgets/main_window.ui" line="329"/>
        <source>Show layer 2</source>
        <translation>顯示第 2 層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="332"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="358"/>
        <source>Find / Replace</source>
        <translation>尋找 / 取代</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="372"/>
        <source>Select all</source>
        <translation>全選</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="377"/>
        <source>Save all</source>
        <translation>全部儲存</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="380"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="385"/>
        <source>Close all</source>
        <translation>全部關閉</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="388"/>
        <source>Ctrl+Shift+W</source>
        <translation>Ctrl+Shift+W</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="396"/>
        <source>Show console</source>
        <translation>顯示控制台</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="399"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="407"/>
        <source>Unselect all</source>
        <translation>全部取消選擇</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="412"/>
        <source>Close quest</source>
        <translation>關閉任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="421"/>
        <location filename="../src/widgets/main_window.cpp" line="1785"/>
        <location filename="../src/widgets/main_window.cpp" line="1792"/>
        <source>Pause music</source>
        <translation>暫停音樂</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="430"/>
        <location filename="../src/widgets/main_window.cpp" line="1781"/>
        <source>Stop music</source>
        <translation>停止音樂</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="442"/>
        <source>Show traversable entities</source>
        <translation>顯示可遍歷實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="454"/>
        <source>Show obstacle entities</source>
        <translation>顯示障礙物實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="459"/>
        <source>Quest properties</source>
        <translation>任務屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="462"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="467"/>
        <source>Import from a quest...</source>
        <translation>從任務導入...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="470"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="475"/>
        <source>Build quest package...</source>
        <translation>建置Solarus任務...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="484"/>
        <source>Export to image...</source>
        <translation>匯出到圖像...</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="487"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="492"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="501"/>
        <location filename="../src/widgets/main_window.ui" line="504"/>
        <source>Run current map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="507"/>
        <source>Shift+F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="512"/>
        <source>Re-open closed tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="515"/>
        <source>Ctrl+Shift+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="363"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="222"/>
        <source>Cut</source>
        <translation>剪下</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="231"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="240"/>
        <source>Paste</source>
        <translation>貼上</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="245"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="254"/>
        <source>Save</source>
        <translation>儲存</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="266"/>
        <source>Show grid</source>
        <translation>顯示網格</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="269"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="341"/>
        <source>Documentation</source>
        <translation>官方文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="344"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.ui" line="353"/>
        <source>Website</source>
        <translation>官方網站</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="845"/>
        <source>Select quest directory</source>
        <translation>選擇任務目錄</translation>
    </message>
    <message>
        <source>Quest successfully created!
The next step is to manually edit your quest properties in quest.dat
(sorry, this is not fully supported by the editor yet).
</source>
        <translation type="vanished">Quête créée avec succès !
La prochaine étape est de modifier manuellement les propriétés de votre quête dans quest.dat
(désolé, l&apos;éditeur ne sait pas encore le faire interactivement).</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="615"/>
        <source>No quest was found in directory
&apos;%1&apos;</source>
        <translation>目錄中找不到任務
&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="130"/>
        <location filename="../src/widgets/main_window.cpp" line="331"/>
        <source>Zoom</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="333"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="334"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="335"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="336"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="337"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="104"/>
        <source>Recent quests</source>
        <translation>最近的任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="150"/>
        <source>Show/hide more layers</source>
        <translation>顯示/隱藏更多層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="154"/>
        <source>Lock/unlock layers</source>
        <translation>鎖定/解鎖圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="162"/>
        <location filename="../src/widgets/main_window.cpp" line="481"/>
        <source>Show/hide entity types</source>
        <translation>顯示/隱藏實體類型</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="265"/>
        <source>Could not locate the assets directory.
Some features like creating a new quest will not be available.
Please make sure that Solarus Quest Editor is correctly installed.</source>
        <translation>無法找到資料目錄。
某些功能(例如建立新任務)將無法使用。
請確保 Solarus Quest Editor 已正確安裝。</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="373"/>
        <source>Show all layers</source>
        <translation>顯示所有圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="383"/>
        <source>Hide all layers</source>
        <translation>隱藏所有圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="402"/>
        <source>Show layer %1</source>
        <translation>顯示圖層 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="453"/>
        <source>Lock layer %1</source>
        <translation>鎖定圖層 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="458"/>
        <source>Ctrl+%1</source>
        <translation>Ctrl+%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="510"/>
        <source>Show all entities</source>
        <translation>顯示所有實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="520"/>
        <source>Hide all entities</source>
        <translation>隱藏所有實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="688"/>
        <source>Obsolete quest</source>
        <translation>過時的任務</translation>
    </message>
    <message>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.</source>
        <translation type="vanished">此任務 (%1) 的格式已過時。
您的資料檔案將自動更新到 Solarus %2。</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="720"/>
        <source>Upgrading quest data files</source>
        <translation>升級任務資料檔案</translation>
    </message>
    <message>
        <source>An error occured while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation type="vanished">升級任務時發生錯誤。
您的任務在格式 %1 中保持不變。</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="785"/>
        <source>Could not find the assets directory.
Make sure that Solarus Quest Editor is properly installed.</source>
        <translation>找不到資料目錄。
請確保 Solarus Quest Editor 已正確安裝。</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1082"/>
        <source>Files are modified</source>
        <translation>文件已修改</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1083"/>
        <source>Do you want to save modifications before running the quest?</source>
        <translation>您想在運行任務之前儲存修改嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1313"/>
        <source>Local Documentation Not Found</source>
        <translation>未找到本地文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1314"/>
        <source>The local copy of Solarus Documentation could not be found. Would you like to try going on line to find the documentaion?</source>
        <translation>無法找到 Solarus 文件的本機副本。 您想嘗試在線查找文件嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1733"/>
        <source>Stop quest</source>
        <translation>停止任務</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1967"/>
        <source>Update existing sprites using this image</source>
        <translation>使用此圖像更新現有精靈圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2034"/>
        <source>Unsaved changes</source>
        <translation>未儲存的更改</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="2035"/>
        <source>All files must be saved before this operation.
Do you want to save them now?</source>
        <translation>執行此操作之前必須儲存所有檔案。
你現在想儲存他們嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1900"/>
        <source>File modified</source>
        <translation>文件已修改</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="689"/>
        <source>The format of this quest (%1) is outdated.
Your data files will be automatically updated to Solarus %2.
Would you like to update this quest?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="696"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="737"/>
        <source>An error occurred while upgrading the quest.
Your quest was kept unchanged in format %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1901"/>
        <source>This file is open and has unsaved changes.
Please save it or close it before renaming.</source>
        <translation>該文件已開啟並且有未儲存的變更。
重新命名之前請儲存或關閉它。</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1965"/>
        <location filename="../src/widgets/main_window.cpp" line="1991"/>
        <source>Rename file</source>
        <translation>重新命名文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1966"/>
        <location filename="../src/widgets/main_window.cpp" line="1992"/>
        <source>New name for file &apos;%1&apos;:</source>
        <translation>檔案&apos;%1&apos;的新名稱:</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1716"/>
        <source>Missing show entity type action</source>
        <translation>缺少顯示實體類型操作</translation>
    </message>
    <message>
        <location filename="../src/widgets/main_window.cpp" line="1789"/>
        <source>Play selected music</source>
        <translation>播放選定的音樂</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapEditor</name>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="14"/>
        <source>Map editor</source>
        <translation>地圖編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="103"/>
        <source>Map id</source>
        <translation>地圖ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="118"/>
        <source>Filename of the map (without extension)</source>
        <translation>地圖的檔案名稱(不含副檔名)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="131"/>
        <source>Open map script (F4)</source>
        <translation>開啟地圖腳本(F4)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="156"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="169"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="176"/>
        <source>Size</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="386"/>
        <source>Patterns</source>
        <translation>圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="425"/>
        <location filename="../src/widgets/map_editor.ui" line="495"/>
        <source>Edit tileset (Ctrl-T)</source>
        <translation>編輯圖塊集 (Ctrl-T)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="441"/>
        <location filename="../src/widgets/map_editor.ui" line="511"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="465"/>
        <source>Contour generator</source>
        <translation>輪廓產生器</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="471"/>
        <source>Generate contour tiles around the selection</source>
        <translation>圍繞選擇區域產生輪廓圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="520"/>
        <source>Contour</source>
        <translation>輪廓</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="570"/>
        <source>Generate contour tiles around the selection (Ctrl+B)</source>
        <translation>圍繞選擇區域產生輪廓圖塊 (Ctrl+B)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="573"/>
        <source>Generate tiles</source>
        <translation>產生圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1140"/>
        <source>Width of the map in pixels</source>
        <translation>地圖的寬度(以像素為單位)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1141"/>
        <source>Height of the map in pixels</source>
        <translation>地圖的高度(以像素為單位)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="258"/>
        <source>Set a world</source>
        <translation>設定世界</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="265"/>
        <source>A name to group maps together</source>
        <translation>將地圖分組在一起的名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="272"/>
        <source>Set a floor</source>
        <translation>設置樓層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="293"/>
        <source>Floor number of the map</source>
        <translation>地圖樓層數</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="321"/>
        <source>Location in its world</source>
        <translation>在其世界中的位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1145"/>
        <location filename="../src/widgets/map_editor.cpp" line="1146"/>
        <source>Coordinates of the map in its world (useful to make adjacent scrolling maps)</source>
        <translation>地圖在其世界中的座標(用於製作相鄰的滾動地圖)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="341"/>
        <location filename="../src/widgets/map_editor.ui" line="479"/>
        <location filename="../src/widgets/map_editor.cpp" line="241"/>
        <source>Tileset</source>
        <translation>圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="350"/>
        <location filename="../src/widgets/map_editor.ui" line="418"/>
        <location filename="../src/widgets/map_editor.ui" line="488"/>
        <source>Tileset of the map</source>
        <translation>地圖的圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="134"/>
        <location filename="../src/widgets/map_editor.ui" line="357"/>
        <location filename="../src/widgets/map_editor.ui" line="428"/>
        <location filename="../src/widgets/map_editor.ui" line="498"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="186"/>
        <source>Layers</source>
        <translation>層級</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="217"/>
        <source>to</source>
        <translation>至</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.ui" line="331"/>
        <location filename="../src/widgets/map_editor.cpp" line="259"/>
        <source>Music</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="113"/>
        <source>Map size</source>
        <translation>地圖大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="131"/>
        <source>Lowest layer</source>
        <translation>最低層級</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="159"/>
        <source>Highest layer</source>
        <translation>最高層級</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="187"/>
        <source>Map world</source>
        <translation>地圖世界</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="205"/>
        <source>Map floor</source>
        <translation>地圖樓層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="223"/>
        <source>Map location</source>
        <translation>地圖位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="277"/>
        <source>Edit entity</source>
        <translation>編輯實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="362"/>
        <source>Move entities</source>
        <translation>移動實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="414"/>
        <source>Resize entities</source>
        <translation>調整實體大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="496"/>
        <location filename="../src/widgets/map_editor.cpp" line="553"/>
        <source>Convert tiles</source>
        <translation>轉換圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="615"/>
        <source>Change pattern</source>
        <translation>改變圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="669"/>
        <source>Set direction</source>
        <translation>設定方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="731"/>
        <source>Set layer</source>
        <translation>設定層級</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="775"/>
        <source>Increment layer</source>
        <translation>遞增圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="820"/>
        <source>Decrement layer</source>
        <translation>遞減圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="865"/>
        <source>Bring to front</source>
        <translation>移到前面</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="923"/>
        <source>Bring to back</source>
        <translation>移到後面</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="983"/>
        <source>Add entities</source>
        <translation>增加實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1030"/>
        <source>Delete entities</source>
        <translation>刪除實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1085"/>
        <source>File &apos;%1&apos; is not a map</source>
        <translation>文件&apos;%1&apos;不是地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1090"/>
        <source>Map %1</source>
        <translation>地圖 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1093"/>
        <source>Map &apos;%1&apos; has been modified. Save changes?</source>
        <translation>地圖&apos;%1&apos;已被修改。 儲存變更?</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1105"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1121"/>
        <source>&lt;No music&gt;</source>
        <translation>&lt;無音樂&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1122"/>
        <source>&lt;Same as before&gt;</source>
        <translation>&lt;與之前相同&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1127"/>
        <location filename="../src/widgets/map_editor.cpp" line="1131"/>
        <source>(Tileset of the map)</source>
        <translation>(地圖的圖塊集)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1294"/>
        <source>Add tile</source>
        <translation>增加圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1295"/>
        <source>Add destination</source>
        <translation>增加目的地</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1296"/>
        <source>Add teletransporter</source>
        <translation>增加傳送門</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1297"/>
        <source>Add pickable</source>
        <translation>增加可拾取物</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1298"/>
        <source>Add destructible</source>
        <translation>增加可破壞物件</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1299"/>
        <source>Add chest</source>
        <translation>增加寶箱</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1300"/>
        <source>Add jumper</source>
        <translation>增加彈跳器</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1301"/>
        <source>Add enemy</source>
        <translation>增加敵人</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1302"/>
        <source>Add non-playing character</source>
        <translation>增加NPC</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1303"/>
        <source>Add block</source>
        <translation>增加方塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1304"/>
        <source>Add switch</source>
        <translation>增加機關按鈕</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1305"/>
        <source>Add wall</source>
        <translation>增加牆壁</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1306"/>
        <source>Add sensor</source>
        <translation>增加感測器</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1307"/>
        <source>Add crystal</source>
        <translation>增加水晶</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1308"/>
        <source>Add crystal block</source>
        <translation>增加水晶塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1309"/>
        <source>Add shop treasure</source>
        <translation>增加商店物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1310"/>
        <source>Add stream</source>
        <translation>增加流體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1311"/>
        <source>Add door</source>
        <translation>增加門</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1312"/>
        <source>Add stairs</source>
        <translation>增加階梯</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1313"/>
        <source>Add separator</source>
        <translation>增加間隔物</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1314"/>
        <source>Add custom entity</source>
        <translation>增加自訂實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1367"/>
        <source>Save map as PNG file</source>
        <translation>將地圖另存為 PNG 文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1369"/>
        <source>PNG image (*.png)</source>
        <translation>PNG檔 (*.png)</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1606"/>
        <location filename="../src/widgets/map_editor.cpp" line="1677"/>
        <source>Layer not empty</source>
        <translation>圖層不為空</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1607"/>
        <location filename="../src/widgets/map_editor.cpp" line="1678"/>
        <source>This layer is not empty: %1 entities will be destroyed.</source>
        <translation>該層不為空:%1 個實體將被銷毀。</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2151"/>
        <source>%1,%2,%3 </source>
        <translation>%1,%2,%3 </translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2459"/>
        <source>Cannot generate tiles: some patterns of the contour &apos;%1&apos; are missing.
Please fix it in the tileset.</source>
        <translation>無法產生圖塊:輪廓&apos;%1&apos;的某些圖案遺失。
請在圖塊集中修復它。</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2138"/>
        <source> - %1</source>
        <translation> - %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="2140"/>
        <source>: %1</source>
        <translation>: %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_editor.cpp" line="1535"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapModel</name>
    <message>
        <location filename="../src/map_model.cpp" line="51"/>
        <source>Cannot open map data file &apos;%1&apos;</source>
        <translation>無法開啟地圖檔案 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/map_model.cpp" line="102"/>
        <source>Cannot save map data file &apos;%1&apos;</source>
        <translation>無法儲存地圖資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="79"/>
        <source>No direction</source>
        <translation>沒有方向</translation>
    </message>
    <message>
        <location filename="../src/entities/entity_model.cpp" line="297"/>
        <source>Unexpected entity type (not allowed in map files): %1</source>
        <translation>意外的實體類型(地圖檔案中不允許):%1</translation>
    </message>
    <message>
        <location filename="../src/entities/block.cpp" line="34"/>
        <location filename="../src/entities/npc.cpp" line="34"/>
        <source>Any</source>
        <translation>任何</translation>
    </message>
    <message>
        <location filename="../src/entities/destination.cpp" line="35"/>
        <source>Keep the same direction</source>
        <translation>保持同一方向</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="34"/>
        <source>Initially lowered</source>
        <translation>最初降低</translation>
    </message>
    <message>
        <location filename="../src/entities/crystal_block.cpp" line="35"/>
        <source>Initially raised</source>
        <translation>最初提升</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="37"/>
        <source>Generalized NPC (something)</source>
        <translation>廣義NPC(某物)</translation>
    </message>
    <message>
        <location filename="../src/entities/npc.cpp" line="38"/>
        <source>Usual NPC (somebody)</source>
        <translation>普通NPC(某人)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="33"/>
        <source>Spiral staircase (going upstairs)</source>
        <translation>螺旋樓梯(上樓)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="34"/>
        <source>Spiral staircase (going downstairs)</source>
        <translation>螺旋樓梯(下樓)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="35"/>
        <source>Straight staircase (going upstairs)</source>
        <translation>直線樓梯(上樓)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="36"/>
        <source>Straight staircase (going downstairs)</source>
        <translation>直線樓梯(下樓)</translation>
    </message>
    <message>
        <location filename="../src/entities/stairs.cpp" line="37"/>
        <source>Platform stairs (same map)</source>
        <translation>平台樓梯(同地圖)</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="31"/>
        <source>Walkable</source>
        <translation>可行走</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="32"/>
        <source>Arrow target</source>
        <translation>箭頭目標</translation>
    </message>
    <message>
        <location filename="../src/entities/switch.cpp" line="33"/>
        <source>Solid</source>
        <translation>固體</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MapView</name>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="542"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="550"/>
        <source>Resize</source>
        <translation>調整大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="551"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="558"/>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tile</source>
        <translation>轉換為動態圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="564"/>
        <source>Change pattern...</source>
        <translation>改變模式...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="571"/>
        <source>Change pattern of similar tiles...</source>
        <translation>改變類似圖塊的圖案...</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="577"/>
        <source>Generate borders around selection</source>
        <translation>圍繞選擇區域產生邊界</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="578"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="587"/>
        <source>One layer up</source>
        <translation>向上一層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="588"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="597"/>
        <source>One layer down</source>
        <translation>往下一層</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="598"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="607"/>
        <source>Bring to front</source>
        <translation>移到前面</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="608"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="617"/>
        <source>Bring to back</source>
        <translation>移到後面</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="618"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="627"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="634"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="663"/>
        <source>Layer %1</source>
        <translation>層級 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="715"/>
        <source>Convert to dynamic tiles</source>
        <translation>轉換為動態圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tile</source>
        <translation>轉換為固定圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="718"/>
        <source>Convert to static tiles</source>
        <translation>轉換為固定圖塊</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="814"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="833"/>
        <location filename="../src/widgets/map_view.cpp" line="841"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="834"/>
        <location filename="../src/widgets/map_view.cpp" line="843"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="835"/>
        <location filename="../src/widgets/map_view.cpp" line="845"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="836"/>
        <location filename="../src/widgets/map_view.cpp" line="847"/>
        <source>Down</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="842"/>
        <source>Right-up</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="844"/>
        <source>Left-up</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="846"/>
        <source>Left-down</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../src/widgets/map_view.cpp" line="848"/>
        <source>Right-down</source>
        <translation>右下</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MovingPatternsState</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::MusicChooser</name>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="161"/>
        <source>Play music</source>
        <translation>播放音樂</translation>
    </message>
    <message>
        <location filename="../src/widgets/music_chooser.cpp" line="182"/>
        <source>Stop music</source>
        <translation>停止音樂</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewElementDialog</name>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="14"/>
        <source>New element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="22"/>
        <source>License:</source>
        <translation type="unfinished">授權許可證:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="29"/>
        <location filename="../src/widgets/new_element_dialog.cpp" line="57"/>
        <source>Script name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.ui" line="36"/>
        <source>Author:</source>
        <translation type="unfinished">作者:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="53"/>
        <source>New GLSL file</source>
        <translation type="unfinished">新的 GLSL 檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="54"/>
        <source>GLSL file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="56"/>
        <source>New Lua script</source>
        <translation type="unfinished">新Lua腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="59"/>
        <source>New folder</source>
        <translation type="unfinished">新的資料夾</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_element_dialog.cpp" line="60"/>
        <source>Folder name:</source>
        <translation type="unfinished">資料夾名稱:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewEntityUserPropertyDialog</name>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="14"/>
        <source>New user property</source>
        <translation>新使用者屬性:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="24"/>
        <source>Key:</source>
        <translation>鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_entity_user_property_dialog.ui" line="34"/>
        <source>Value:</source>
        <translation>值:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialog</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog.ui" line="14"/>
        <source>Create New Quest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogContentsPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="17"/>
        <source>Initial Quest Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="20"/>
        <source>Choose the initial set of files for your quest.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="42"/>
        <source>Community Resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="60"/>
        <source>Empty Quest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start with default free resources and scripts.&lt;br/&gt;Uses GPL and Creative Commons licenses.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_contents_page.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start from no quest data.&lt;br/&gt;You will have to create data files from scratch.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogDirectoryPage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="17"/>
        <source>Quest Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="20"/>
        <source>Choose a location on disk for the new quest.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="31"/>
        <source>Browse</source>
        <translation type="unfinished">瀏覽</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_directory_page.ui" line="40"/>
        <source>Error Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="122"/>
        <source>Directory not empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="123"/>
        <source>The quest directory is not empty, are you sure you wish to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="148"/>
        <source>Select quest directory</source>
        <translation type="unfinished">選擇任務目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="171"/>
        <source>A quest already exists in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog.cpp" line="177"/>
        <source>Parent directory does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewQuestDialogTitlePage</name>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="14"/>
        <source>WizardPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="17"/>
        <source>Quest Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_quest_dialog_title_page.ui" line="20"/>
        <source>Enter the title of the new quest.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewResourceElementDialog</name>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="20"/>
        <source>Create resource</source>
        <translation>建立資源</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="31"/>
        <source>Resource id (filename)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="41"/>
        <source>[required]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="48"/>
        <source>Description</source>
        <translation type="unfinished">描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="55"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="90"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="104"/>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="118"/>
        <source>[optional]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="62"/>
        <source>Create with default code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="74"/>
        <source>Ownership</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="83"/>
        <source>Author</source>
        <translation type="unfinished">作者</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="97"/>
        <source>License (data file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.ui" line="111"/>
        <source>License (script file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resource id (filename):</source>
        <translation type="vanished">資源 ID(檔案名稱):</translation>
    </message>
    <message>
        <source>Licence:</source>
        <translation type="vanished">授權許可證:</translation>
    </message>
    <message>
        <source>Author:</source>
        <translation type="vanished">作者:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">描述:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="48"/>
        <source>New map</source>
        <translation>新地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="49"/>
        <source>Map id (filename):</source>
        <translation>地圖 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="56"/>
        <source>New tileset</source>
        <translation>新圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="57"/>
        <source>Tileset id (filename):</source>
        <translation>圖塊集 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="62"/>
        <source>New sprite</source>
        <translation>新精靈圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="63"/>
        <source>Sprite id (filename):</source>
        <translation>精靈圖 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="68"/>
        <source>New music</source>
        <translation>新音樂</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="69"/>
        <source>Music id (filename):</source>
        <translation>音樂 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="74"/>
        <source>New sound</source>
        <translation>新音效</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="75"/>
        <source>Sound id (filename):</source>
        <translation>音效 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="80"/>
        <source>New item</source>
        <translation>新物品</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="81"/>
        <source>Item id (filename):</source>
        <translation>物品 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="87"/>
        <source>New enemy</source>
        <translation>新敵人</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="88"/>
        <source>Enemy id (filename):</source>
        <translation>敵人 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="94"/>
        <source>New custom entity</source>
        <translation>新自訂實體</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="95"/>
        <source>Custom entity id (filename):</source>
        <translation>自訂實體 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="101"/>
        <source>New language</source>
        <translation>新語言</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="102"/>
        <source>Language id (filename):</source>
        <translation>語言 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="107"/>
        <source>New font</source>
        <translation>新字體</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="108"/>
        <source>Font id (filename):</source>
        <translation>字體 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="113"/>
        <source>New shader</source>
        <translation>新的著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="114"/>
        <source>Shader id (filename):</source>
        <translation>著色器 ID(檔案名稱):</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="129"/>
        <location filename="../src/widgets/new_resource_element_dialog.cpp" line="140"/>
        <source>License</source>
        <translation type="unfinished">授權許可證</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::NewStringDialog</name>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="14"/>
        <source>New string</source>
        <translation>新字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="24"/>
        <source>New string key:</source>
        <translation>新字串鍵:</translation>
    </message>
    <message>
        <location filename="../src/widgets/new_string_dialog.ui" line="34"/>
        <source>New string value:</source>
        <translation>新字串值:</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PackageDialog</name>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="14"/>
        <source>Build Solarus Package</source>
        <translation>建置Solarus軟體套件</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="27"/>
        <source>Save quest package to:</source>
        <translation>儲存Solarus軟體套件至:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="43"/>
        <source>Browse</source>
        <translation>瀏覽</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="69"/>
        <source>Building quest package...</source>
        <translation>建置Solarus軟體套件...</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="90"/>
        <source>Build successful!</source>
        <translation>建置成功!</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="116"/>
        <source>Build failed</source>
        <translation>建置失敗</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="138"/>
        <source>Exit Code:</source>
        <translation>退出代碼:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.ui" line="145"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="59"/>
        <source>Build</source>
        <translation>建置</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="90"/>
        <source>Starting...
</source>
        <translation>開始...
</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="103"/>
        <source>Crashed</source>
        <translation>崩潰</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="115"/>
        <source>Quest package location:</source>
        <translation>任務包位置:</translation>
    </message>
    <message>
        <location filename="../src/widgets/package_dialog.cpp" line="117"/>
        <source>Solarus Packages (*.solarus)</source>
        <translation>Solarus 軟體包 (*.solarus)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::PatternPickerDialog</name>
    <message>
        <location filename="../src/widgets/pattern_picker_dialog.ui" line="14"/>
        <source>Pick a pattern</source>
        <translation>挑選圖案</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::Quest</name>
    <message>
        <location filename="../src/quest.cpp" line="289"/>
        <source>Unknown resource type</source>
        <translation>未知資源類型</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1199"/>
        <source>Invalid file name: &apos;%1&apos;</source>
        <translation>檔案名稱無效:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1257"/>
        <source>File &apos;%1&apos; does not exist</source>
        <translation>文件&apos;%1&apos;不存在</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1270"/>
        <source>File &apos;%1&apos; already exists</source>
        <translation>文件&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1331"/>
        <source>Wrong script name: &apos;%1&apos; (should end with &apos;.lua&apos;)</source>
        <translation>錯誤的腳本名稱:&apos;%1&apos;(應以&apos;.lua&apos;結尾)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1407"/>
        <source>Cannot create file &apos;%1&apos;</source>
        <translation>無法建立檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1196"/>
        <source>Empty file name</source>
        <translation>檔案名為空</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="140"/>
        <source>No quest</source>
        <translation>沒有任務</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="145"/>
        <source>Missing Solarus version in quest.dat</source>
        <translation>quest.dat 中缺少 Solarus 版本</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1293"/>
        <source>File &apos;%1&apos; is not a folder</source>
        <translation>檔案&apos;%1&apos;不是資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1306"/>
        <source>File &apos;%1&apos; is a folder</source>
        <translation>檔案&apos;%1&apos;是資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1456"/>
        <source>Cannot read file &apos;%1&apos;</source>
        <translation>無法讀取檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1427"/>
        <location filename="../src/quest.cpp" line="1464"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>無法寫入檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1357"/>
        <source>Wrong GLSL shader file name: &apos;%1&apos; (should end with &apos;.glsl&apos;)</source>
        <translation>GLSL 著色器檔案名稱錯誤:&apos;%1&apos;(應以&apos;.glsl&apos;結尾)</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1392"/>
        <source>Wrong image file name: &apos;%1&apos; (should end with &apos;.png&apos;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1779"/>
        <source>Cannot load image file: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1784"/>
        <source>This sprite already exists: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1806"/>
        <source>Cannot create folder &apos;%1&apos;: parent folder does not exist</source>
        <translation>無法建立資料夾&apos;%1&apos;:父資料夾不存在</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1812"/>
        <location filename="../src/quest.cpp" line="1849"/>
        <source>Cannot create folder &apos;%1&apos;</source>
        <translation>無法建立資料夾 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1982"/>
        <source>Resource &apos;%1&apos; already exists</source>
        <translation>資源&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1999"/>
        <location filename="../src/quest.cpp" line="2053"/>
        <source>Cannot rename file &apos;%1&apos;</source>
        <translation>無法重新命名檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2191"/>
        <source>Cannot delete file &apos;%1&apos;</source>
        <translation>無法刪除檔案&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2231"/>
        <source>Cannot delete folder &apos;%1&apos;</source>
        <translation>無法刪除資料夾&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2268"/>
        <source>Failed to delete file &apos;%1&apos;</source>
        <translation>刪除檔案 &apos;%1&apos;失敗</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2274"/>
        <source>Failed to delete folder &apos;%1&apos;</source>
        <translation>刪除資料夾 &apos;%1&apos;失敗</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2121"/>
        <source>Same source and destination id</source>
        <translation>相同的來源和目標 ID</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2128"/>
        <source>A resource with id &apos;%1&apos; already exists</source>
        <translation>ID 為&apos;%1&apos;的資源已存在</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="2133"/>
        <location filename="../src/quest.cpp" line="2177"/>
        <source>No such resource: &apos;%1&apos;</source>
        <translation>沒有這樣的資源:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest.cpp" line="1234"/>
        <source>File &apos;%1&apos; is not in this quest</source>
        <translation>文件&apos;%1&apos;不在此任務中</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestDatabase</name>
    <message>
        <location filename="../src/quest_database.cpp" line="36"/>
        <source>Map</source>
        <comment>resource_type</comment>
        <extracomment>To describe the type of resource itself like: this is a Map.</extracomment>
        <translation>地圖</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="37"/>
        <source>Tileset</source>
        <comment>resource_type</comment>
        <translation>圖塊集</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="38"/>
        <source>Sprite</source>
        <comment>resource_type</comment>
        <translation>精靈圖</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="39"/>
        <source>Music</source>
        <comment>resource_type</comment>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="40"/>
        <source>Sound</source>
        <comment>resource_type</comment>
        <translation>音效</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="41"/>
        <source>Item</source>
        <comment>resource_type</comment>
        <translation>物品</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="42"/>
        <source>Enemy</source>
        <comment>resource_type</comment>
        <translation>敵人</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="43"/>
        <source>Custom entity</source>
        <comment>resource_type</comment>
        <translation>自訂實體</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="44"/>
        <source>Language</source>
        <comment>resource_type</comment>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="45"/>
        <source>Font</source>
        <comment>resource_type</comment>
        <translation>字體</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="46"/>
        <source>Shader</source>
        <comment>resource_type</comment>
        <translation>著色器</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="51"/>
        <source>Map</source>
        <comment>resource_element</comment>
        <extracomment>To be used with a specific element id like: Rename Map X.</extracomment>
        <translation>地圖</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="52"/>
        <source>Tileset</source>
        <comment>resource_element</comment>
        <translation>圖塊集</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="53"/>
        <source>Sprite</source>
        <comment>resource_element</comment>
        <translation>精靈圖</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="54"/>
        <source>Music</source>
        <comment>resource_element</comment>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="55"/>
        <source>Sound</source>
        <comment>resource_element</comment>
        <translation>音效</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="56"/>
        <source>Item</source>
        <comment>resource_element</comment>
        <translation>物品</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="57"/>
        <source>Enemy</source>
        <comment>resource_element</comment>
        <translation>敵人</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="58"/>
        <source>Custom entity</source>
        <comment>resource_element</comment>
        <translation>自訂實體</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="59"/>
        <source>Language</source>
        <comment>resource_element</comment>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="60"/>
        <source>Font</source>
        <comment>resource_element</comment>
        <translation>字體</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="61"/>
        <source>Shader</source>
        <comment>resource_element</comment>
        <translation>著色器</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="65"/>
        <source>Maps folder</source>
        <translation>地圖資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="66"/>
        <source>Tilesets folder</source>
        <translation>圖塊集資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="67"/>
        <source>Sprites folder</source>
        <translation>精靈圖資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="68"/>
        <source>Musics folder</source>
        <translation>音樂資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="69"/>
        <source>Sounds folder</source>
        <translation>音效資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="70"/>
        <source>Items folder</source>
        <translation>物品資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="71"/>
        <source>Enemies folder</source>
        <translation>敵人資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="72"/>
        <source>Custom entities folder</source>
        <translation>自訂實體資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="73"/>
        <source>Languages folder</source>
        <translation>語言資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="74"/>
        <source>Fonts folder</source>
        <translation>字體資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="75"/>
        <source>Shaders folder</source>
        <translation>著色器資料夾</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="79"/>
        <source>New map...</source>
        <translation>新地圖...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="80"/>
        <source>New tileset...</source>
        <translation>新圖塊集...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="81"/>
        <source>New sprite...</source>
        <translation>新精靈圖...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="82"/>
        <source>New music...</source>
        <translation>新音樂...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="83"/>
        <source>New sound...</source>
        <translation>新音效...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="84"/>
        <source>New item...</source>
        <translation>新物品...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="85"/>
        <source>New enemy breed...</source>
        <translation>新的敵人行為...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="86"/>
        <source>New custom entity model...</source>
        <translation>新的自訂實體模型...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="87"/>
        <source>New language...</source>
        <translation>新語言...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="88"/>
        <source>New font...</source>
        <translation>新字體...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="89"/>
        <source>New shader...</source>
        <translation>新的著色器...</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="125"/>
        <source>No quest</source>
        <translation>沒有任務</translation>
    </message>
    <message>
        <location filename="../src/quest_database.cpp" line="130"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>無法寫入檔案 &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestFilesModel</name>
    <message>
        <location filename="../src/quest_files_model.cpp" line="362"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="359"/>
        <source>File</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="365"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="368"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="371"/>
        <source>License</source>
        <translation>授權許可證</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="591"/>
        <source>Quest</source>
        <translation>任務</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="596"/>
        <source>Main Lua script</source>
        <translation>主要Lua腳本</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="610"/>
        <source>Dialogs file</source>
        <translation>對話文件</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="614"/>
        <source>Strings file</source>
        <translation>字串文件</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="622"/>
        <source>Map script</source>
        <translation>地圖腳本</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="625"/>
        <source>Script</source>
        <translation>腳本</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="629"/>
        <source>GLSL shader code</source>
        <translation>GLSL 著色器程式</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="637"/>
        <source>Tileset tiles image</source>
        <translation>圖塊集圖塊影像</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="641"/>
        <source>Tileset sprites image</source>
        <translation>圖塊集精靈圖影像</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="644"/>
        <source>Image</source>
        <translation>圖像</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="649"/>
        <source>Data file</source>
        <translation>資料檔案</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="778"/>
        <source>%1 (file not found)</source>
        <translation>%1(未找到文件)</translation>
    </message>
    <message>
        <location filename="../src/quest_files_model.cpp" line="788"/>
        <source>%1 (not in the quest)</source>
        <translation>%1(不在任務中)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestProperties</name>
    <message>
        <location filename="../src/quest_properties.cpp" line="47"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>無法開啟檔案 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="58"/>
        <source>No quest</source>
        <translation>沒有任務</translation>
    </message>
    <message>
        <location filename="../src/quest_properties.cpp" line="63"/>
        <source>Cannot write file &apos;%1&apos;</source>
        <translation>無法寫入檔案&apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestPropertiesEditor</name>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="62"/>
        <source>Quest information</source>
        <translation>任務資訊</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="81"/>
        <source>Solarus version</source>
        <translation>Solarus 版本</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="88"/>
        <source>Version of the engine your data files are compatible with</source>
        <translation>您的資料檔案相容的引擎版本</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="103"/>
        <source>Write directory</source>
        <translation>寫入目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="115"/>
        <source>Folder where to write savegames, relative to &quot;$HOME/.solarus/&quot;.
Must identify your quest to avoid confusion with other quests.</source>
        <translation>寫入保存遊戲的資料夾，相對於&quot;$HOME/.solarus/&quot;。
必須明確標示您的任務以避免與其他任務混淆。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="133"/>
        <source>Quest title</source>
        <translation>任務標題</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="145"/>
        <source>The name of your quest.</source>
        <translation>任務的名稱。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="159"/>
        <source>Summary</source>
        <translation>大綱</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="171"/>
        <source>One line describing your quest.</source>
        <translation>簡短地描述你做的任務。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="185"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="224"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="236"/>
        <source>People who develop this quest.</source>
        <translation>開發此任務的人。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="250"/>
        <source>Quest version</source>
        <translation>任務版本</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="262"/>
        <source>Current release of your quest.</source>
        <translation>您的任務目前的版本。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="276"/>
        <source>Release date</source>
        <translation>發布日期</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="287"/>
        <source>In progress</source>
        <translation>開發進行中</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="294"/>
        <source>Released</source>
        <translation>已發布</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="329"/>
        <source>Status and date of the current release.</source>
        <translation>目前發布版本的狀態和日期。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="343"/>
        <source>Website</source>
        <translation>網站</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="355"/>
        <source>Official website of the quest.</source>
        <translation>任務的官方網站。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="378"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="389"/>
        <source>Video options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="398"/>
        <source>Smooth camera scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="405"/>
        <source>Enables subpixel smoothing of camera scrolling movement.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="415"/>
        <source>Synchronize update rate with video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="422"/>
        <source>Keeps the main loop update rate the same as the video refresh rate.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="210"/>
        <source>A more detailed description of your quest.</source>
        <translation>更加詳細描述您的任務。</translation>
    </message>
    <message>
        <source>Quest size</source>
        <translation type="vanished">任務大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="507"/>
        <source>Size of the logical game area (before any scaling).
This will be the visible space of the current map.</source>
        <translation>遊戲區域的大小(在任何縮放之前)。
這將是目前地圖的可見空間。</translation>
    </message>
    <message>
        <source>Folder where to write savegames, relative to &quot;$HOME/.solarus/&quot;.
Should identify your quest to avoid confusion with other quests.</source>
        <translation type="vanished">Dossier où écrire les sauvegardes, par rapport à &quot;$HOME/.solarus/&quot;.
Permet d&apos;identifier votre quête pour éviter toute confusion avec d&apos;autres quêtes.</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="436"/>
        <source>Normal quest size</source>
        <translation>正常任務大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="525"/>
        <source>Minimum quest size</source>
        <translation>最小任務大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="596"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="681"/>
        <source>Only useful to support a range of logical sizes.</source>
        <translation>僅適用於支援範圍內的邏輯大小。</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="610"/>
        <source>Maximum quest size</source>
        <translation>最大任務大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.ui" line="466"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="555"/>
        <location filename="../src/widgets/quest_properties_editor.ui" line="640"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <source># Write directory #
Directory where your quest will write its savegames and setting files.
It will be a subdirectory of &apos;$HOME/.solarus/&apos;, automatically created by the engine. 
Its name should identify your quest, to avoid confusion with other Solarus quests that might also be installed on the user&apos;s machine. 
You must define it before you can use savegames or setting files.

# Title bar #
Title of the window. You should probably put the title of your game here.
</source>
        <translation type="vanished"># Dossier d&apos;écriture #
Dossier dans lequel votre quête va enregistrer ses sauvegardes de jeu et fichiers de configuration.
Il sera un sous-dossier de &apos;$HOME/.solarus/&apos;, crée automatiquement par le moteur.
Son nom devrait identifier votre quête, pour éviter la confusion avec d&apos;autres quête de Solarus qui pourrait aussi être installé sur la machine de l&apos;utilisateur.
Vous devez le définir avant de pouvoir utiliser les sauvegardes de jeu ou les fichiers de configuration.

# Barre de titre #
Titre de la fenêtre. Vous devriez probablement mettre le titre de votre jeu ici.
</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="63"/>
        <source>Change write directory</source>
        <translation>更改寫入目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="93"/>
        <source>Change title</source>
        <translation>更改標題</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="122"/>
        <source>Change summary</source>
        <translation>變更摘要</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="148"/>
        <source>Change detailed description</source>
        <translation>更改詳細說明</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="174"/>
        <source>Change author</source>
        <translation>更改作者</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="200"/>
        <source>Change quest version</source>
        <translation>更改任務版本</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="226"/>
        <source>Change release date</source>
        <translation>更改發布日期</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="252"/>
        <source>Change website</source>
        <translation>更改網站</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="279"/>
        <source>Change normal size</source>
        <translation>改變正常尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="309"/>
        <source>Change minimum size</source>
        <translation>更改最小尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="353"/>
        <source>Change maximum size</source>
        <translation>改變最大尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="397"/>
        <source>Set subpixel camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="424"/>
        <source>Set dynamic timestep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="463"/>
        <source>Quest properties</source>
        <translation>任務屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_properties_editor.cpp" line="466"/>
        <source>Quest properties have been modified. Save changes?</source>
        <translation>任務屬性已被修改。 儲存變更?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::QuestTreeView</name>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="458"/>
        <source>New folder...</source>
        <translation>新的資料夾...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="466"/>
        <source>New script...</source>
        <translation>新腳本...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="65"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="544"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="59"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="503"/>
        <location filename="../src/widgets/quest_tree_view.cpp" line="511"/>
        <source>Play</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="565"/>
        <source>Open Script</source>
        <translation>開啟腳本</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="576"/>
        <source>Open Dialogs</source>
        <translation>開啟對話框</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="582"/>
        <source>Open Strings</source>
        <translation>開啟字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="72"/>
        <source>Rename...</source>
        <translation>重新命名...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="680"/>
        <source>Change description...</source>
        <translation>更改說明...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="80"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="73"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="88"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="405"/>
        <source>Add to quest as %1...</source>
        <translation>作為 %1 新增到任務...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="429"/>
        <source>New sprite from image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="443"/>
        <source>New GLSL file...</source>
        <translation>新的 GLSL 檔案...</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="499"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="625"/>
        <source>Open Properties</source>
        <translation>開啟屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="631"/>
        <source>Explore folder</source>
        <translation>探索資料夾</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="87"/>
        <source>Author and license...</source>
        <translation>作者和許可證...</translation>
    </message>
    <message>
        <source>New folder</source>
        <translation type="vanished">新的資料夾</translation>
    </message>
    <message>
        <source>Folder name:</source>
        <translation type="vanished">資料夾名稱:</translation>
    </message>
    <message>
        <source>New Lua script</source>
        <translation type="vanished">新Lua腳本</translation>
    </message>
    <message>
        <source>File name:</source>
        <translation type="vanished">檔案名稱:</translation>
    </message>
    <message>
        <source>New GLSL file</source>
        <translation type="vanished">新的 GLSL 檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1101"/>
        <source>Change description</source>
        <translation>更改說明</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1102"/>
        <source>New description for %1 &apos;%2&apos;:</source>
        <translation>%1 &apos;%2&apos; 的新 描述:</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1139"/>
        <source>File information for &apos;%1&apos;</source>
        <translation>&apos;%1&apos;的文件資訊</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1141"/>
        <source>File information for %1 selected items</source>
        <translation>%1 個選定項目的文件資訊</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1222"/>
        <source>Do you really want to delete &apos;%1&apos;?</source>
        <translation>您確定要刪除&apos;%1&apos;嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1223"/>
        <source>Do you really want to delete these %1 items?</source>
        <translation>您確定要刪除這些 %1 項嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/quest_tree_view.cpp" line="1225"/>
        <source>Delete confirmation</source>
        <translation>刪除確認</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SettingsDialog</name>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="14"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="24"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="30"/>
        <source>Appaerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="36"/>
        <source>Theme:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="50"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="55"/>
        <source>Light mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="60"/>
        <source>Dark mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="84"/>
        <source>Files</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="92"/>
        <source>Working directory:</source>
        <translation>工作目錄:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="102"/>
        <source>Browse...</source>
        <translation>瀏覽...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="111"/>
        <source>Restore open tabs at startup</source>
        <translation>恢復啟動時開啟的分頁</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="121"/>
        <source>Running</source>
        <translation>運行</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="129"/>
        <source>Save modified files before running:</source>
        <translation>運行前保存修改的檔案:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="137"/>
        <source>Ask</source>
        <translation>提問</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="142"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="147"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="170"/>
        <source>No audio</source>
        <translation>無音效</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="179"/>
        <source>Quest size:</source>
        <translation>任務大小:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="191"/>
        <source>Force Software Rendering</source>
        <translation>強制使用軟體渲染</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="198"/>
        <source>Suspend when unfocused</source>
        <translation>不專注視窗時暫停</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="292"/>
        <source>Tabulation</source>
        <translation>間隔</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="298"/>
        <source>Length:</source>
        <translation>長度:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="331"/>
        <source>Replace by space</source>
        <translation>用空格取代</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="354"/>
        <source>External Editor</source>
        <translation>外部編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="366"/>
        <source>Command:</source>
        <translation>命令行:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="376"/>
        <source>executable %f %p</source>
        <translation>可執行檔 %f %p</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="395"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="433"/>
        <location filename="../src/widgets/settings_dialog.ui" line="571"/>
        <location filename="../src/widgets/settings_dialog.ui" line="625"/>
        <location filename="../src/widgets/settings_dialog.ui" line="772"/>
        <location filename="../src/widgets/settings_dialog.ui" line="912"/>
        <source>Default zoom:</source>
        <translation>預設縮放:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="458"/>
        <location filename="../src/widgets/settings_dialog.ui" line="650"/>
        <location filename="../src/widgets/settings_dialog.ui" line="937"/>
        <source>Grid:</source>
        <translation>網格:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="491"/>
        <location filename="../src/widgets/settings_dialog.ui" line="693"/>
        <location filename="../src/widgets/settings_dialog.ui" line="824"/>
        <location filename="../src/widgets/settings_dialog.ui" line="970"/>
        <source>Show at opening</source>
        <translation>開啟時顯示</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="508"/>
        <location filename="../src/widgets/settings_dialog.ui" line="716"/>
        <location filename="../src/widgets/settings_dialog.ui" line="987"/>
        <source>Default size:</source>
        <translation>預設尺寸:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="515"/>
        <location filename="../src/widgets/settings_dialog.ui" line="723"/>
        <location filename="../src/widgets/settings_dialog.ui" line="994"/>
        <source>Style:</source>
        <translation>大小:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="522"/>
        <location filename="../src/widgets/settings_dialog.ui" line="730"/>
        <location filename="../src/widgets/settings_dialog.ui" line="831"/>
        <location filename="../src/widgets/settings_dialog.ui" line="1001"/>
        <source>Color:</source>
        <translation>顏色:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="565"/>
        <source>Tileset graphics view</source>
        <translation>圖塊集圖形視圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="611"/>
        <source>Sprite editor</source>
        <translation>精靈圖編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="425"/>
        <location filename="../src/widgets/settings_dialog.ui" line="617"/>
        <source>Main graphics view</source>
        <translation>主要圖形視圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="737"/>
        <source>Auto detect size</source>
        <translation>自動檢測尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="764"/>
        <source>Previewer graphics view</source>
        <translation>預覽圖形視圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="797"/>
        <source>Origin:</source>
        <translation>來源:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="222"/>
        <source>Text editor</source>
        <translation>文字編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="228"/>
        <source>Font</source>
        <translation>字體</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="234"/>
        <source>Family:</source>
        <translation>字體系列:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="268"/>
        <source>Size:</source>
        <translation>尺寸:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="419"/>
        <source>Map editor</source>
        <translation>地圖編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="898"/>
        <source>Tileset editor</source>
        <translation>圖塊集編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.ui" line="904"/>
        <source>Graphics view</source>
        <translation>圖形視圖</translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="vanished">背景顏色:</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="108"/>
        <source>Select external editor</source>
        <translation>選擇外部編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="186"/>
        <source>Restore default settings</source>
        <translation>恢復預設設定</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="187"/>
        <source>Do you really want to restore default settings?</source>
        <translation>您真的要恢復預設設定嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="355"/>
        <source>Working directory</source>
        <translation>工作目錄</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="994"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="995"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="996"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="997"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/settings_dialog.cpp" line="998"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderEditor</name>
    <message>
        <source>Shader properties</source>
        <translation type="vanished">著色器屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="81"/>
        <source>Shader id</source>
        <translation>著色器 ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="88"/>
        <source>Filename of the shader program (without extension)</source>
        <translation>著色器程式碼的檔案名稱(不含副檔名)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="104"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="117"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="124"/>
        <location filename="../src/widgets/shader_editor.cpp" line="68"/>
        <source>Scaling factor</source>
        <translation>比例因素</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="153"/>
        <source>Preview settings</source>
        <translation>預覽設定</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="167"/>
        <source>Preview mode</source>
        <translation>預覽模式</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="188"/>
        <source>Picture</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="202"/>
        <source>Map</source>
        <translation>地圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="213"/>
        <source>Sprite</source>
        <translation>精靈圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="275"/>
        <source>Open a PNG file</source>
        <translation>開啟 PNG 文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="376"/>
        <source>Animation</source>
        <translation>動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="393"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="463"/>
        <source>Vertex shader</source>
        <translation>頂點著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="481"/>
        <source>Use a vertex shader</source>
        <translation>使用頂點著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="539"/>
        <source>New vertex shader file</source>
        <translation>新的頂點著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="550"/>
        <source>Save vertex shader file</source>
        <translation>儲存頂點著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="566"/>
        <source>Open vertex shader file</source>
        <translation>打開頂點著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="624"/>
        <source>Fragment shader</source>
        <translation>片段著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="642"/>
        <source>Use a fragment shader</source>
        <translation>使用片段著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="700"/>
        <source>New fragment shader file</source>
        <translation>新的片段著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="711"/>
        <source>Save fragment shader file</source>
        <translation>儲存片段著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.ui" line="727"/>
        <source>Open fragment shader file</source>
        <translation>打開片段著色器文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="92"/>
        <source>Shader file</source>
        <translation>著色器檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="158"/>
        <source>File &apos;%1&apos; is not a shader</source>
        <translation>文件&apos;%1&apos;不是著色器</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="163"/>
        <source>Shader %1</source>
        <translation>著色器 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="166"/>
        <source>Shader &apos;%1&apos; has been modified. Save changes?</source>
        <translation>著色器 &apos;%1&apos; 已被修改。 儲存變更?</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="426"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="647"/>
        <source>New GLSL file</source>
        <translation>新的 GLSL 檔案</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="648"/>
        <source>File name:</source>
        <translation>檔案名稱:</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="688"/>
        <source>Open a GLSL file</source>
        <translation>開啟 GLSL 文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="690"/>
        <source>GLSL shader file (*.glsl)</source>
        <translation>GLSL 著色器檔案 (*.glsl)</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="702"/>
        <source>Shader GLSL files must be in the shaders directory</source>
        <translation>Shader GLSL 檔案必須位於 Shaders 目錄中</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="836"/>
        <source>Open a PNG picture</source>
        <translation>開啟 PNG 文件</translation>
    </message>
    <message>
        <location filename="../src/widgets/shader_editor.cpp" line="838"/>
        <source>PNG file (*.png)</source>
        <translation>PNG檔 (*.png)</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::ShaderModel</name>
    <message>
        <location filename="../src/shader_model.cpp" line="44"/>
        <source>Cannot open shader data file &apos;%1&apos;</source>
        <translation>無法開啟著色器檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/shader_model.cpp" line="166"/>
        <source>Cannot save shader &apos;%1&apos;</source>
        <translation>無法儲存著色器&apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SoundChooser</name>
    <message>
        <location filename="../src/widgets/sound_chooser.cpp" line="37"/>
        <source>Play sound</source>
        <translation>播放聲音</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteEditor</name>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="20"/>
        <source>Sprite editor</source>
        <translation>精靈圖編輯器</translation>
    </message>
    <message>
        <source>Sprite properties</source>
        <translation type="vanished">精靈圖屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="114"/>
        <source>Sprite id</source>
        <translation>精靈圖ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="121"/>
        <source>Filename of the sprite (without extension)</source>
        <translation>精靈圖的檔案名稱(不含副檔名)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="134"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="141"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="164"/>
        <source>Add</source>
        <translation>新增</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="202"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="240"/>
        <source>Duplicate</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="354"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="397"/>
        <source>Animation properties</source>
        <translation>動畫屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="409"/>
        <source>Source image</source>
        <translation>來源圖像</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="445"/>
        <source>Source image of the animation</source>
        <translation>來源圖像的動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="461"/>
        <source>Tileset of the animation</source>
        <translation>圖塊集的動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="468"/>
        <source>Refresh image</source>
        <translation>更新圖像</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="471"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="488"/>
        <source>Change</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="513"/>
        <source>Delay in milliseconds between two frames of the animation</source>
        <translation>動畫兩幀之間的延遲(以毫秒為單位)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="516"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="532"/>
        <source>Frame delay</source>
        <translation>幀延遲</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="539"/>
        <source>Loop on frame</source>
        <translation>幀循環</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="549"/>
        <source>Index of the frame where you want the animation to come back when the last frame finishes</source>
        <translation>最後一幀結束時，您希望動畫返回幀的索引</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="565"/>
        <source>Default animation</source>
        <translation>預設動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="572"/>
        <source>Default animation of the sprite</source>
        <translation>精靈圖的預設動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="575"/>
        <source>Set as default</source>
        <translation>設定為預設</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="690"/>
        <source>Direction preview</source>
        <translation>預覽方位</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="585"/>
        <source>Direction properties</source>
        <translation>方位屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="278"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="316"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="594"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="785"/>
        <source>X coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>影像中包含畫面的區域左上角的 X 座標</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="786"/>
        <source>Y coordinate of the top-left corner of area containing the frames in the image</source>
        <translation>影像中包含畫面的區域左上角的 Y 座標</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="601"/>
        <source>Origin</source>
        <translation>來源</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="608"/>
        <source>Number of frames</source>
        <translation>幀數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="615"/>
        <source>Number of columns</source>
        <translation>列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="790"/>
        <source>X coordinate of the origin point of the sprite</source>
        <translation>精靈圖原點的 X 座標</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="791"/>
        <source>Y coordinate of the origin point of the sprite</source>
        <translation>精靈圖原點的 Y 座標</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="622"/>
        <source>Number of frames of this direction</source>
        <translation>此方向的幀數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="635"/>
        <source>Number of columns of the grid containing the frames of this direction in the image</source>
        <translation>影像中包含該方向幀的網格的列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.ui" line="648"/>
        <source>Size</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="780"/>
        <source>Width of each frame in the image</source>
        <translation>影像中每格的寬度</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="781"/>
        <source>Height of each frame in the image</source>
        <translation>影像中每格的高度</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="70"/>
        <location filename="../src/widgets/sprite_editor.cpp" line="801"/>
        <source>Create animation</source>
        <translation>建立動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="107"/>
        <source>Duplicate animation</source>
        <translation>重複動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="119"/>
        <source> (copy)</source>
        <translation> (複製)</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="141"/>
        <source>Change animation name</source>
        <translation>更改動畫名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="176"/>
        <source>Delete animation</source>
        <translation>刪除動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="213"/>
        <source>Change default animation</source>
        <translation>更改預設動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="246"/>
        <source>Change source image</source>
        <translation>變更來源影像</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="281"/>
        <source>Change frame delay</source>
        <translation>更改幀延遲</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="315"/>
        <source>Change loop on frame</source>
        <translation>更改幀上的循環</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="350"/>
        <source>Add direction</source>
        <translation>添加方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="387"/>
        <source>Duplicate direction</source>
        <translation>重複方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="422"/>
        <source>Move direction up</source>
        <translation>向上移動方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="455"/>
        <source>Move direction down</source>
        <translation>向下移動方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="488"/>
        <source>Delete direction</source>
        <translation>刪除方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="519"/>
        <source>Change direction size</source>
        <translation>改變方向尺寸</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="554"/>
        <source>Change direction position</source>
        <translation>改變方向位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="589"/>
        <source>Change direction origin</source>
        <translation>改變方向原點</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="623"/>
        <source>Change number of frames of direction</source>
        <translation>改變方向的幀數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="659"/>
        <source>Change number of columns of direction</source>
        <translation>改變方向的列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="695"/>
        <source>Change number of frames/columns of direction</source>
        <translation>更改方向的幀數/列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="749"/>
        <source>File &apos;%1&apos; is not a sprite</source>
        <translation>文件&apos;%1&apos;不是精靈圖</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="754"/>
        <source>Sprite %1</source>
        <translation>精靈圖 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="757"/>
        <source>Sprite &apos;%1&apos; has been modified. Save changes?</source>
        <translation>精靈圖 &apos;%1&apos; 已被修改。 儲存變更?</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="802"/>
        <source>Create direction</source>
        <translation>創建方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_editor.cpp" line="992"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteModel</name>
    <message>
        <location filename="../src/sprite_model.cpp" line="51"/>
        <source>Cannot open sprite &apos;%1&apos;</source>
        <translation>無法開啟精靈圖&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="205"/>
        <source>Cannot save sprite &apos;%1&apos;</source>
        <translation>無法保存精靈圖&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="326"/>
        <source> (default)</source>
        <translation> (預設)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="349"/>
        <source>(right)</source>
        <translation>(右)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="350"/>
        <source>(up)</source>
        <translation>(上)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="351"/>
        <source>(left)</source>
        <translation>(左)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="352"/>
        <source>(down)</source>
        <translation>(下)</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="355"/>
        <source>Direction %1 %2</source>
        <translation>方位 %1 %2</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="428"/>
        <location filename="../src/sprite_model.cpp" line="487"/>
        <location filename="../src/sprite_model.cpp" line="611"/>
        <source>Animation name cannot be empty</source>
        <translation>動畫名稱不能為空</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="433"/>
        <location filename="../src/sprite_model.cpp" line="492"/>
        <location filename="../src/sprite_model.cpp" line="615"/>
        <source>Animation &apos;%1&apos; already exists</source>
        <translation>動畫&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="549"/>
        <location filename="../src/sprite_model.cpp" line="607"/>
        <location filename="../src/sprite_model.cpp" line="876"/>
        <location filename="../src/sprite_model.cpp" line="931"/>
        <source>Animation &apos;%1&apos; does not exist</source>
        <translation>動畫&apos;%1&apos;不存在</translation>
    </message>
    <message>
        <location filename="../src/sprite_model.cpp" line="984"/>
        <location filename="../src/sprite_model.cpp" line="1047"/>
        <source>Direction %1 does not exist in animation &apos;%2&apos;</source>
        <translation>動畫&apos;%2&apos;中不存在方位 %1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpritePreviewer</name>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="33"/>
        <source>Show origin</source>
        <translation>顯示來源</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="40"/>
        <location filename="../src/widgets/sprite_previewer.cpp" line="377"/>
        <source>Zoom</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="86"/>
        <source>Index of the current frame</source>
        <translation>當前幀的索引</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="114"/>
        <source>Last</source>
        <translation>最後</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="146"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="178"/>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="239"/>
        <source>Previous</source>
        <translation>上一個</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.ui" line="271"/>
        <source>First</source>
        <translation>第一個</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="187"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="190"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="379"/>
        <source>25 %</source>
        <translation>25 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="380"/>
        <source>50 %</source>
        <translation>50 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="381"/>
        <source>100 %</source>
        <translation>100 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="382"/>
        <source>200 %</source>
        <translation>200 %</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_previewer.cpp" line="383"/>
        <source>400 %</source>
        <translation>400 %</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteScene</name>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="201"/>
        <source>This tileset has no sprite image.
Please select another tileset.</source>
        <translation>該圖塊集沒有精靈圖。
請選擇另一個圖塊集。</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="207"/>
        <source>Missing source image &apos;%1&apos;</source>
        <translation>缺少來源影像&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_scene.cpp" line="371"/>
        <source>No such direction index: %1</source>
        <translation>無此方向索引:%1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteTreeView</name>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="38"/>
        <source>Create animation</source>
        <translation>建立動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="44"/>
        <source>Create direction</source>
        <translation>創建方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="50"/>
        <source>Rename animation</source>
        <translation>重新命名動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="51"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="58"/>
        <source>Duplicate</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="65"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="71"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_tree_view.cpp" line="77"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::SpriteView</name>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="50"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="57"/>
        <source>Duplicate...</source>
        <translation>複製...</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="64"/>
        <source>Change the number of frames/columns</source>
        <translation>更改幀數/列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="65"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="73"/>
        <source>Change the number of frames</source>
        <translation>更改幀數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="80"/>
        <source>Change the number of columns</source>
        <translation>更改列數</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="613"/>
        <source>New multiframe direction</source>
        <translation>新的多幀方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="622"/>
        <location filename="../src/widgets/sprite_view.cpp" line="682"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="608"/>
        <source>New direction</source>
        <translation>新的方向</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="670"/>
        <source>Move here</source>
        <translation>移動至此</translation>
    </message>
    <message>
        <location filename="../src/widgets/sprite_view.cpp" line="676"/>
        <source>Duplicate here</source>
        <translation>複製至此</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsEditor</name>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="14"/>
        <source>Strings editor</source>
        <translation>字串編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="32"/>
        <source>Language properties</source>
        <translation>語言屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="41"/>
        <source>Language id</source>
        <translation>語言id</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="48"/>
        <source>Folder name of the language</source>
        <translation>語言的資料夾名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="61"/>
        <source>Language description</source>
        <translation>語言的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="68"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="80"/>
        <source>Compare to language</source>
        <translation>比較語言</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="93"/>
        <source>Refresh language</source>
        <translation>重新整理語言</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="96"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="145"/>
        <source>Add</source>
        <translation>新增</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="183"/>
        <source>Change key</source>
        <translation>修改鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="221"/>
        <source>Duplicate string(s)</source>
        <translation>重複的字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.ui" line="259"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="67"/>
        <source>Create string</source>
        <translation>創建字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="98"/>
        <source>Duplicate strings</source>
        <translation>重複的字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="129"/>
        <source>Change string key</source>
        <translation>更改字串鍵</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="162"/>
        <source>Change string key prefix</source>
        <translation>更改字串鍵前綴</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="200"/>
        <source>Delete string</source>
        <translation>刪除字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="230"/>
        <source>Delete strings</source>
        <translation>刪除字串</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="264"/>
        <source>Change string value</source>
        <translation>更改字串值</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="312"/>
        <source>Strings %1</source>
        <translation>字串 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="315"/>
        <source>Strings &apos;%1&apos; have been modified. Save changes?</source>
        <translation>字串 %1 被修改了，是否儲存?</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="325"/>
        <source>&lt;No language&gt;</source>
        <translation>&lt;沒有語言&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="440"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="499"/>
        <source>_copy</source>
        <translation>_copy</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="502"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>字串&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="576"/>
        <source>Delete confirmation</source>
        <translation>刪除確認</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_editor.cpp" line="577"/>
        <source>Do you really want to delete all strings prefixed by &apos;%1&apos;?</source>
        <translation>您確實要刪除所有以&apos;%1&apos;為前綴的字串嗎?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsModel</name>
    <message>
        <location filename="../src/strings_model.cpp" line="45"/>
        <location filename="../src/strings_model.cpp" line="791"/>
        <source>Cannot open strings data file &apos;%1&apos;</source>
        <translation>無法開啟字串資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="83"/>
        <source>Cannot save strings data file &apos;%1&apos;</source>
        <translation>無法保存字串資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="236"/>
        <source>Key</source>
        <translation>鍵</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="237"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="240"/>
        <source>Translation (%1)</source>
        <translation>翻譯 (%1)</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="242"/>
        <source>Translation</source>
        <translation>翻譯</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="392"/>
        <location filename="../src/strings_model.cpp" line="529"/>
        <location filename="../src/strings_model.cpp" line="642"/>
        <source>Invalid string id: &apos;%1&apos;</source>
        <translation>無效的字串 ID:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="396"/>
        <location filename="../src/strings_model.cpp" line="459"/>
        <location filename="../src/strings_model.cpp" line="525"/>
        <location filename="../src/strings_model.cpp" line="605"/>
        <source>String &apos;%1&apos; already exists</source>
        <translation>字串&apos;%1&apos;已存在</translation>
    </message>
    <message>
        <location filename="../src/strings_model.cpp" line="521"/>
        <source>String &apos;%1&apos; does not exist</source>
        <translation>字串&apos;%1&apos;不存在</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::StringsTreeView</name>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="37"/>
        <source>New string...</source>
        <translation>新字串...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="43"/>
        <source>Duplicate string(s)...</source>
        <translation>重複的字串...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="49"/>
        <source>Change key...</source>
        <translation>改變按鍵...</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="50"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/strings_tree_view.cpp" line="57"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditor</name>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="59"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="101"/>
        <source>Cannot open file &apos;%1&apos;</source>
        <translation>無法開啟檔案 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor.cpp" line="128"/>
        <source>Cannot open file &apos;%1&apos; for writing</source>
        <translation>無法開啟檔案 &apos;%1&apos; 並寫入</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TextEditorWidget</name>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="360"/>
        <source>Cut</source>
        <translation>剪下</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="371"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="382"/>
        <source>Paste</source>
        <translation>貼上</translation>
    </message>
    <message>
        <location filename="../src/widgets/text_editor_widget.cpp" line="395"/>
        <source>Select all</source>
        <translation>全選</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilePatternsListView</name>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="39"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="46"/>
        <source>Change id...</source>
        <translation>更改ID...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tile_patterns_list_view.cpp" line="47"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetEditor</name>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="14"/>
        <source>Tileset editor</source>
        <translation>圖塊集編輯器</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="102"/>
        <source>Tileset id</source>
        <translation>圖塊集ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="109"/>
        <source>Filename of the tileset (without extension)</source>
        <translation>圖塊集的檔案名稱(不含副檔名)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="125"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="138"/>
        <source>User-friendly description to show in the editor</source>
        <translation>在編輯器中顯示對使用者友好的描述</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="161"/>
        <source>Background color applied to maps using this tileset</source>
        <translation>使用此圖塊集應用於地圖的背景顏色</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="232"/>
        <source>Name identifying the pattern</source>
        <translation>識別模式的名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="148"/>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="181"/>
        <source>Patterns</source>
        <translation>圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="196"/>
        <source>Selection properties</source>
        <translation>選擇屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="208"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="717"/>
        <source>Pattern id</source>
        <translation>圖案ID</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="242"/>
        <location filename="../src/widgets/tileset_editor.ui" line="488"/>
        <location filename="../src/widgets/tileset_editor.ui" line="585"/>
        <source>Rename (F2)</source>
        <translation>重新命名 (F2)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="277"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="210"/>
        <source>Ground</source>
        <translation>地面</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="290"/>
        <source>Terrain of the pattern</source>
        <translation>圖案的地形</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="337"/>
        <source>Animation</source>
        <translation>動畫</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="337"/>
        <source>Scrolling</source>
        <translation>捲動</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="350"/>
        <source>Kind of animation of the pattern</source>
        <translation>圖案的動畫類型</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="360"/>
        <source>Frames</source>
        <translation>幀</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="382"/>
        <source>Horizontal or vertical separation (only for multi-frame patterns)</source>
        <translation>水平或垂直分離(僅適用於多幀圖案)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="391"/>
        <source>Delay</source>
        <translation>延遲</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="398"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="414"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="500"/>
        <source>Mirror loop</source>
        <translation>鏡面循環</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="425"/>
        <source>Contours</source>
        <translation>輪廓</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="456"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="887"/>
        <source>Create contour</source>
        <translation>建立輪廓</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="563"/>
        <source>Contour properties</source>
        <translation>輪廓屬性</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="569"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="588"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="595"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="617"/>
        <source>Positioning</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="627"/>
        <source>Outside the selection</source>
        <translation>選擇區域以外</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="636"/>
        <source>Inside the selection</source>
        <translation>選擇區域以內</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="300"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="252"/>
        <source>Default layer</source>
        <translation>預設圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="320"/>
        <source>Repeatable</source>
        <translation>可重複</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="76"/>
        <source>Background color</source>
        <translation>背景顏色</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="104"/>
        <source>Move pattern</source>
        <translation>移動圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="172"/>
        <source>Move patterns</source>
        <translation>移動圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="294"/>
        <source>Repeat mode</source>
        <translation>重複模式</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="381"/>
        <source>Animation separation</source>
        <translation>動畫分離</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="420"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="460"/>
        <source>Frame delay</source>
        <translation>幀延遲</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="543"/>
        <source>Create pattern</source>
        <translation>創建圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="838"/>
        <source>Delete contour</source>
        <translation>刪除輪廓</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="915"/>
        <source>Delete contour pattern</source>
        <translation>刪除輪廓圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1352"/>
        <source>Rename tile pattern</source>
        <translation>重新命名圖塊圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1353"/>
        <source>New pattern id:</source>
        <translation>新圖案ID:</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1354"/>
        <source>Update existing maps using this pattern</source>
        <translation>使用此圖案更新現有地圖</translation>
    </message>
    <message>
        <source>Update references in existing maps</source>
        <translation type="obsolete">Mettre à jour les références dans les maps existantes</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.ui" line="526"/>
        <location filename="../src/widgets/tileset_editor.cpp" line="643"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="966"/>
        <source>File &apos;%1&apos; is not a tileset</source>
        <translation>文件&apos;%1&apos;不是圖塊集</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="971"/>
        <source>Tileset %1</source>
        <translation>圖塊集 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="974"/>
        <source>Tileset &apos;%1&apos; has been modified. Save changes?</source>
        <translation>圖塊集 &apos;%1&apos; 已被修改。 儲存變更?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1256"/>
        <source>Invalid description</source>
        <translation>描述無效</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="579"/>
        <source>Duplicate</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="138"/>
        <source>Resize pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="750"/>
        <source>Border set id</source>
        <translation>設定邊界id</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="777"/>
        <source>Border set inner</source>
        <translation>設定內邊界</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="809"/>
        <source>Border set patterns</source>
        <translation>邊界設定圖案</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1437"/>
        <source>Cannot open map file &apos;%1&apos;</source>
        <translation>無法開啟地圖檔案 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1445"/>
        <source>Invalid map file: &apos;%1&apos;</source>
        <translation>無效的地圖檔:%1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1470"/>
        <source>Failed to export map after changing pattern id: &apos;%1&apos;</source>
        <translation>更改模式 ID 後匯出地圖失敗:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1478"/>
        <source>Cannot open map file &apos;%1&apos; for writing</source>
        <translation>無法開啟地圖檔&apos;%1&apos;進行寫入</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1891"/>
        <source>Do you really want to delete pattern &apos;%1&apos;?</source>
        <translation>您確實要刪除圖案&apos;%1&apos;嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1895"/>
        <source>Do you really want to delete these %1 patterns?</source>
        <translation>您確實要刪除這些 %1 圖案嗎?</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1901"/>
        <source>Delete confirmation</source>
        <translation>刪除確認</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1938"/>
        <source>Border set name</source>
        <translation>邊界名稱</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_editor.cpp" line="1939"/>
        <source>Border set name:</source>
        <translation>邊界名稱:</translation>
    </message>
    <message>
        <source>The tileset image was modified.
Do you want to refresh the tileset?</source>
        <translation type="vanished">L&apos;image du tileset a été modifée.
Voulez-vous recharger l&apos;image ?</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetModel</name>
    <message>
        <location filename="../src/tileset_model.cpp" line="118"/>
        <source>Cannot open tileset data file &apos;%1&apos;</source>
        <translation>無法開啟圖塊集資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="165"/>
        <source>Cannot save tileset data file &apos;%1&apos;</source>
        <translation>無法儲存圖塊集資料檔&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="184"/>
        <source>Failed to refresh tileset: %1</source>
        <translation>無法刷新圖塊集:%1</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="502"/>
        <location filename="../src/tileset_model.cpp" line="567"/>
        <location filename="../src/tileset_model.cpp" line="634"/>
        <source>Invalid tile pattern index: %1</source>
        <translation>無效的圖塊圖案索引:%1</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1796"/>
        <source>Contour already exists: &apos;%1&apos;</source>
        <translation>輪廓已存在:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1800"/>
        <location filename="../src/tileset_model.cpp" line="1851"/>
        <source>Invalid contour id: &apos;%1&apos;</source>
        <translation>無效輪廓 ID:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1806"/>
        <source>Failed to create contour &apos;%1&apos;</source>
        <translation>無法建立輪廓&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1823"/>
        <location filename="../src/tileset_model.cpp" line="1847"/>
        <location filename="../src/tileset_model.cpp" line="1878"/>
        <location filename="../src/tileset_model.cpp" line="1899"/>
        <location filename="../src/tileset_model.cpp" line="1923"/>
        <location filename="../src/tileset_model.cpp" line="1938"/>
        <location filename="../src/tileset_model.cpp" line="1966"/>
        <location filename="../src/tileset_model.cpp" line="1987"/>
        <location filename="../src/tileset_model.cpp" line="2005"/>
        <source>No such contour: &apos;%1&apos;</source>
        <translation>沒有這樣的輪廓:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1829"/>
        <source>Failed to delete contour &apos;%1&apos;</source>
        <translation>無法刪除輪廓&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1855"/>
        <source>Contour id already in use: &apos;%1&apos;</source>
        <translation>輪廓 ID 已在使用:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="1861"/>
        <source>Failed to rename contour &apos;%1&apos;</source>
        <translation>無法重命名輪廓&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="432"/>
        <location filename="../src/tileset_model.cpp" line="638"/>
        <source>Invalid tile pattern id: &apos;%1&apos;</source>
        <translation>無效的圖塊圖案 ID:&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/tileset_model.cpp" line="436"/>
        <location filename="../src/tileset_model.cpp" line="642"/>
        <source>Tile pattern &apos;%1&apos; already exists</source>
        <translation>圖塊圖案&apos;%1&apos;已存在</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetScene</name>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="177"/>
        <source>Missing tileset image &apos;%1&apos;</source>
        <translation>缺少圖塊集影像&apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_scene.cpp" line="454"/>
        <source>No such pattern index: %1</source>
        <translation>無此圖案索引:%1</translation>
    </message>
</context>
<context>
    <name>SolarusEditor::TilesetView</name>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="182"/>
        <source>Change id...</source>
        <translation>更改ID...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="183"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="190"/>
        <source>Delete...</source>
        <translation>刪除...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="168"/>
        <source>Create contour...</source>
        <translation>建立輪廓...</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="157"/>
        <source>Resize</source>
        <translation type="unfinished">調整大小</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="160"/>
        <source>R</source>
        <translation type="unfinished">R</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="171"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="204"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="205"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="206"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="207"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="667"/>
        <source>Ground</source>
        <translation type="unfinished">地面</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="672"/>
        <source>Default layer</source>
        <translation>預設圖層</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="677"/>
        <source>Repeatable</source>
        <translation>可重複</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="682"/>
        <source>Scrolling</source>
        <translation>捲動</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="756"/>
        <source>Layer %1</source>
        <translation>層級 %1</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1351"/>
        <source>New pattern (more options)</source>
        <translation>新圖案(更多選項)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1357"/>
        <source>New pattern (%1)</source>
        <translation>新圖案 (%1)</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1367"/>
        <location filename="../src/widgets/tileset_view.cpp" line="1502"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1488"/>
        <source>Move here</source>
        <translation>移動到這</translation>
    </message>
    <message>
        <location filename="../src/widgets/tileset_view.cpp" line="1494"/>
        <source>Duplicate here</source>
        <translation>複製到這</translation>
    </message>
</context>
</TS>

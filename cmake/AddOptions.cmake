# Solarus -specific -D options.

# Disable GUI native dialogs.
set(SOLARUSEDITOR_NO_NATIVE_DIALOGS "OFF" CACHE BOOL "Disable native dialogs in the Solarus GUI.")
